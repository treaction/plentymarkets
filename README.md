

# Mail In One plugin

This is the **Mail In One** plugin developed by treaction ag.

## Requirements

This is a plugin for [plentymarkets 7](https://www.plentymarkets.com). No other plugins are required for running the plugin.

## Installing

For detailed information about plugin provisioning refer to [plentymarkets developers](https://developers.plentymarkets.com/dev-doc/basics#plugin-provisioning).

## Plugin documentation

- Overview of plentymarkets [plugin interfaces](https://developers.plentymarkets.com/dev-doc/basics#guide-interface)
- The plentymarkets [REST API](https://developers.plentymarkets.com/rest-doc/introduction)

## Join our community

Sign up today and become a member of our [forum](https://forum.plentymarkets.com/c/plugin-entwicklung). Discuss the latest trends in plugin development and share your ideas with our community.

## Versioning

Visit our forum and find the latest news and updates in our [Changelog](https://forum.plentymarkets.com/c/changelog?order=created).

## License

This project is licensed under the Apache  LICENSE - see the [LICENSE.md](/LICENSE.md) file for details.
