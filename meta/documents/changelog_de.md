# Release Notes für Mail In One

## v3.0.3 (2019-07-12)

#### Behoben

- Screenshots angepasst

## v3.0.2 (2019-07-09)

#### Behoben

- Mehrere Bug fixes wurden durchgeführt
- An-/Ausschaltbares Error-/Info Logging über Synchronisationsläufe

## v3.0.1 (2019-05-09)

#### Behoben

- Mehrere Bug fixes wurden erfolgreich durchgeführt

## v3.0.0 (2019-04-26)

#### Hinzugefügt

- Neues Event "Bestell-Event" hinzugefügt

## v2.2.1 (2019-04-17)

#### Behoben

- Mehrere Bug fixes wurden erfolgreich durchgeführt

## v2.1.1 (2019-03-13)

#### Behoben

- Mehrere Bug fixes wurden erfolgreich durchgeführt

## v2.1.0 (2018-10-19)

#### Hinzugefügt

- Button Reset Sync hinzugefügt um Synchronisation zurückzusetzen.
- Die Abgemeldeten Kontakte wurden nicht synchronisiert.


## v2.0.1 (2018-07-18)

#### Geändert

- Der Slogan "Online mehr Geschäft" wurde aus dem Plugin-Namen entfernt.
- Das Änderungsprotokoll wurde aktualisiert.


## v2.0.0 (2018-07-11)

#### Hinzugefügt

- Button hinzugefügt für direkten Wechsel von plenty zur Mail-In-One Oberfläche
- Synchronisation-Ansicht hinzugefügt unter CRM -> Newsletter -> Mail-In-One -> Synchronisation
- Kontaktfilter-Ansicht hinzugefügt unter CRM -> Newsletter -> Mail-In-One -> Kontaktfilter
- Ereignisse Ansicht hinzugefügt unter CRM -> Newsletter -> Mail-In-One -> Ereignisse
- Neues Feature hinzugefügt - Manuele Synchronisation von Kontakten
- Neues Feature hinzugefügt - Manuele Synchronisation von Kontakten und Ereignissen 
- Neues Feature hinzugefügt - Neue Ereignisse anlegen
- Steuerung von Marketing Automation Strecken über plenty Oberfläche
- Steuerung von DOI-Mailings über plenty Oberfläche
- Kontaktfelder in Mail-In-One für die plenty Ordner:
    - Kunde
    - Interessent 
    - Interessent ohne DOI 

#### Geändert

- Kontaktsynchronisation alle 15 Minuten

#### Behoben

- Mehrere Bug fixes wurden erfolgreich durchgeführt



## v1.1.2 (2018-02-26)

#### Behoben

- Datentyp ändern.

## v1.1.1 (2018-02-26)

#### Behoben

- composer löschen.

## v1.1.0 (2018-02-19)

#### Behoben

- Die Schreibweise der Klasse auf UpperCamelCase ändern.

#### Geändert

- PlugIn wird unter System -> CRM -> Newsletter -> MailInOne anzeigt.

## v1.0.1 (2018-01-19)

#### Behoben

- Ein Bugfix wurde erfolgreich durchgeführt.

## v1.0.0 (2017-12-29)

### Hinzugefügt

- Initiale Plugin-Dateien hinzugefügt

