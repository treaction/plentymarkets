# Release Notes for Mail In One

## v3.0.3 (2019-07-12)

#### Fixed

- Screenshots customized

## v3.0.2 (2019-07-09)

#### Fixed

- Several Bug fixes have been implemented
- On/off switchable error/ info logging for synchronization runs

## v3.0.1 (2019-05-09)

#### Fixed

- Several Bug fixes have been implemented

## v3.0.0 (2019-04-26)

#### Changed

- New event "Order-Event" added

## v2.2.1 (2019-04-17)

#### Fixed

- Several Bug fixes have been implemented

## v2.1.1 (2019-03-13)

#### Fixed

- Several Bug fixes have been implemented

## v2.1.0 (2018-10-19)

#### Changed

- Button Reset Sync added to restore the synchronisation.
- Unsubscribed contacts have not been synchronised.


## v2.0.1 (2018-07-18)

#### Changed

- The Slogan "Online mehr Geschäft" is removed from Plugin Name.
- Updated the changelog.

## v2.0.0 (2018-07-11)

#### Added

- Button added with redirect to the Mail-In-One interface
- Synchronisation view added under CRM -> Newsletter -> MailInOne -> Synchronisation
- Contact filter view added under CRM -> Newsletter -> MailInOne -> Kontaktfilter
- Events view added under CRM -> Newsletter -> MailInOne -> Ereignisse
- New feature added - manual synchronisation of contacts
- New feature added - manual synchronisation of contacts and events
- New feature added - Create new events
- Control Mail-In-One Marketing Automation through the plenty interface
- Control DOI-Mailings through the plenty interface
- Contact fields in Mail-In-One of the plenty folders:
    - Kunde
    - Interessent
    - Interessent ohne DOI
    
#### Changed

- Contact synchronisation every 15 minutes

#### Fixed

- Several Bug fixes have been implemented

## v1.1.2 (2018-02-26)

#### Fixed

- Change Data type.

## v1.1.1 (2018-02-26)

#### Fixed

- Delete Composer.

## v1.1.0 (2018-02-19)

#### Fixed

- Change the spelling of the class to UpperCamelCase.

#### Changed

- plugIn is displayed under System -> CRM -> Newsletter -> MailInOne.

## v1.0.1 (2018-01-19)

#### Fixed

- A bugfix has been successfully completed.

## v1.0.0 (2017-12-29)

### Added

- Added initial plugin files