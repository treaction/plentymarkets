
# User Guide für das Mail-In-One-Plugin

<div class="container-toc"></div>

## Benutzerhandbuch

**Mail-In-One** ist eine Enterprise E-Mail-Marketing-Lösung, die auf die Bedürfnisse von Online Shops zugeschnitten wurde. In über 11.000 Kampagnen konnten wir über Leads für unsere Kunden generieren (Stand April 2019). Unsere Mission „Online mehr Geschäft“ für Ihren Shop.

Der Leistungsumfang im Überblick

**Marketing Automation**

   - Willkommen-Strecken für Neukunden & Interessenten
   - Kundenrückgewinnungskampagnen
   - Bestellbestätigungen
   - Versandbestätigungen
   - Bewertungen und Feedback
   - Warenkorb-Abbrecher
   - Individuelle Automatisierung von Marketingprozessen

**Newsletter-Marketing**

   - responsive E-Mail-Vorlagen zur Auswahl
   - Intuitiver E-Mail-Designer
   - Einfache Zielgruppenbildung mit Kontaktfiltern
   - Kontakt - und Listenmanagement
   - Easy Step by Step Kampagnenplaner
   - Umfangreiche Analysen und Reporting

**plentymarkets Integration**

   - einfache Aktivierung des Mail-In-One Zugangs
   - Direkter Absprung zu Mail-In-One Webanwendung
   - Automatische Synchronisation von Kontakten und Ereignissen
   - Steuerung der Marketing Programme mit dem plenty Ordnerkonzept
   - Unterstützung von Double Opt-In, Kontaktereignissen, Marketingprogrammen
   - Visualisierung der Synchronisation von Kontakten mit Aktionen
   - Anzeige der Kampagnen, Verteilerlisten und Kontaktfilter
   - Möglichkeit Logging für Support über Standardeinstellungen zu aktivieren
   - Schnelle Bereitstellung
   - Kostenlose Erstberatung


Sie bekommen außerdem eine **kostenlose Erstberatung** zum Thema E-Mail-Marketing / Marketing Automation und Produktdemo. Sie können die Lösung im Anschluss 30 Tage kostenfrei und unverbindlich testen.

## Mail-In-One für Ihren plentymarkets-Shop einrichten

Sie finden uns in Ihrem Shopsystem unter: **System -> CRM -> Newsletter -> MailInOne -> Kontoeinstellungen**.Sollten Sie noch kein eigenes Mail-In-One Konto verfügen, können sie hier dieses beantragen. Dazu klicken Sie bitte einfach auf Neues **Mail-In-One Konto anlegen**.

## Neues Mail-In-One Konto anlegen

Sollten Sie noch kein eigenes Mail-In-One Konto besitzen, so klicken Sie auf „Neues Mail-In-One Konto anlegen“ und tragen die benötigten Informationen ein. Wir benötigen alle mit * Stern gekennzeichneten Felder für die Einrichtung ihres kostenlosen Testzugangs. Sie haben eine kostenfreie 30/90 Tage Testphase.

<table>
<thead>
		<th>
			Einstellung
		</th>
		<th>
			Erläuterung
		</th>
	</thead>
	<tbody>
      <tr>
         <td>Vorname</td> 
         <td>Vorname des Testnutzers</td>      
      </tr>
     <tr>
         <td>Nachname</td> 
         <td>Nachname des Testnutzers</td>       
      </tr>
      <tr>
         <td>Firma</td> 
         <td>Name der Firma</td>       
      </tr>
      <tr>
         <td>Telefonnummer</td>  
         <td>Telefon unter der wir sie tagsüber erreichen. Sie bekommen eine kostenfreie Erstberatung und Einweisung per Telefon</td>      
      </tr>
      <tr>
         <td>PLZ</td>  
         <td>PLZ ihrer Firma</td>      
      </tr>
      <tr>
         <td>Ort</td>
         <td>Ort ihrer Firma</td>        
      </tr>
      <tr>
         <td>Land</td> 
         <td>Land ihrer Firma</td>       
      </tr>
      <tr>
         <td>E-Mail-Adresse</td> 
         <td>E-Mail für die Zusendung der Zugangsdaten</td>       
      </tr>
</tbody>
</table>

Klicken Sie im Anschluss auf „speichern“. Sie erhalten binnen 24 Stunden Ihren kostenfreien 30/90 Tage Testzugang.

## Vorhandenes Mail-In-One verbinden

Sollten Sie bereits einen eigenen Mail-In-One Account haben, geben Sie hier Ihren **API-Key** ein. Diesen finden Sie in Ihrem Mail-In-One Account unter **Einstellungen -> API KEY**. 

Kopieren Sie den API KEY vollständig und fügen diesen in das vorgesehene Feld ein. Klicken Sie im Anschluss auf „speichern“. 

<table>
<thead>
		<th>
			Einstellung
		</th>
		<th>
			Erläuterung
		</th>
	</thead>
	<tbody>
      <tr>
         <td>API-Key</td> 
         <td>Falls sie bereits ein Konto haben, tragen Sie bitte hier ihren API-Key ein.</td>      
      </tr>
</tbody>
</table>

Bei Rückfragen stehen wir Ihnen jederzeit zur Verfügung.

## Sprung zu Mail-In-One

Jeder Kunde von Mail-In-One bekommt einen individuellen Link zur Anmeldung, Sie können einmalig den Link zur Anmeldung hinterlegen. Im Anschluss können Sie bequem über den Button „Mail-In-One“ zur Webanwendung von Mail-In-One wechseln. Es öffnet sich ein neuer Tab in ihrem Browser mit ihrer Anmeldeseite.

<table>
<thead>
		<th>
			Einstellung
		</th>
		<th>
			Erläuterung
		</th>
	</thead>
	<tbody>
      <tr>
         <td>Login-URL</td> 
         <td>Link zur Anmeldeseite in Mail-In-One</td>      
      </tr>
</tbody>
</table>

## Kampagnen 

Nach der Einrichtung werden Ihre Kampagnen aus Mail-In-One in plentymarkets angezeigt. Weitergehende Informationen wie Öffner, Klicks finden sie in Mail-In-One. Mit der Kampagnen-Id können sie ihre Kampagne einfach in Mail-In-One suchen und finden.

<table>
<thead>
		<th>
			Übersicht
		</th>
		<th>
			Erläuterung
		</th>
	</thead>
	<tbody>
      <tr>
         <td>ID</td> 
         <td>Die ID Ihrer Kampagne</td>      
      </tr>
 <tr>
         <td>Name der Kampagne</td> 
         <td>Der Name der in Mail-In-One hinterlegt wurde</td>      
      </tr>
 <tr>
         <td>Status</td> 
         <td>Bezeichnet den Status der Kampagne 
             •	Draft: Entwurf
             •	Released: Aktive Trigger Mailings
             •	Done: Verschickt
        </td>      
      </tr>
 <tr>
         <td>Typ</td> 
         <td>Beschreibt den Typ der Kampagne
             •	Regular: Normale eMail Kampagne
             •	Trigger: Trigger Mailing
             •	doi: Double Opt In Versand
         </td>      
      </tr>
 <tr>
         <td>Versendet</td> 
         <td>Anzahl der Empfänger</td>      
      </tr>
 <tr>
         <td>Zeitpunkt</td> 
         <td>Zeitpunkt der Versendung</td>      
      </tr>
 <tr>
         <td>Öffner</td> 
         <td>Unique Öffner</td>      
      </tr>
 <tr>
         <td>Öffnungen</td> 
         <td>Gesamte Anzahl der Öffnungen</td>      
      </tr>
 <tr>
         <td>Öffnungsrate</td> 
         <td>Prozentuale Rate Ihrer Öffnungen</td>      
      </tr>
 <tr>
         <td>Klicker</td> 
         <td>Unique Klicker</td>      
      </tr>
 <tr>
         <td>Klicks</td> 
         <td>Gesamte Anzahl der Klicks</td>      
      </tr>
 <tr>
         <td>Klickrate</td> 
         <td>Prozentuale Rate Ihrer Klicks</td>      
      </tr>
 <tr>
         <td>Abmelder</td> 
         <td>Anzahl der Abmelder in diesem Mailing</td>      
      </tr>
 <tr>
         <td>Hard- & Softbounces</td> 
         <td>Anzahl der Mails die nicht zugestellt werden konnten</td>      
      </tr>

</tbody>
</table>

### Verteilerlisten und Kontaktfilter

In Mail-In-One können sie mehrere Verteilerlisten und Kontaktfilter anlegen. Jede Kampagne basiert auf einer Verteilerliste. Die Verteilerliste nutzt Kontaktfilter zur Filterung und Segmentierung ihres Verteilers. Sie können frei definierbare Marketingattribute für ihren Verteiler definieren. Bei der Filterung können sie auf eine Vielzahl von Informationen zurückgreifen wie Response-Informationen wie z.B. Öffnungen/Klicks, Kontaktereignisse und uvm. zurückgreifen. Die Kontaktfilter sind wiederverwendbar und können nach Belieben kombiniert werden.

Nach der Einrichtung werden Ihre Kontaktfilter und Verteilerlisten auf der plentymarkets Oberfläche angezeigt. Sobald Sie eine neue Verteilerliste oder neuen Kontaktfilter anlegen, wird dieser automatisch mit dem System synchronisiert.

## Synchronisation

Die Mail-In-One Plug In synchronisiert regelmäßig und automatisch neue oder geänderte Kontakte. Jeder Austausch von Daten wird in der Ansicht „Synchronisation“ visualisiert. Die Ansicht zeigt folgende Informationen an.

## Kontaktmanagement

Mit der Aktivierung ihres Konto werden Ihre Kontakte automatisch zwischen plentymarkets und ihrem Mail-In-One-Konto synchronisiert. Sobald neue Kontakte in plentymarkets hinzukommen werden diese einmal pro Tag automatisch in Ihr Mail-In-Konto übertragen.

<table>
<thead>
		<th>
			Einstellung
		</th>
		<th>
			Erläuterung
		</th>
	</thead>
	<tbody>
      <tr>
         <td>Letzte Synchronisation am</td> 
         <td>Zeitstempel der Synchronisation</td>      
      </tr>
<tr>
         <td>Synchronisierte Kontakte</td> 
         <td>Anzahl der nach Mail-In-One synchronisierten Kontakte</td>      
      </tr>
<tr>
         <td>Ausgelöste Kontakt Ereignisse</td> 
         <td>Anzahl der in Mail-In-One ausgelösten Kontakte Ereignisse </td>      
      </tr>
<tr>
         <td>Gestartete Marketing Automation</td> 
         <td>Anzahl der gestarteten Marketing Programme in Mail-In-One</td>      
      </tr>
<tr>
         <td>Ausgelöste DOI</td> 
         <td>Anzahl der gesendeten Double-Opt-In Mails</td>      
      </tr>
</tbody>
</table>

Neben der automatischen Synchronisation gibt es die Möglichkeit über die Button **„Kontakte synchronisieren“** und **„Kontakte und Ereignisse synchronisieren“** manuell die Übertragung der Kontakte auszulösen. Diese Funktionen sind insbesondere beim Testen von neuen oder geänderten Marketing Programmen von Vorteil. Der Button **„Kontakte synchronisieren“** übertragt nur neue oder geänderte Kontakte. Es werden jedoch nicht die in der Ansicht **„Ereignisse“** definierten Aktionen wie Double Opt-In, Kontaktereignisse oder Marketing Programme gestartet. Der zweite Button **„Kontakte und Ereignisse synchronisieren“** aktualisiert Kontakt und löst die definierten Ereignisse aus.

Es gibt auch die Möglichkeit über den Button **„Rest Sync“** die Synchronisation zurückzusetzen. 
Die Abgemeldeten Kontakte wurden nicht synchronisiert.

## Kontaktereignisse

plentymarkets bietet mit dem Ordner-Konzept eine flexible Option Newsletter-Empfänger zu gruppieren. Ein Newsletter-Empfänger kann in mehreren Ordern enthalten sein. Beispiel ein Online Shop verkauft über eBay und direkt über den eigenen Online Shop. Er legt zwei Ordner an „eBay“ und „Online Shop“. Sobald ein Kunde das erste Mal über einen der beiden Wege gekauft hat, wird er dem passenden Ordner hinzugefügt. Ein Kunde kann allerdings bereits sowohl bei eBay als auch im eigenen Online Shop gekauft haben. In diesem Falle wäre der Kunde in den beiden Ordner „eBay“ und „Online Shop“ enthalten.

Für jeden plentymarkets-Ordner kann in der Ansicht „Ereignisse“ individuell definiert werden wie Mail-In-One reagieren soll.

In Mail-In-One definierte Kontaktereignisfelder müssen sein: ContactId (Datentyp: ganze Zahl), Firstname (Datentyp: Text), Lastname (Datentyp: Text), FolderName (Datentyp: Text). 

<table>
<thead>
		<th>
			Einstellung
		</th>
		<th>
			Erläuterung
		</th>
	</thead>
	<tbody>
      <tr>
         <td>ID</td> 
         <td>Fortlaufende Nummer für Ereigniskonfiguration</td>      
      </tr>
      <tr>
         <td>Event</td> 
         <td>Art des Ereignisses am Kontakt – Es wird „Neuer Kontakt“ und „Interessent unterschieden“</td>      
      </tr>
      <tr>
         <td>Ordner</td> 
         <td>Ereigniskonfiguration wird nur ausgeführt, wenn der Kontakt sich in dem passenden Ordner befindet</td>      
      </tr>
       <tr>
         <td>Integration</td> 
         <td>Art der Integration wird hier definiert. Es wird unterschieden Kontaktereignis, DOI senden, Marketing Program. Es ist möglich für eine bestimmte Kombination von plenty Ordner und Event mehrere Arten von Integration auszulösen. Beispielsweise sollen verschiedene Marketing Programme oder Kontaktereignisse gestartet werden. In dem Falle müssen mehrere Einträge in die Konfigurationstabelle gemacht werden.</td>      
      </tr>
       <tr>
         <td>Kontaktereignis</td> 
         <td>Auswahl eines in Mail-In-One definierten Kontaktereignisses. Kontaktereignisse können einzelne Transaktionsmails auslösen anstatt kompletter Marketing Automation Programme.</td>      
      </tr>
 <tr>
         <td>Marketing Automation</td> 
         <td>Nummer des zu startenden Marketing Automation Programms in Mail-In-One. Mail-In-One erlaubt die graphische Modellierung komplexer Marketign Programme mit einem einfachen Werkzeug.</td>      
      </tr>
 <tr>
         <td>ADOI Mailing</td> 
         <td>Auswahl des gewünschten DOI Mailings. Es können in Mail-In-One mehreren DOI Mailings hinterlegt werden. Beispielsweise für die Newsletter-Anmeldung im Online Shop und zusätzlich ein eigenes DOI für eine laufende Kampagne.</td>      
      </tr>
 <tr>
         <td>DOI erforderlich</td> 
         <td>Für Kontakte die sich nur in diesem Ordner befinden muss ein DOI vorliegen. Die Kontakte werden synchronisiert. Allerdings werden Sie in Mail-In-One nicht aktiv. Sie erhalten keine E-Mails.</td>      
      </tr>
 <tr>
         <td>Löschen</td> 
         <td>Löschen der Ereigniskonfiguration</td>      
      </tr>
<tr>
         <td>Edit</td> 
         <td>Bearbeiten der Ereigniskonfiguration</td>      
      </tr>
</tbody>
</table>

## Bestellereignisse

Neu ab der Version 3.0.0 sind die Bestellereignisse. Sie können hier plentymarkets Status-Ereignisse mit Mail-In-One Ereignissen verknüpfen und so ein Kontaktereignis oder eine Marketing automation auslösen.

Bevor Sie aber hier Einstellungen vornehmen können, müssen Sie zunächst in Ihrem plentysystem unter System-> Aufträge -> Ereignisse diese konfigurieren.
Geben Sie zunächst einen Eindeutigen Namen, um das Ereignis später zuordnen zu können. 

Aufgrund der API die von plenty bereitgestellt wird, sind die Aktionen die Sie auswählen können eingeschränkt. Bitte wählen Sie **“Neuer Auftrag”** bzw **“Statuswechsel”** aus. Unter Statuswechsel stehen Ihnen sämtliche Auswahlmöglichkeiten zur Verfügung.

Haben Sie Ihr Ereignis ausgewählt, fügen Sie unter **“Aktionen”** die dazu passende Mail-In-One Aktion aus. Achten Sie bitte darauf, dass die Statusnummer mit der in Mail-In-One übereinstimmen muss! 
 
Haben Sie Ihr Ereignis angelegt, können Sie nun in Ihrem Mail-In-One Plugin unter **“Bestellereignisse”** ein neues Event anlegen, klicken Sie dazu auf das Plus Symbol. 
Auch hier vergeben Sie bitte ein Eindeutigen Namen.

Unter **“PLENTY EREIGNIS”** wählen Sie bitte das gewünschte Ereignis aus, idealerweise das was sie vorher angelegt haben. Wählen Sie dann das dazu passenden Mail-In-One Ereignis aus.  

Unter **“INTEGRATION”** können Sie zwischen Kontakt Ereignis und Marketing Programm wählen. Mit **Kontaktereignis** können Sie eine Triggermail versenden, die Sie zuvor in Ihrem Mail-In-One System angelegt haben. Achten Sie bitte darauf, dass sie bevor Sie das Mailing auf Aktiv schalten, Sie unter “Versandlogik & Freigabe” bei “Typ” “Einzelversand bei Ereignis” auswählen und dort das angelegte Ereignis zuordnen. Damit die Daten korrekt an das Trigger Mailing übergeben werden können, müssen im Kontaktereignis innerhalb von Mail-In-One folgende Datenfelder hinterlegt sein:

<table>
<thead>
		<th>
			Bezeichnung
		</th>
		<th>
			Format
		</th>
	</thead>
	<tbody>
      <tr>
         <td>gender</td> 
         <td>Text</td>      
      </tr>
<tr>
         <td>firstName</td> 
         <td>Text</td>      
      </tr>
<tr>
         <td>lastName</td> 
         <td>Text</td>      
      </tr>
<tr>
         <td>contactId</td> 
         <td>Ganze Zahl</td>      
      </tr>
<tr>
         <td>orderId</td> 
         <td>Ganze Zahl</td>      
      </tr>
<tr>
         <td>Products</td> 
         <td>JSON</td>      
      </tr>
</tbody>
</table>

Für Products sind folgende Eigenschaften hinterlegt: **id , quantity, orderItemName, currency, priceGross, netTotal, grossTotal**

Innerhalb des Kontaktereignis sieht der JSON Part folgendermaßen aus:

"Products":[{"quantity":1,"orderItemName":"Barhockerl Fury gepolstert","currency":"EUR","id":333,"priceGross":79},{"netTotal":70.58},{"grossTotal":83.99}]

Haben Sie Kontaktereignis als Integration ausgewählt, müssen Sie im darunterliegenden Feld das entsprechende Ereignis auswählen. Marketing Programm ID muss in diesem Fall nicht ausgefüllt werden.

Wählen Sie stattdessen **Marketing Programm** wird der Kontakt nach dem Auslösen in eine Marketing Automation geleitet, die sie ebenfalls zuvor Angelegt haben. Um die entsprechende Automation zuordnen zu können, müssen Sie bei “Marketing Programm ID” die ID der Automation hinterlegen, die ausgeführt werden soll. Diese ID entnehmen sie bitte der URL, wenn sie eine aktive Automation öffnen. Die ID finden sie dann in diesem Part der URL “processId=##” weit hinten an der URL. 
Hier Müssen Sie kein Kontaktereignis auswählen. 


Haben Sie alle Einstellungen vorgenommen, lassen Sie den Status auf “Aktiv” und speichern Ihr Event ab.

In der Bestellereignisse Übersicht sehen sie alle bereits erstellten Events und ebenfalls Ihren aktuellen Status. Von hier aus können Sie diese Löschen oder über “Edit” die Einstellungen nochmals anpassen. 

<table>
<thead>
		<th>
			Einstellung
		</th>
		<th>
			Erläuterung
		</th>
	</thead>
	<tbody>
      <tr>
         <td>ID</td> 
         <td>Fortlaufende Nummer des Bestellevents</td>      
      </tr>
<tr>
         <td>plenty Ereignis</td> 
         <td>Startpunkt in plenty, der ein Mail-In-One Ereignis auslösen soll</td>      
      </tr>
<tr>
         <td>Mail-In-One Ereignis</td> 
         <td>Dieses Ereignis wird ausgesteuert, sobald daten übermittelt werden</td>      
      </tr>
<tr>
         <td>Integration</td> 
         <td>Art des Ereignisses: Es stehen zwei zur Auswahl. Kontaktereignis, welches ein Triggermailing auslöst und Marketing Programm, welches ein Marketing Automation auslöst</td>      
      </tr>
<tr>
         <td>Kontaktereignis</td> 
         <td>Auswahl eines in Mail-In-One definierten Kontaktereignisses</td>      
      </tr>
<tr>
         <td>Marketing Automation ID</td> 
         <td>Nummer des zu startenden Marketing Automation Programms in Mail-In-One. Mail-In-One erlaubt die graphische Modellierung komplexer Marketing Programme mit einem einfachen Werkzeug</td>      
      </tr>
<tr>
         <td>Status</td> 
         <td>Gibt den Status des Events wieder</td>      
      </tr>
<tr>
         <td>Löschen</td> 
         <td>Löschen des Bestellereignisses</td>      
      </tr>
<tr>
         <td>Edit</td> 
         <td>Bearbeiten des Bestellereignisses</td>      
      </tr>
</tbody>
</table>

## Domain-Delegation

Die klare Empfehlung für gutes E-Mail-Marketing ist die Nutzung von Sub-Domänen für Ihre Newsletter und Transaktionsmails. Ihre Empfänger sollten direkt erkennen, das diese E-Mail von Ihnen gesendet wurde. Beispielsweise heißt ihre Shop-Seite https://mein-online-shop.de. Wir würden ihnen empfehlen, das Sie zwei Sub-Domänen anlegen. Ein Sub-Domäne für den Newsletter beispielsweise news.online-shop.de. Diese können Sie für Ihren Newsletter verwenden. Für die Transaktionsmail also E-Mails aus Willkommen-Strecken, Rund um Bestellungen usw. empfehlen wir die Anlage einer weiteren Sub-Domäne beispielsweise order.online-shop.de.

Ihre Domänen-Verwaltung finden Sie unter dem roten Plenty Logo – Service Center. Gehen Sie dort in die Domänen-Verwaltung. Legen Sie die Sub-Domänen an und tragen Sie für jede angelegte Sub-Domäne die beiden Mail-In-One Name-Server ein:

    - ns0.isprit2.de
    - ns1.isprit2.de

Gerne unterstützen wir Sie bei der Anlage und Delegation der Sub-Domäne an Mail-In-One. Einfach kurze E-Mail an campaign@treaction.net

## CRM Kundendaten plentymarkets

**Newsletter-Checkbox**

Für die Synchronisation Ihrer Kunden von plentymarkets nach Mail-In-One muss die Checkbox „Newsletter“ gesetzt sein. Sie können über Automatisierungsregeln für die verschiedenen Konstellationen automatisch sicherstellen, das die Checkbox gesetzt wird. Falls „Newsletter“ am Kunden nicht aktiviert wurde, so wird er automatisch bei Änderung aus den plentymarkets-Ordner für die Newsletter geslöscht.

**Gruppierung der Kunden in Ordnern**

Plentymarkets bietet mit dem Ordner-Konzept für Newsletter eine flexible Funktion zur Gruppierung von Kunden. Sie können über entsprechende Suchkriterien Ihre Kunden auswählen und alle gebündelt in einen Ordner schieben. Mail-In-One synchronisiert die Zuordnung der Kunden zu den Ordnern. Sie können diese Ordner dann zur Selektion in Mail-In-One nutzen. Ordner könnte beispielsweis genutzt werden zur Abbildung über welche Plattform der Kunde gekauft hat wie Amazon, eBay, eigener Online Shop. Eine weitere Anwendung ist die Segmentierung der Kunden nach Produktgruppen in denen der Kunden gekauft hat wie zum Beispiel Herrenbekleidung, Damenbekleidung, Sportausrüstung.

## Logging für Support aktivieren

Falls vom Support angeraten, können Sie ein temporäres Logging der MailInOne-Synchronisationsläufe aktivieren. Klicken Sie dazu auf der Plentymarkets-Oberfläche Daten -> Log und klicken Sie auf das Zahnrad. Es öffnet sich ein Popup-Fenster. Im Popup-Fenster wählen Sie MailInOne. Es erscheinen Konfigurationseinstellungen auf der rechten Seite im Popup. Wählen Sie die Checkbox „Aktiv“,  Dauer = 1 Tag, LogLevel „Info“ oder „Error“. Achtung: Aktiveren Sie das Logging auf Level „Info“ nur auf Aufforderung durch den Support: Bei hohem Datenvolumen kann das Logging erhebliche Speicherressourcen in Anspruch nehmen.


## Über die treaction AG 

Wir sind Experten für Lead-Generierung, Marketing Automation und E-Mail-Marketing. Unsere Kunden sind kleine und mittelständische Unternehmen in Deutschland, Österreich und der Schweiz.

Durch Fokus und Erfahrung aus vielen erfolgreichen Kampagnen, schaffen wir für unsere Kunden zuverlässig mehr Umsatz. Nutzen Sie unsere umfassende Erfahrung aus tausenden Kampagnen für Ihren Erfolg im Online Marketing.

Die Prinzipien unseres Handelns sind Klarheit, Schnelligkeit und Leistungsorientierung für unsere Kunden.

Wir bieten verbinden moderne Technologie zu integrierten Lösung. Auf Wunsch können wir Traffic für Ihren Online Shop zu generieren. Unsere besondere Expertise liegt in der Conversion Rate Optimierung (CRO). 

Unsere ganzheitlicher Ansatz bedeutet für Sie entscheidende Vorteile:

  - Nachweislich mehr Umsatz aus Ihrem Newsletter-Marketing
  - Steigerung der Newsletter-Anmeldungen in Ihrem Online Shop
  - Mit CRO schaffen wir höhere Umsätze bei gleichen Marketing-Budget
  - Generierung von neuen Leads über Online Marketing
  - Steigerung der Wiederkäuferquote durch Willkommen- oder Bestellstrecken
  - Automatisierte Kundenrückgewinnungskampagnen

Wir beraten Sie gerne unverbindlich und kostenfrei zu Ihren Möglichkeiten. Rufen Sie uns an unter +49(561)997923-60 oder senden Sie uns eine E-Mail an campaign@treaction.net 






