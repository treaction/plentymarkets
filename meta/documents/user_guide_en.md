
# User Guide for the Mail-In-One-Plugin

<div class="container-toc"></div>

## User manual

**Mail-In-One** is an enterprise email marketing solution, specially catered towards the needs of modern online shops. We have generated over 150.0000 Leads through 11.000 campaigns for our clients (April 2019). **“more online business”** is our mission for your online shop. 

Scope of our services

**Marketing Automation**

   - Welcome workflow for new – and potential customers
   - Customer win-back campaigns
   - Order confirmation
   - Delivery confirmation
   - Ratings and feedback
   - Shopping basket cancellation workflow
   - Individual designed automation of marketing processes 


**Newsletter-Marketing**

   - Range of responsive email-templates
   - Intuitive email designer
   - Simple target group segmentation with contact filters
   - Contact – and list-management
   - Easy step by step campaign planning
   - Extensive assortment of analysis and reports


**plentymarkets Integration**

   - Uncomplicated Mail-In-One access activation
   - Direct redirect to the Mail-In-One web application
   - Automatic contact – and event synchronisation 
   - Marketing programmes controlled through the plenty folder concept
   - Support of Double Opt-In contact events and marketing programmes 
   - Visualised synchronisation of contacts with actions 
   - Ability to enable logging for support via default settings
   - Fast provisioning
   - Free initial consultation


Additionally, plentymarkets customers receive a free initial consultation for email-marketing / marketing automation as well as a product demo. You can test our solution for 30 days free of charge and without any further obligations.

## Setting up Mail-In-One for your plentymarkets shop

You can find the Mail-In-One plugin in your plentymarkets shop under: **System -> CRM -> Newsletter -> MailInOne ->Kontoeinstellungen**. After a click on the button System, which is in the upper left corner, open **CRM » Newsletter » MailinOne » Kontoeinstellungen.**

## Create a new Mail-In-One Account

If you do not yet have your own Mail-In-One account, click on "Create new Mail-In-One account" and enter the required information. We need all fields marked with an asterisk (*) to set up your free trial account. You have a free 30 day trial period.

<table>
<thead>
		<th>
			Setting
		</th>
		<th>
			Description
		</th>
	</thead>
	<tbody>
      <tr>
         <td>Vorname</td> 
         <td>First name of your test user</td>      
      </tr>
     <tr>
         <td>Nachname</td> 
         <td>Last name of your test user</td>       
      </tr>
      <tr>
         <td>Firma</td> 
         <td>Name of your company</td>       
      </tr>
      <tr>
         <td>Telefonnummer</td>  
         <td>Please enter the phone number where we can contact you during office hours. You will get a free initial consultation via telephone</td>      
      </tr>
      <tr>
         <td>PLZ</td>  
         <td>Postal code of your company</td>      
      </tr>
      <tr>
         <td>Ort</td>
         <td>City of your company</td>        
      </tr>
      <tr>
         <td>Land</td> 
         <td>Country of your company</td>       
      </tr>
      <tr>
         <td>E-Mail-Adresse</td> 
         <td>We will send your credentials for Mail-In-One to this email address</td>       
      </tr>
</tbody>
</table>

After entering all required data, please click “save”. Within 24 hours you will receive access to your free 30 days Mail-In-One trial.

## Use existing Mail-In-One Account

If you already have an existing Mail-In-One account, please enter your **API key**. You will find your API-Key in your Mail-In-One account under **Settings -> API key**.

Please copy the complete API key and paste it within the appropriate input field “API-Key”. Please click save afterwards.

<table>
<thead>
		<th>
			Setting
		</th>
		<th>
			Description
		</th>
	</thead>
	<tbody>
      <tr>
         <td>API-Key</td> 
         <td>If you have an existing Mail-In-One account, please enter your API key here</td>      
      </tr>
</tbody>
</table>

If you have a question or require support, don’t hesitate to contact us.

## Redirect to Mail-In-One

Every Mail-In-One client will receive an individual login link. You can save the link for direct login access to the Mail-In-One application. Moreover, you can easily change to the Mail-In-One web application through the Mail-In-One button. A new tab will open in your browser with the Mail-In-One login page.

<table>
<thead>
		<th>
			Setting
		</th>
		<th>
			Description
		</th>
	</thead>
	<tbody>
      <tr>
         <td>Login-URL</td> 
         <td>Link to the Mail-In-One Login Page</td>      
      </tr>
</tbody>
</table>


## Campaigns

After setup, your campaigns from Mail-In-One will be displayed in plentymarkets. Additional information such as email openers or clicks can be found in Mail-In-One. With the campaign id you can easily search and find your campaign in Mail-In-One.

<table>
<thead>
		<th>
			Übersicht
		</th>
		<th>
			Overview
		</th>
	</thead>
	<tbody>
      <tr>
         <td>ID</td> 
         <td>The Campaign ID</td>      
      </tr>
 <tr>
         <td>Name der Kampagne</td> 
         <td>The name that was defined in Mail-In-One</td>      
      </tr>
 <tr>
         <td>Status</td> 
         <td>sets the status of the campaign
             •	Draft
             •	Released: Active trigger mails
             •	Done: sent
        </td>      
      </tr>
 <tr>
         <td>Typ</td> 
         <td>Indicates the status of the campaign
             •	Regular: Normal email campaign
             •	Trigger: Trigger mailing
             •	DOI: Double Opt In mailing
         </td>      
      </tr>
 <tr>
         <td>Versendet</td> 
         <td>Number of recipients</td>      
      </tr>
 <tr>
         <td>Zeitpunkt</td> 
         <td>Time of mail delivery</td>      
      </tr>
 <tr>
         <td>Öffner</td> 
         <td>Unique openers</td>      
      </tr>
 <tr>
         <td>Öffnungen</td> 
         <td>Total number of openings</td>      
      </tr>
 <tr>
         <td>Öffnungsrate</td> 
         <td>Percentage rate of your openings</td>      
      </tr>
 <tr>
         <td>Klicker</td> 
         <td>Unique clicks</td>      
      </tr>
 <tr>
         <td>Klicks</td> 
         <td>Total number of clicks</td>      
      </tr>
 <tr>
         <td>Klickrate</td> 
         <td>Percent rate of your clicks</td>      
      </tr>
 <tr>
         <td>Abmelder</td> 
         <td>Number of unsubscribers</td>      
      </tr>
 <tr>
         <td>Hard- & Softbounces</td> 
         <td>Number of mails that could not be delivered</td>      
      </tr>

</tbody>
</table>

### Mailing list and contact filters

In Mail-In-One you can create multiple mailing lists and contact filters. Every campaign is based on a mailing list. The mailing list uses contact filters to segment your contact base, to define an individual target group. You can create custom marketing attributes “contact fields”. Contact filters can be defined through various information sources e.g. response information, opens/clicks or contact events. Contact filters are reusable and combinable.

After setting up your Mail-In-One account, your contact filters and mailing lists are displayed in your plentymarkets shop. When creating a new mailing list or contact filter, it will be automatically synchronised to the plentymarkets.

## Synchronisation

The Mail-In-One plugin regularly and automatically synchronises new or edited contacts. Every data exchange process is displayed in the „Synchronisation“ tab. Following information is displayed:

Besides the automatic synchronisation, there is the possibility with the button „Kontakte synchronisieren“ and „Kontakte und Ereignisse synchronisieren“ to manually start the contact syncronisation procedure. This function is particularly useful when testing new or edited marketing programmes. The Button „Kontakte synchronisieren“ transfers only new or edited contacts. However, the actions defined in the Events tab such as double opt-in, contact events or marketing programmes are not initiated.  The second Button „Kontakte und Ereignisse synchronisieren“refreshes the contacts and triggers any defined events.

You have the possibility to restore the synchronisation with the "Rest Sync" button. 
Unsubscribed contacts have not been synchronised.

## Contact Management

With the activation of your account, contacts are automatically synchronised between your plentymarkets shop and Mail-In-One account. All new contacts added to your plentmarkets shop are synchronised every 15 minutes to your Mail-In-One account. 

<table>
<thead>
		<th>
			Setting
		</th>
		<th>
			Description
		</th>
	</thead>
	<tbody>
      <tr>
         <td>Letzte Synchronisation am</td> 
         <td>Synchronisation time stamp</td>      
      </tr>
<tr>
         <td>Synchronisierte Kontakte</td> 
         <td>Number of contacts synchronised to Mail-In-One</td>      
      </tr>
<tr>
         <td>Ausgelöste Kontakt Ereignisse</td> 
         <td>Number of triggered contact events in Mail-In-On</td>      
      </tr>
<tr>
         <td>Gestartete Marketing Automation</td> 
         <td>Number of initiated marketing programmes in Mail-In-One</td>      
      </tr>
<tr>
         <td>Ausgelöste DOI</td> 
         <td>Number of sent Double-Opt-In Emails</td>      
      </tr>
</tbody>
</table>

## Contact Events

Plentymarkets offers a flexible option to segment newsletter recipients with its folder-concept. A newsletter recipient can be in multiple folders. For example, an online shop sells through eBay but also through its own standalone online shop. The Admin creates two folders „eBay“ and „Online Shop“. As soon as a customer buys through one of the ways, the user will be placed within the corresponding folder. A customer can however already have purchased through both ways. In this situation, the customer will be contained within both folders. 

For every plentymarkets folder, in the „Ereignisse“ tab, can individually be defined how Mail-In-One should operate. 

Contact event fields defined in Mail-In-One must be: ContactId (datatype: unsigned integer), Firstname (datatype: text), Lastname (datatype: text), FolderName (datatype: text).

<table>
<thead>
		<th>
			Setting
		</th>
		<th>
			Description
		</th>
	</thead>
	<tbody>
      <tr>
         <td>ID</td> 
         <td>Unique identity number of the event configuration</td>      
      </tr>
      <tr>
         <td>Event</td> 
         <td>Types of the contact events – distinction between „Neuer Kontakt“ and  „Interessent unterschieden“</td>      
      </tr>
      <tr>
         <td>Ordner</td> 
         <td>Event configuration is only initiated when the contact is in the correct folder</td>      
      </tr>
       <tr>
         <td>Integration</td> 
         <td>Integration type is defined here. It is distinguished in contact event, send DOI and marketing programme. For combinations of plenty folders and events it is possible to initiate various types of integrations. For example, multiple marketing programmes or contact events are to be started. In this situation, multiple entries must be made in the configuration table.</td>      
      </tr>
       <tr>
         <td>Kontaktereignis</td> 
         <td>Selection of a Mail-In-One defined contact event. Contact events can trigger individual transaction mails instead of complete marketing automation programs.</td>      
      </tr>
 <tr>
         <td>Marketing Automation</td> 
         <td>Number of the marketing automation program to start in Mail-In-One. Mail-In-One allows the graphical modelling of complex marketing programs with a simple tool.</td>      
      </tr>
 <tr>
         <td>ADOI Mailing</td> 
         <td>Selection of the desired DOI mailing. Several DOI mailings can be stored in Mail-In-One. For example, for a newsletter registration in your online shop and additionally a DOI mailing for an ongoing campaign.</td>      
      </tr>
 <tr>
         <td>DOI erforderlich</td> 
         <td>For contacts in this folder, a DOI must be present. The contacts are synchronised. However, they are not active in Mail-In-One. These contacts will not receive emails.</td>      
      </tr>
 <tr>
         <td>Löschen</td> 
         <td>Delete the event configuration.</td>      
      </tr>
<tr>
         <td>Edit</td> 
         <td>Edit the event configuration.</td>      
      </tr>
</tbody>
</table>

## Order - Events

New from version 3.0.0 are the order events. Here you can associate the plentymarkets status events with Mail-In-One events to trigger a contact event or marketing automation.

Before you can make any settings here, you must first configure them in your plentysystem under System-> Jobs -> Events. Enter a unique name to be able to assign the event later.

The API provided by plentymarkets only allows limited the actions. Please select **"Neuer Auftrag"** or **"Statuswechsel"**. Under status change you have all options available.

Once you've selected your event, under Actions, add the appropriate Mail-In-One action. Please make sure that the status number matches the one in Mail-In-One.
When you have created your event, you can create a new event under "Order Event" in your Mail-In-One Plugin by clicking on the plus (+) icon. Again, please give it a clear name.

Under **“PLENTY EREIGNIS”** please select the desired event, ideally the one you created before. Then select the appropriate Mail-In-One event.

Under **"INTEGRATION"** you can choose between contact events and marketing programs. A **contact event** lets you send a trigger mail that you have previously created in your Mail-In-One system. 

Before you activate the mailing please make sure to go to **“Versandlogik & Freigabe”** under **"Typ"**, **"Individual dispatch on event"** and assign the created contact event there. If you have selected Contact event as integration, you must select the appropriate event in the field below. 

<table>
<thead>
		<th>
			Bezeichnung
		</th>
		<th>
			Format
		</th>
	</thead>
	<tbody>
      <tr>
         <td>gender</td> 
         <td>Text</td>      
      </tr>
<tr>
         <td>firstName</td> 
         <td>Text</td>      
      </tr>
<tr>
         <td>lastName</td> 
         <td>Text</td>      
      </tr>
<tr>
         <td>contactId</td> 
         <td>Ganze Zahl</td>      
      </tr>
<tr>
         <td>orderId</td> 
         <td>Ganze Zahl</td>      
      </tr>
<tr>
         <td>Products</td> 
         <td>JSON</td>      
      </tr>
</tbody>
</table>

The following properties are stored for Products: **id , quantity, orderItemName, currency, priceGross, netTotal, grossTotal**

"Products":[{"quantity":1,"orderItemName":"Barhockerl Fury gepolstert","currency":"EUR","id":333,"priceGross":79},{"netTotal":70.58},{"grossTotal":83.99}]

If you select **Marketing Programm**, the contact will be redirected into a Marketing Automation flow that you created earlier. In order to be able to assign the corresponding automation, you must enter the ID of the automation to be executed in the **"Marketing Programm ID"**. Please take this ID from the URL when opening an active automation. You will find the ID in this part of the URL "processId = ##" at the end of the URL. Here you are not required to select a contact event.

Once you have configured all your settings, leave the status on "active" and save your event.

In the **order event overview**, you can see all created events and their current status. Here you can delete or adjust the settings of an event with **"Edit"**.

<table>
<thead>
		<th>
			Setting
		</th>
		<th>
			Description
		</th>
	</thead>
	<tbody>
      <tr>
         <td>ID</td> 
         <td>Sequential number of the order event</td>      
      </tr>
<tr>
         <td>plenty Ereignis</td> 
         <td>Starting point in plentymarkets, which should trigger a Mail-In-One event</td>      
      </tr>
<tr>
         <td>Mail-In-One Ereignis</td> 
         <td>This event will be triggered as soon as data is transmitted</td>      
      </tr>
<tr>
         <td>Integration</td> 
         <td>Type of event: There are two choices. Contact event that triggers a trigger mailing or marketing program that triggers a marketing automation</td>      
      </tr>
<tr>
         <td>Kontaktereignis</td> 
         <td>Select a contact event defined in Mail-In-One</td>      
      </tr>
<tr>
         <td>Marketing Automation ID</td> 
         <td>Number of the Marketing Automation program to start in Mail-In-One. Mail-In-One allows graphical modeling of complex marketing programs with a simple tool</td>      
      </tr>
<tr>
         <td>Status</td> 
         <td>Returns the status of the event</td>      
      </tr>
<tr>
         <td>Löschen</td> 
         <td>LDelete the order event</td>      
      </tr>
<tr>
         <td>Edit</td> 
         <td>Edit the order event</td>      
      </tr>
</tbody>
</table>

## Domain delegation

The clear recommendation for good email marketing is the use of subdomains for your newsletters and transaction mailings. Your recipients should immediately recognise that emails were sent by you. For example, your plentymarkets online shop is https://my-online-shop.de. We recommend that you create two subdomains. A subdomain for newsletter marketing, for example news.online-shop.de. And a secondary subdomain order.online-shop.de for transaction emails, which are emails from welcome routes, orders, etc.

You can find your domain administration under the red plentymarkets logo – service center. Once there, go to „Domänen-Verwaltung.“

Create the sub-domains and enter the two Mail-In-One Name servers for each subdomain created:

    - ns0.isprit2.de
    - ns1.isprit2.de

We are happy to assist you with the installation and delegation of your subdomains to Mail-In-One. Just send a short email to campaign@treaction.net

## CRM customer data plentymarkets

**Newsletter-Checkbox**

The checkbox „newsletter“ has to be set true for contact synchronisation from plentymarkets to Mail-In-One. You can automatically use automation rules for the different constellations to ensure that the checkbox is set. If "newsletter" has not been activated on a customer, it will automatically be deleted from the plentymarkets folder.

**Customer segmentation in folders**

Plentymarkets offers a flexible customer segmentation feature for newsletters with its folder concept. You can select your customers using appropriate search criteria and bundle them all into a folder. Mail-In-One synchronises the assignment of the customers to the folders. You can then use these folders for selection in Mail-In-One. For example, folders could be used to depict what platform the customer has purchased, such as Amazon, eBay, and their own online shop. Another application is the segmentation of customers by product groups in which the customer has purchased, such as men's clothing, women's clothing or sports equipment.

## Enable logging for support

If recommended by the support, you can activate temporary logging of the MailInOne synchronization runs. Click on the Plentymarkets interface Data -> Log and click on the gearwheel. A pop-up window opens. In the pop-up window select MailInOne. Configuration settings appear on the right side of the popup. Select the checkbox "Active", Duration = 1 day, LogLevel "Info" or "Error". Attention: Activate logging at level "Info" only when requested by support: Logging can take up considerable storage resources with high data volumes.


## About treaction AG 

We are experts for lead generation, marketing automation and email-marketing. Our customers are small- to medium-sized businesses in Germany, Austria and Switzerland.

Through focus and experience from many successful campaigns, we reliably generate more revenue for our customers. Our principles are clarity, speed and achievement orientation for our customers. Take advantage of our extensive experience from thousands of campaigns for your success in online marketing.

The principles of our actions are clarity, speed and performance orientation for our customers.

We offer advanced technology to integrated solution. On request we can generate traffic for your online store. Our special expertise lies in Conversion Rate Optimization (CRO).

Our holistic approach means decisive advantages for you:

  - Nachweislich mehr Umsatz aus Ihrem Newsletter-Marketing
  - Increased sales from your newsletter marketing
  - Increased number of newsletter registrations in your online shop
  - With CRO, we generate higher sales with the same marketing budget
  - Generation of new leads via online marketing
  - Increase the repurchaser quota through welcome or order routes
  - Automated customer recovery campaigns


We advise you without obligation and free of charge. Call us on +49 (561) 997923-60 or send us an email to campaign@treaction.net





