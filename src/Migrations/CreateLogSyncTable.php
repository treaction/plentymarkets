<?php
namespace MailInOne\Migrations;

use MailInOne\Models\Database\LogSync;
use Plenty\Modules\Plugin\DataBase\Contracts\Migrate;

/**
 * Class CreateLogSyncTable
 */
class CreateLogSyncTable
{
   
    public function run(Migrate $migrate)
    {
        /**
         * Create the SyncLog table
         */
            try{
                $migrate->createTable(LogSync::class);
            } catch (\Exception $ex) {
                throw ($ex);
            }
            
    }
}