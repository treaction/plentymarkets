<?php
namespace MailInOne\Migrations;

use MailInOne\Models\Database\LoginUrl;
use Plenty\Modules\Plugin\DataBase\Contracts\Migrate;

/**
 * Class CreateLoginUrlTable
 */
class CreateLoginUrlTable
{
   
    public function run(Migrate $migrate)
    {
        /**
         * Create the LoginUrl table
         */
            try{
                $migrate->createTable(LoginUrl::class);
            } catch (\Exception $ex) {
                throw ($ex);
            }
            
    }
}