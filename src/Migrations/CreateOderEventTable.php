<?php
namespace MailInOne\Migrations;

use MailInOne\Models\Database\OrderEvent;
use Plenty\Modules\Plugin\DataBase\Contracts\Migrate;

/**
 * Class CreateOderEventTable
 */
class CreateOderEventTable
{
   
    public function run(Migrate $migrate)
    {
        /**
         * Create the OrderEvent table
         * 
         */          
             $migrate->createTable(OrderEvent::class);
                       
    }
}