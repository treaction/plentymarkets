<?php
namespace MailInOne\Migrations;

use MailInOne\Models\Database\Settings;
use Plenty\Modules\Plugin\DataBase\Contracts\Migrate;

/**
 * Class CreateSettingsTable
 */
class CreateSettingsTable
{
   
    public function run(Migrate $migrate)
    {
        /**
         * Create the settings table
         */
            try{
                $migrate->createTable(Settings::class);
            } catch (\Exception $ex) {
                throw ($ex);
            }
            
    }
}