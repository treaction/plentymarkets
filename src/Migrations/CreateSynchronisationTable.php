<?php
namespace MailInOne\Migrations;

use MailInOne\Models\Database\Synchronisation;
use Plenty\Modules\Plugin\DataBase\Contracts\Migrate;

/**
 * Class CreateSynchronisationTable
 */
class CreateSynchronisationTable
{
   
    public function run(Migrate $migrate)
    {
        /**
         * Create the Synchronisation table
         */
            try{
                $migrate->createTable(Synchronisation::class);
            } catch (\Exception $ex) {
                throw ($ex);
            }
            
    }
}