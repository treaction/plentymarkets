<?php
namespace MailInOne\Migrations;

use MailInOne\Models\Database\ContactEvent;
use Plenty\Modules\Plugin\DataBase\Contracts\Migrate;

/**
 * Class CreateContactEventTable
 */
class CreateContactEventTable
{
   
    public function run(Migrate $migrate)
    {
        /**
         * Create the ContactEvent table
         * 
         */
            try{
                $migrate->createTable(ContactEvent::class);
            } catch (\Exception $ex) {
                throw ($ex);
            }
            
    }
}