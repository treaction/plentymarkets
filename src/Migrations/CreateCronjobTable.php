<?php
namespace MailInOne\Migrations;

use MailInOne\Models\Database\Cronjob;
use Plenty\Modules\Plugin\DataBase\Contracts\Migrate;

/**
 * Class CreateCronjobTable
 */
class CreateCronjobTable
{
   
    public function run(Migrate $migrate)
    {
        /**
         * Create the LoginUrl table
         */
            try{
                $migrate->createTable(Cronjob::class);
            } catch (\Exception $ex) {
                throw ($ex);
            }
            
    }
}