<?php
/*
 * @version V3.0.2
 * @date 2019-07-09
 * @status tested, aproved, frozen
 */


namespace MailInOne\Helper;

// use Plenty\Modules\Cron\Contracts\CronHandler as Cron;
use MailInOne\Services\SynchronizationService;
use MailInOne\Services\AccountService;
use MailInOne\Services\Database\SettingsService;

use Plenty\Plugin\Log\Loggable;



/**
 * Description of SyncHelper:
 * Capsulates methods used in different data synchronisation scenarios plenty : MIO  (cron / user defined synchronisation)
 * @author jsr|treaction ag
 * @version 3.0.2
 */
class SyncHelper // extends Helper
{

    use Loggable;

    /**
     * @var const PLENTY_FOLDER_PREFIX  "plenty_": prefix for folders / custom fields from plenty to avoid deletion of user defined cust.fields in MIO
     */
    const PLENTY_FOLDER_PREFIX         = "plenty";
    const INITAL_DATATRANSFER_CUST_FLD = "plenty_INITIALDATATRSF";
    const WRITE_MESSAGE_CONTAINER_LOGS = false;

    // Reduce number of LOG entries for debugging in loop sections -> get examples of important log-data
    const MAX_LOG_ENTRIES = 100;

    /**
     * @var modes to indicate delta or inital synchronisation
     */
    const MODE_DELTA="DELTA";
    const MODE_INITIAL="INITIAL";
    const MODE_CEMA="CEMA";

    const MIO_DOUBLE_OPT_IN=4;
    const MIO_DOUBLE_OPT_IN_PLUS=5;


    /**
     * @var const UNKNOWN_PLENTY_GENDER='' (Keine Angabe)
     */
    const UNKNOWN_PLENTY_GENDER='Herr';


    /**
     * @var const DO_NOTHING. MIO handling instruction. Case (0) do nothing: do not add record to MIO sync
     */
    const DO_NOTHING = 0;


    /**
     * @var const CASE_ONE. MIO handling instruction. Case (1): sync contacts WITHOUT id
     */
    const CASE_ONE = 1;


    /**
     * @var const CASE_TWO. MIO handling instruction. Case (2): sync UPDATE contacts WITH id
     */
    const CASE_TWO = 2;


    /**
     * @var const CASE_THREE. MIO handling instruction. Case (3): sync NEW contacts WITH id
     */
    const CASE_THREE = 3;


    /**
     * @var const UPDATED_CONTACT = 'aktualisierte Kontakt' predefined plenty contact event for updated existing contacts
     */
    const UPDATED_CONTACT = 'aktualisierte Kontakt';


    /**
     * @var const NEW_CONTACT = 'Neuer Kontakt' predefined plenty contact event for new contacts
     */
    const NEW_CONTACT = 'Neuer Kontakt';


    /**
     * @var const to be found in Plenty Kontakt-Ereignis as 'DOI senden'
     */
    const PLENTY_CONT_EVENT_SEND_DOI = 'DOI senden';


    /**
     * @var const to be found in Plenty Kontakt-Ereignis as 'Kontaktereignis'
     */
    const PLENTY_CONT_EVENT_SEND_CE = 'Kontaktereignis';


    /**
     * @var const to be found in Plenty Kontakt-Ereignis as 'Marketing Program'
     */
    const PLENTY_CONT_EVENT_SEND_MA = 'Marketing Program';


    /**
     *
     * @var \MailInOne\Helper\MessageContainerHelper
     */
    // private $msg_container;


    /**
     *
     * @var int count number of log-entries and stop logging when self::MAX_LOG_ENTRIES is reached
     */
    private $count_log_entries;



    /**
     *
     * @var instance of MailInOne\Services\AccountService
     */
    private $plenty;


    /**
     *
     * @var array with all plenytmarket folders resp. Mail-In-One custom fields
     */
    private $plenty_folders;

    /**
     *
     * @var \MailInOne\Services\Database\SettingsService
     */
    private $plenty_settings;

    /**
     *
     * @var MailInOne\Services\SynchronizationService
     */
    private $mio;


    /**
     *
     * @var array plenty folder ids
     */
    private $plenty_folder_ids;

    /**
     *
     * @var array \MailInOne\Services\AccountService::getSettings()
     */
    private $mio_api_key;
    
     /**
     *
     * @var array \MailInOne\Services\Database\SettingsService::getSynchronisation()
     */
    private $synchronisation_option;


    /**
     *
     * @var string tenant mio api key as string (may be in json-format)
     */
    private $tenant_apikey;

    /**
     * @var array ['contact','event','ma','doi','updateContactWithId','newContactWithOutId','newContactWithId']<br>'updateContactWithId','newContactWithOutId','newContactWithId' are used as array indexes.<br>These arrays collect corresponding MIO contacts for later transfer to MIO
     */
    private $count;


    /**
     *
     * @var array to collect MIO API return values (returned by a JS console.log in browsers webdev networkanalysis. can be checked only by manually executed sync's)
     */
    private $mio_api_return_values;

    /**
     * @var array $syncContactsWithOutId for data transfer to MIO
     */
    private $syncContactsWithOutId;

    /**
     * TODO change to snake case
     * @var array $syncUpdateContactsWithId for data transfer to MIO
     */
    private $syncUpdateContactsWithId;

    /**
     * @var array $syncNewContactsWithId for data transfer to MIO
     */
    private $syncNewContactsWithId;


    /**
     *
     * @var array $arr['unique_email_adr']  = (int) <mio-doi-permission>
     */
    private $mio_emails_with_doi_permission;


    /**
     *
     * @var string date of last plenty to MIO synchronisation
     */
    private $last_sync_date;

    /**
     * MessageControllerHelper const and vars went here
     */

    /**
     * @var const max. number of entries for message container until it's flushed to plenty log system
     */
    const MC_MAX_NUMBER_OF_MESSAGES=100;

    /**
     *
     * @var array message container to consolidate messages
     */
    private $mc_messages;


    /**
     *
     * @var array to collect statistics for log
     */
    private $log_stats;


    /**
     * SyncHelper::__construct
     * @version 3.0.2
     * @param SynchronizationService $synchronizationService
     * @param AccountService $accountService
     * @param SettingsService $settingsService
     */
    public function __construct(
        SynchronizationService $synchronizationService,
        AccountService $accountService,
        SettingsService $settingsService
    ){
        $this->count_log_entries = 0;
        $this->helperInitArrays();
        $this->mcInit();

        try {
            $this->mio             = $synchronizationService;
            $this->plenty          = $accountService;
            $this->plenty_folders  = $this->plenty->listAllFolders();

            /* DEV LOG
            $this->getLogger('SyncHelper_PlentyFolderList')
                 ->info('MailInOne::plentymiosync.execLog', ['folders' => json_encode($this->plenty_folders, JSON_UNESCAPED_SLASHES) ]);

            $this->getLogger('SyncHelper_construct')
                 ->info('MailInOne::plentymiosync.execLog', ['start' => date('Y-m-d H:i:s')  ]);
            */

            $this->plenty_settings = $settingsService;
            $this->mio_api_key     = $this->plenty_settings->getSettings();

            $this->tenant_apikey   = (!is_string($this->mio_api_key))? json_encode(['mioapikey'=>$this->mio_api_key]) : $this->mio_api_key;
            $this->last_sync_date  = $this->plenty_settings->getLastSync();

            $this->mio_emails_with_doi_permission =  $this->helperGetMioContactsWithDoiPermission();

            /* DEV LOG
            $this->getLogger('SyncHelper_construct')
                 ->info('MailInOne::plentymiosync.execLog', ['mio_emails_with_doi_permission' => $this->mio_emails_with_doi_permission ]);
            */

        } catch (\Exception $exc) {
            $this->getLogger('SyncHelper_construct')
                 ->error('MailInOne::plentymiosync.execLog', ['INIT_FAILED' => $exc->getTraceAsString() ]);
            throw $exc;
        }
    }


    /*
    private function helperGetMioContactsWithCustomFields():array
    {
        $cf = json_decode(json_encode($this->mioGetCustomFieldsFromMio()));
        $custom_fields = [];
        // get only MIO custom_fields that begin with "plenty"
        foreach( $cf->custom_fields as $name => $v){

           if(1 === preg_match('#^(plenty){1}.*#', $name)) $custom_fields[]=$name;
        }

        $this->getLogger('SyncHelper_MioPlentyCustFields')
                 ->info('MailInOne::plentymiosync.execLog', ['mio_plentyonly_cust_fields' => $custom_fields ]);

        try {
            $temp = $this->mio->getAllMioContactsWithCustomFields(['apiKey'=>$this->mio_api_key, 'custom_fields'=> $custom_fields]);
        } catch (\Exception $exc) {
            $this->getLogger('SyncHelper_MioContCustFldErr')
                 ->info('MailInOne::plentymiosync.errLog', ['trace' => $exc->getTraceAsString() ]);
            return [];
        }


        $ret_arr = [];
        foreach( $temp as $record) {
            $ret_arr[$record->email[0]] = [ 'mio_id' => $record->id[0], 'mio_custom_fields' => $record->custom_fields[0] ];
        }
        return $ret_arr;
    }
    */


    /**
     * @internal runs DELTA data synchronisation with Triggers for DOI. Runs the same algorithm as mioRunDataSynchronisationInitial, but on UNinitilized synchronisation timestamp, runs MIO DOI triggers based on Plenty configuration (see plenty->CRM->Newsletter->MailInOne->Kontaktereignisse). This method can be used by pressing synchronisation buttom WITHOUT pressing "Reset Sync" before in Plenty-MIO-UI and is used by CRON-JOB based synchronisation
     * @see ContactSyncCron::handle
     * @author jsr|treaction ag
     * @version 3.0.2
     * @see class::mioExecBulkDataSynchronisation()
     * @see class::mioExecSingleRecordDeltaDataSynchronisation()
     * @access public used by ContactSyncCron::class
     * @param bool $do_bluk_sync default true (post <b>bulk</b> data to MIO), false (post <b>single records</b> to MIO)
     * @return type
     */
    public function mioRunDataSynchronisationDelta(bool $do_bluk_sync=true)
    {
        if ($do_bluk_sync) {
            return $this->mioExecBulkDataSynchronisation(self::MODE_DELTA);
        } else {
            return ''; // not implemented => due to obsolete single record data transfer to MIO
        }
    }


    /**
     * @internal runs INITIAL data synchronisation with Triggers for DOI. Runs the same algorithm as mioRunDataSynchronisationDelta, but on initilized synchronisation timestamp, runs MIO DOI triggers based on Plenty configuration (see plenty->CRM->Newsletter->MailInOne->Kontaktereignisse). This method is used by pressing synchronisation buttom AFTER pressing "Reset Sync" before in Plenty-MIO-UI
     * @see SyncController::setSync
     * @author jsr|treaction ag
     * @version 3.0.2
     * @access public used by SyncController::class
     * @param bool $do_bluk_sync
     * @return array (console.log data for UI)
     */
    public function mioRunDataSynchronisationInitial(bool $do_bluk_sync=true)
    {
        if ($do_bluk_sync) {
            return $this->mioExecBulkDataSynchronisation(self::MODE_INITIAL);
        } else {
            return ''; // not implemented => due to obsolete single record data transfer to MIO
        }
    }


    /**
     * @internal runs INITIAL data synchronisation. Triggers additionally MIO Contact Events and MIO Marketing Automation based on Plenty configuration (see plenty->CRM->Newsletter->MailInOne->Kontaktereignisse). Method is executed by pressing button "Reset Sync" and then "Kontakte und Ereignisse synchronisieren"  in Plenty MIO-PlugIn UI.
     * @see SyncController::setEvent
     * @author jsr|treaction ag
     * @version 3.0.2
     * @return array (console.log data for UI)
     */
    public function mioRunDataSyncWithTriggersForContactEventMaDoi(bool $do_bluk_sync=true)
    {
        if ($do_bluk_sync) {
            return $this->mioExecBulkDataSynchronisation(self::MODE_CEMA);
        } else {
            return ''; // not implemented => due to obsolete single record data transfer to MIO
        }
    }

    /**
     * @internal Runs bulk data synchronisation as delta snyc and set indicator custom field (see const INITAL_DATATRANSFER_CUST_FLD) in MIO, triggers MIO's DOI-email shipment
     * @author jsr|treaction ag
     * @version 3.0.2
     * @uses SyncHelper::count
     * @uses SyncHelper::$syncContactsWithOutId
     * @uses SyncHelper::$syncUpdateContactsWithId
     * @uses SyncHelper::$syncNewContactsWithId
     * @param string $mode ("DELTA"|"INITAL") controll whether to run DELTA or INITIAL data synchronisation. default = SyncHelper::MODE_DELTA
     * @access private
     * @return string json
     */
    private function mioExecBulkDataSynchronisation($mode):string
    {
        if ($mode !== self::MODE_DELTA && $mode !== self::MODE_INITIAL && $mode !== self::MODE_CEMA ) {
            $this->getLogger('SyncHelper_SyncInfo')
                ->error('MailInOne::plentymiosync.execLog', ['Wrong Paramter' => " wrong value for param mode {$mode}" ]);
            return json_encode(['wrongmode'=>$mode]);
        }
        else {
            $this->getLogger('SyncHelper_SyncMode')
                ->info('MailInOne::plentymiosync.execLog', ['Starting Sync' => "in mode {$mode}" ]);
        }

        // init return value of this method -> is returned json encoded at the end of this method
        $resultArray = [];

        // get basic contact and folder data from plenty
        // $folderIds = $this->plentyGetFolderIds();
        $contacts  = $this->plenty->listAllRecipients(); // uses \Plenty\Modules\Account\Newsletter\Contracts\NewsletterRepositoryContract
        $no_of_contacts = count($contacts);
        
        // getSynchronisation Option(Modus:Massen or Einzel, Menge)
        $this->synchronisation_option = $this->plenty_settings->getSynchronisation(); 
        $synchronisation_modus =   $this->synchronisation_option[0]; // Modus: MAssen or Einzel
        $synchronisation_quantity =   $this->synchronisation_option[1]; // Menge
        
        // get plenty events
        // needed to determine handling instructions of plenty data for MIO
        $arr_plenty_events = $this->plentyGetEvents();
        $no_of_events = count($arr_plenty_events);

        $this->getLogger('SyncHelper_BeforeSyncStats')
                ->info('MailInOne::plentymiosync.execLog', ['LoopInfo' => "contacts:{$no_of_contacts}/events:{$no_of_events}" ]);

        $this->getLogger('SyncHelper_BeforeSyncLoop')
                ->info('MailInOne::plentymiosync.execLog', ['events_array' => $arr_plenty_events ]);

        /**
         * loop over all plenty contacts, try to enrich MIO target data ($result) with available plenty contact data
         * depending on data content $result will be collected in three different MIO target arrays:
         * SyncHelper::$syncContactsWithOutId,
         * SyncHelper::$syncUpdateContactsWithId and/or
         * SyncHelper::$syncNewContactsWithId
         */
        foreach ($contacts as $contact) {

            $result = []; // init result
            $this->plentyEnrichResultContactData($result, $contact);

            $this->log_stats['CONTACT_DATA'][] = $result;


            /**
             * loop over all events.
             * Why? There is only relation between events and folders by $this->plentyGetFoldernameById($contact['folderId']) ) and $this->plentyGetEvents()[...]->ordner!
             * Based on received plenty data take handling instruction for MIO and manipulate MIO data accordingly to handling instuction
             */
            foreach ($arr_plenty_events as $plenty_event) {

                /* further, but unused plenty event data:
                $id                  = $plenty_event->id;
                $kontaktereignis     = $plenty_event->kontaktereignis;
                $kontaktereignisID   = $plenty_event->kontaktereignisID;
                $marketingAutomation = $plenty_event->marketingAutomation;
                // use plenty_event as param
                $event          = $plenty_event->event;
                $ordner         = $plenty_event->ordner; //x
                $integration    = $plenty_event->integration; //x
                $doi_key        = $plenty_event->doiKey; //x
                $bool_needs_doi = $plenty_event->isDoi; //x
                 *
                 */

                //$msg = (!empty($result['contactId'])) ? $result['contactId']: '';

                $handling_instruction = $this->mioGetHandlingInstructionByMode(
                    $mode,
                    $contact,
                    $plenty_event,
                    $result
                );

                if (self::DO_NOTHING != $handling_instruction) {
                    $this->mioRunHandlingInstructionForSync(
                        $mode,
                        $handling_instruction,
                        $plenty_event,
                        $result
                    );
                } else {
                    ;//do nothing
                }

                $this->log_stats['COUNT'][] = "C1:{$this->count['newContactWithOutId']} C2:{$this->count['updateContactWithId']} C3:{$this->count['newContactWithId']}";
            }
        }

        $this->helperLogHndlgDcsn(); // dev debugging only

        /**
         * bulk "synchronisation" of collected plenty data into MIO
         */
        $syncAllContacts = [
            'apiKey'                   => $this->plentyGetMioApiKey(),
            'syncContactsWithOutId'    => $this->syncContactsWithOutId,
            'syncUpdateContactsWithId' => $this->syncUpdateContactsWithId,
            'syncNewContactsWithId'    => $this->syncNewContactsWithId,
        ];

        try {
            switch ($synchronisation_modus) {
                case 'Massen':
                     $resultArray['syncAllContacts']['Massen'] = $this->mio->syncContacts($syncAllContacts);
                    break;
                 case 'Einzel':
                     $resultArray['syncAllContacts']['Einzel'] = $this->mio->syncSinglesContact($syncAllContacts);
                    break;

                default:
                    $resultArray['syncAllContacts']['Default'] = $this->mio->syncContacts($syncAllContacts);
                    break;
            }
            

            $this->plenty_settings->saveLogSync(
                $this->count['contact'],
                $this->count['doi'],
                $this->count['contactevent'],
                $this->count['marketinautomation']
            );



        } catch (\Exception $exc) {
            $this->getLogger('SyncController_syncContacts')
                ->error('MailInOne::plentymiosync.errLog', [ 'tracelog' => $exc->getTraceAsString() ]);
        }

        $this->getLogger('SyncController_setSync_'.$mode)
                ->info('MailInOne::plentymiosync.execLog', [ 'mio_api_return_values' => $this->mio_api_return_values ]);

        $this->getLogger('SyncController_setSync_END')
                ->info('MailInOne::plentymiosync.execLog', [ 'Endtime' => date('Y-m-d H:i:s') ]);

        return json_encode(array_merge( $resultArray, $this->mio_api_return_values ));
    }




    /**
     * @internal get Event Array from plenty : all events of "aktualisierte Kontakt" and ""Neuer Kontakt" with doi-information and MIO sanitized foldername
     * returns an array  [ {
        id:"222"
        event:"Neuer Kontakt"
        ordner:"plentyInteressent"
        integration:"DOI senden"
        kontaktereignis:""
        kontaktereignisID:"0"
        marketingAutomation:""
        doiKey:"9Y3XDLx0"
        doiName:"NL KW - 3"
        isDoi:"1"
        createdAt:"2019-06-17 09:51:43"
    },
    {
        "id": "187",
        "event": "Neuer Kontakt",
        "ordner": "plentyPizza",
     ... }]
     * @author jsr|treaction ag
     * @version 3.0.2
     * @return array
     */
    private function plentyGetEvents()
    {
        $event_array = [];

        try {
            $event_array = $this->plenty_settings->getEvent();

            // jsr: replaced: if (is_bool($settingsEventArray) && !$settingsEventArray) { // always false
            if (empty($event_array)) { // try to get event data by initializing first
                $this->plenty_settings->saveEventInit();
                $event_array = $this->plenty_settings->getEvent();
            }
        } catch (\Exception $exc) {
            $this->getLogger('SyncHelper_plentyGetEvents' )
                ->error('MailInOne::plentymiosync.errLog', [ 'msg' => 'Line ('. __LINE__.'). Trace: '.$exc->getTraceAsString()]);
        }

        $event_array = $this->mioSanitizeFolderNamesInEventArray( $event_array );
        /*
        $this->getLogger('SyncHelper_plentyGetEvents_'.__LINE__)
                 ->debug('MailInOne::plentymiosync.execLog', ['EventArrayAfter' => json_encode($event_array, JSON_PRETTY_PRINT)]);
        */
        return $event_array;
    }// plentyGetEvents


    /**
     * @author jsr|treaction ag
     * @version 3.0.2
     * @param  \Collection $event_array (laravel collection)
     * @return \Collection
     */
    private function mioSanitizeFolderNamesInEventArray($event_array)
    {
        if(!empty($event_array)) {
            foreach ($event_array as $k => $v) {
                $event_array[$k]->ordner = self::PLENTY_FOLDER_PREFIX.$this->helperSanitizePlentyFolderNamesForMio( $v->ordner );
            }
        }
        return $event_array;
    }



    /**
     * @internal fill $result-array call by reference with plenty contact data based on call findContactById($crm_contact_id) => see \Plenty\Modules\Account\Contact\Contracts\ContactRepositoryContract::class
     * @param array $result &$result call by reference!
     * @param type $crm_contact_id a plenty Contact-ID
     */
    private function plentyAddMoreContactDetailsToResult(array &$result, $crm_contact_id)
    {
        try {
            $plenty_contact = $this->plenty->findContactById($crm_contact_id);
        } catch (\Exception $exc) {
            $this->logLimited('Err_DetailsToResult', 'error', $exc->getTraceAsString() );
        }

        if (!empty($plenty_contact)) {

            //$this->logLimited('DetailsToResultOk', "plenty_contact_id_{$crm_contact_id}", $plenty_contact );

            $result['plentyID']    = $plenty_contact["plentyId"];
            $result['lastOrderAt'] = $plenty_contact["lastOrderAt"];
            $result['updatedAt']   = $plenty_contact["updatedAt"];
            $result['createdAt']   = $plenty_contact["createdAt"];
            // if contact was found in crm, result[contactId] is changed from newletter-contactId to crm-contact-id,
            // elsewise result[contactId] is transfered plenty-newsletter-id to MIO
            $result['contactId']   = $plenty_contact["contactId"]; // should be equal to $crm_contact_id;
            /*
            if(empty($result['lastOrderAt'])) {
                unset($result['lastOrderAt']); // removed, has  no influence on custom field creation
            }
             */

        } else {
            ;// $this->logLimited('DetailsToResultWrn', 'plenty_contact', 'not found');
        }
    }


    /**
     * @internal  checks, if an inital data sync plenty to MIO was done
     * @author jsr|treaction ag
     * @version 3.0.2
     * @deprecated <b>!!currently unused!!</b>
     * @access private
     * @return int  (-1) in case of error by this->mioGetCustomFieldsFromMio,<br>(0, false) if custom fields from this->mioGetCustomFieldsFromMio, but inital data transfer didn't take place<br>(1, true) if INITAL_DATATRANSFER_CUST_FLD was found as MIO custom field in corresponding tenant
     */
    private function mioHasInitalDataFromPlenty(): int
    {

        $custom_fields_in_mio = $this->mioGetCustomFieldsFromMio();
        $found = false;

        if(false === $custom_fields_in_mio) {
            // $this->writeLogToMessageContainer('ERR',__FUNCTION__.'|'.__LINE__.'|'."FATAL: mioGetCustomFieldsFromMio() failed|");
            return (-1);
        } elseif (!empty($custom_fields_in_mio) && is_array($custom_fields_in_mio)) {
            foreach ($custom_fields_in_mio as $custom_field) {
                if (self::INITAL_DATATRANSFER_CUST_FLD === trim($custom_field['name'])) {
                    // $this->writeLogToMessageContainer('INFO',__FUNCTION__.'|'.__LINE__.'|'."intial sync (plenty_INITIALDATATRSF) in MIO detected, proceding with delta sync|");
                    $found = true;
                }
            }
        }

        if($found) {
            return (1);
        }
        else {
            // $this->writeLogToMessageContainer('INFO',__FUNCTION__.'|'.__LINE__.'|'."starting inital sync plenty to mio|");
            return (0);
        }
    }


    /**
     * @internal get custom field array from MIO
     * @author jsr|treaction ag
     * @version 3.0.2
     * @return mixed  empty array, if something went wrong, else array of MIO custom fields, <b>false</b> in case of data access error
     */
    private function mioGetCustomFieldsFromMio()
    {
        try {

            $res = $this->mio->getCustomFields(['apiKey'=>$this->plentyGetMioApiKey()]); // call okay??

            $ret =[];

            if ($res instanceof \stdClass) {
                $ret = (array) $res;
            } elseif( is_array($res)) {
                $ret = $res;
            }elseif (NULL === $res) {
                ; // do nothing
            } else {
                $ret = false;
            }

            return $ret;
        } catch (\Exception  $exc) {
            $this->getLogger('SyncHelper_GetCustomFieldsFromMio' )
                ->error('MailInOne::plentymiosync.errLog', [ 'msg' => 'Line ('. __LINE__.'): call to this->mio->getCustomFields() failed. Trace: '.$exc->getTraceAsString()]);
            return false;
        }
    }


    /**
     * @internal insert custom field (see const INITAL_DATATRANSFER_CUST_FLD) into MIO to indicate that a inital synchronsisation of data has taken place
     * @author jsr|treaction ag
     * @version 3.0.2
     * @access private
     * @return bool
     */
    private function mioConfirmInitalDataSynchronisation()
    {
        return $this->mioCreateStandardCustomField(self::INITAL_DATATRANSFER_CUST_FLD);
    }


    /**
     * @internal get handling instruction by mode
     * @author jsr|treaction ag
     * @version 3.0.2
     * @param string $mode (SyncHelper::MODE_DELTA|SyncHelper::MODE_INITIAL)
     * @param array $contact
     * @param \stdClass $plenty_event
     * @param array $result
     * @return int handling instruction (SyncHelper::CASE_ONE|SyncHelper::CASE_TWO|SyncHelper::CASE_THREE)
     */
    public function mioGetHandlingInstructionByMode(
        string $mode,
        $contact,
        $plenty_event,
        $result
    ):int
    {
        $handling_instruction = self::DO_NOTHING;
        switch ($mode) {
                case self::MODE_DELTA:
                    $handling_instruction = $this->mioGetHandlingInstructionForDeltaSync(
                        self::MODE_DELTA,
                        $contact,
                        $plenty_event,
                        $result
                    );
                    break;

                case self::MODE_INITIAL:
                    $handling_instruction = $this->mioGetHandlingInstructionForInitalSync(
                        self::MODE_INITIAL,
                        $contact,
                        $plenty_event,
                        $result
                    );
                    break;

                case self::MODE_CEMA:
                    $handling_instruction = $this->mioGetHandlingInstructionForInitalSync(
                        self::MODE_CEMA,
                        $contact,
                        $plenty_event,
                        $result
                    );
                    break;

                default:
                    $handling_instruction = self::DO_NOTHING;
                    break;
        }
        return $handling_instruction;
    }


    /**
     * @internal get handling instruction for INITIAL synchronisation based on available plenty contact data.<br><b>lastSync</b>-Database entry is not used for this handling instruction.<br>!Core method in class SyncHelper!
     * @author jsr|treaction ag
     * @version 3.0.2
     * @param array $contact contact data from plenty
     * @param stdclass $plenty_event plenty event data. event properties like "aktualisierte Kontakt", "Neuer Kontakt", ordner to compare with $result['folderName'], further props)
     * @param array $result preassigned plenty data for this checks and later data maniplulation operations in MIO
     * @return int an int value of {0,1,2,3}. 0=do nothing (unsufficient contact information), 1=sync contacts WITHOUT id, 2=sync existing contacts WITH id, 3=sync NEW contacts WITH id
     */
    public function mioGetHandlingInstructionForInitalSync(
        $mode,
        $contact,
        $plenty_event,
        $result
    ): int
    {
        return $this->mioGetHandlingInstructionForDeltaSync($mode, $contact, $plenty_event, $result);

        /*
        $case = self::DO_NOTHING; // init $case

        if ($plenty_event->ordner !== $result['folderName']) {
            return self::DO_NOTHING;
            $this->log_stats['DO_NOTHING'][] = "for foldername {$result['folderName']} contact_email={$result['email']}" ;
        }

        // use or (||) for weak condition check (tested okay!), alternatively use and (&&) for strong condition check
        $is_updated_contact = ($plenty_event->event == self::UPDATED_CONTACT) &&
                              ($this->helperIsUpdatedContact($result['createdAt'],$result['updatedAt']));

        $is_new_contact     = ($plenty_event->event == self::NEW_CONTACT) && ($this->helperIsNewContact($result['createdAt'],$result['updatedAt']));

        if ($this->helperIsContactWithoutId($result["contactId"])) {

            // $case one: sync contacts WITHOUT id
            $case = (($plenty_event->event === self::NEW_CONTACT)) ? self::CASE_ONE : self::DO_NOTHING;

        } else { // contacts WITH id

            // $case two: sync UPDATE contacts WITH id
            $case = ($is_updated_contact)
                ? self::CASE_TWO
                // $case three: sync NEW contacts WITH id
                : ($is_new_contact)
                    ? self::CASE_THREE
                    : self::DO_NOTHING;
        }

        $this->log_stats['DO_HANDLININST'][] = "for foldername {$result['folderName']} email={$result['email']} instr=".$this->helperGetHandlingInstructionTxt($case);

        $this->logLimited('HndlInstIntlSync', 'decision_data', json_encode(
            [   'contact' => json_encode($contact, JSON_UNESCAPED_SLASHES),
                'plenty_event' => json_encode($plenty_event, JSON_UNESCAPED_SLASHES),
                'result' => json_encode($plenty_event, JSON_UNESCAPED_SLASHES),
                'HandlingInstruction' => $this->helperGetHandlingInstructionTxt($case)
            ],
            JSON_UNESCAPED_SLASHES|JSON_OBJECT_AS_ARRAY
        ));

        return $case;
         */
    }


    /**
     * @internal get handling instruction for DELTA synchronisation based on available plenty contact data. !Core method in class SyncHelper!
     * @author jsr|treaction ag
     * @version 3.0.2
     * @TODO optimize else part
     * @param array $contact contact data from plenty
     * @param stdclass $plenty_event plenty event data. event properties like "aktualisierte Kontakt", "Neuer Kontakt", ordner to compare with $result['folderName'], further props)
     * @param array $result preassigned plenty data for this checks and later data maniplulation operations in MIO
     * @return int an int value of {0,1,2,3}. 0=do nothing (unsufficient contact information), 1=sync contacts WITHOUT id, 2=sync existing contacts WITH id, 3=sync NEW contacts WITH id
     */
    private function mioGetHandlingInstructionForDeltaSync(
        $mode,
        $contact,
        $plenty_event,
        $result
    ): int
    {
        /* DEV LOG
        if( $contact['contactId'] == 154 ) {
            $this->log_stats['REBECCA_FOLDERS'][] = ['pe-ord'=>$plenty_event->ordner, 'res-ord'=>$result['folderName'], 'abgl'=> ($plenty_event->ordner !== $result['folderName'])?"NE":"EQ" ];
        }
         *
         */

        $case = self::DO_NOTHING;

        // return do nothing if folders are different
        if ($plenty_event->ordner !== $result['folderName']) {
            // $this->log_stats['HNDLNG_INST_FOLDEREXIT'][] = ['foldername'=>$result['folderName'], 'contact_email'=>$result['email'], 'instruction'=>'DO NOTHING'];
            return self::DO_NOTHING;
        }

        $is_crm_contact = (!empty($result['createdAt']) && !empty($result['updatedAt']));

        if( $is_crm_contact ) { // crm contact

            // nur indem für einen gefeuerten MA oder CE entsp. Daten hier gespeichert würden, liesse sich vermeiden, MA oder CE nicht erneut für  Kontakte auszulösen (Button  CE / MA Sync) mit zu
            if($mode === self::MODE_CEMA) {
                /*
                 * TODO (08.07.2019): Klärung (V3.1.0 ???)!
                 * Abgleich erforderlich.
                 * Dazu müssten 2 Tabellen für an Emails gefeuerte MA und gefeuerte CE aufgebaut werden.
                 * In den Tabellen werden alle email | (MA-id|CE-id) | datum  gespeichert.
                 * Wurde das gleiche MA oder CE für eine Email bereits gefeuert, dann wird der Trigger nicht ein zweites Mal ausgelöst.
                 *
                 */
                $is_crm_updated_after_last_sync = (strtotime($result['updatedAt']) >= strtotime($this->last_sync_date));

                $add_to_sync        = ((strtotime($result['timestamp']) >= strtotime($this->last_sync_date)) || $is_crm_updated_after_last_sync);
                $is_updated_contact = ($plenty_event->event == self::UPDATED_CONTACT); //  && ($this->helperIsUpdatedContact($result['createdAt'],$result['updatedAt']));
                $is_new_contact     = ($plenty_event->event == self::NEW_CONTACT);

                /* DEV LOG
                if( $result['contactId']=='165') {
                    $this->getLogger('SyncHelper_getHi165')
                     ->info('MailInOne::plentymiosync.execLog', ['data' => implode('; ', [__LINE__, $add_to_sync, $result['timestamp'], $this->last_sync_date, $is_updated_contact,$is_new_contact, $mode  ]) ]);
                }
                 *
                 */

            } else { // delta sync (cron) oder inital sync (button)
                //$add_to_sync        = (strtotime($result['updatedAt']) >= strtotime($this->last_sync_date));// Mike 5.7.19 16:40h
                $add_to_sync        = (strtotime($result['timestamp']) >= strtotime($this->last_sync_date));// Mike 5.7.19 16:40h
                $is_updated_contact = ($plenty_event->event == self::UPDATED_CONTACT) && ($this->helperIsUpdatedContact($result['createdAt'],$result['updatedAt']));
                $is_new_contact     = ($plenty_event->event == self::NEW_CONTACT) && ($this->helperIsNewContact($result['createdAt'],$result['updatedAt']));

                /* DEV LOG
                if( $result['contactId']=='165') {
                    $this->getLogger('SyncHelper_getHi165')
                     ->info('MailInOne::plentymiosync.execLog', ['data' => implode('; ', [__LINE__, $add_to_sync, $result['updatedAt'], $this->last_sync_date, $is_updated_contact,$is_new_contact, $mode  ]) ]);
                }
                 *
                 */
            }

        } else { // pure newsletter contact

            $add_to_sync = (strtotime($result['timestamp']) >= strtotime($this->last_sync_date));
            $is_updated_contact = ($plenty_event->event == self::UPDATED_CONTACT);
            $is_new_contact     = ($plenty_event->event == self::NEW_CONTACT);

            /* DEV LOG
            if( $result['contactId']=='165') {
                $this->getLogger('SyncHelper_getHi165')
                 ->info('MailInOne::plentymiosync.execLog', ['data' => implode('; ', [__LINE__, $add_to_sync, $result['timestamp'], $this->last_sync_date, $is_updated_contact,$is_new_contact, $mode  ]) ]);
            }
             *
             */
        }

        // detect what to do with the contact represented by "$result"
        if ($this->helperIsContactWithoutId($contact['contactId'])) { // pure newsletter contact
            // $case one: sync contacts WITHOUT id
            $case = ( $is_new_contact && $add_to_sync )? self::CASE_ONE : self::DO_NOTHING;

        } else { // crm contact

            // $case two: sync UPDATED contacts WITH id
            $case = ($is_updated_contact && $add_to_sync)
                ? self::CASE_TWO
                : ($is_new_contact && $add_to_sync)
                    ? self::CASE_THREE
                    : self::DO_NOTHING;
        }

        /* DEV LOG
        if( $contact['contactId'] == '165' ) {
            $this->log_stats['REBECCA_EVENT'][] = $plenty_event;
            $this->log_stats['REBECCA_RESULT'][] = array_merge( $result, ['instruction' => $this->helperGetHandlingInstructionTxt($case)]);
        }
         *
         */

        /* DEV LOG
        if(in_array($case, [self::CASE_ONE, self::CASE_TWO, self::CASE_THREE])) {
            $this->log_stats['FOUND_HNDLNG_INST'][] = ['foldername'=>$result['folderName'], 'contact_email'=>$result['email'], 'instruction'=>$this->helperGetHandlingInstructionTxt($case)];
        } else {
            $this->log_stats['FOUND_HNDLNG_INST_DN'][] = [ 'contactId'=>$result['contactId'], 'foldername'=>$result['folderName'], 'contact_email'=>$result['email'], 'plenty_event_id'=> $plenty_event->id, 'plenty_event'=> $plenty_event->event, 'instruction'=>$this->helperGetHandlingInstructionTxt($case)];
        }
         *
         */

        return $case;
    }


    /**
     * @internal run handling instruction for DELTA synchronisation based on available plenty contact data.<br>one of SyncHelper::syncContactsWithOutId, SyncHelper::syncUpdateContactsWithId or SyncHelper::syncNewContactsWithId array is assigned contact data based on handling instruction for further transfer to MIO<br>!Core method in class SyncHelper!
     * @internal in mode self::DELTA and self::INITIAL contacts are sync'ed and doi's are triggerd if needed, in mode self::MODE_CNTEVNT_MRTGAMTN contacts are sync'ed and DOIs, contact events and marketing automations are triggerd if needed
     * @author jsr|treaction ag
     * @version 3.0.2
     * @uses SyncHelper::syncContactsWithOutId for handling instruction SyncHelper::CASE_ONE
     * @uses SyncHelper::syncUpdateContactsWithId for handling instruction SyncHelper::CASE_TWO
     * @uses SyncHelper::syncNewContactsWithId for handling instruction SyncHelper::CASE_THREE
     * @uses SyncHelper::count to count contacts, doi, newContactWithOutId, updateContactWithId and newContactWithId
     * @param string $mode [self::MODE_INITAL,self::MODE_DELTA,self::MODE_CNTEVNT_MRTGAMTN]
     * @param int $handling_instruction (SyncHelper::CASE_ONE|SyncHelper::CASE_TWO|SyncHelper::CASE_THREE)
     * @param stdclass $plenty_event plenty event data. event properties like "aktualisierte Kontakt", "Neuer Kontakt", ordner to compare with $result['folderName'], further props)
     * @param array $result previously obtained data of a plenty contact
     */
    private function mioRunHandlingInstructionForSync(
        $mode,
        int $handling_instruction,
        $plenty_event,
        $result
    ){

        /* DEV LOG
        if( $result['contactId']=='165') {
        $txt = $this->helperGetHandlingInstructionTxt($handling_instruction);
            $this->getLogger('SyncHelper_RunHandlingInstructionCASE165' )
                ->info('MailInOne::plentymiosync.execLog', [ 'handling_instruction' => $txt ]);
            $this->getLogger('SyncHelper_RunHandlingInstructionEV165' )
                ->info('MailInOne::plentymiosync.execLog', [ 'plenty_event' => $plenty_event ]);
            $this->getLogger('SyncHelper_RunHandlingInstructionRES165' )
                ->info('MailInOne::plentymiosync.execLog', [ 'result' => $result ]);
        }
         *
         */

        switch ($handling_instruction) {

            case self::CASE_ONE: // contacts without id => a brand new newsletter contact

                $this->syncContactsWithOutId[$this->count['newContactWithOutId']] = [
                    'apiKey' => $this->plentyGetMioApiKey(),
                    'maileon' => $result,
                    'doi' => $plenty_event->isDoi
                ];
                $this->count['newContactWithOutId']++;
                $this->count['contact']++;
                $this->count['doi'] += $this->mioIfNeededTriggerDoiEmail($this->plentyGetMioApiKey(), $plenty_event, $result);

                if( $mode === self::MODE_CEMA) {
                    $this->count['marketinautomation'] += $this->mioIfNeededTriggerMarketingAutomation($this->plentyGetMioApiKey(), $plenty_event, $result);
                    $this->count['contactevent'] += $this->mioIfNeededTriggerContactEvent($this->plentyGetMioApiKey(), $plenty_event, $result);

                }
                break;

            case self::CASE_TWO: // existing contacts with id

                $this->syncUpdateContactsWithId[$this->count['updateContactWithId']] = [
                    'apiKey' => $this->plentyGetMioApiKey(),
                    'maileon' => $result,
                    'doi' => $plenty_event->isDoi
                ];
                $this->count['updateContactWithId']++;
                $this->count['contact']++;
                $this->count['doi'] += $this->mioIfNeededTriggerDoiEmail($this->plentyGetMioApiKey(), $plenty_event, $result);

                if( $mode === self::MODE_CEMA) {
                    $this->count['marketinautomation'] += $this->mioIfNeededTriggerMarketingAutomation($this->plentyGetMioApiKey(), $plenty_event, $result);
                    $this->count['contactevent'] += $this->mioIfNeededTriggerContactEvent($this->plentyGetMioApiKey(), $plenty_event, $result);
                }
                break;

            case self::CASE_THREE: // new contacts with id

                $this->syncNewContactsWithId[$this->count['newContactWithId']] = [
                    'apiKey' => $this->plentyGetMioApiKey(),
                    'maileon' => $result,
                    'doi' => $plenty_event->isDoi
                ];
                $this->count['newContactWithId']++;
                $this->count['contact']++;

                $this->count['doi'] += $this->mioIfNeededTriggerDoiEmail($this->plentyGetMioApiKey(), $plenty_event, $result);

                if( $mode === self::MODE_CEMA) {
                    $this->count['marketinautomation'] += $this->mioIfNeededTriggerMarketingAutomation($this->plentyGetMioApiKey(), $plenty_event, $result);
                    $this->count['contactevent'] += $this->mioIfNeededTriggerContactEvent($this->plentyGetMioApiKey(), $plenty_event, $result);
                }
                break;

            case self::DO_NOTHING:
            default:
                break;
        }
    }


    /**
     * @internal trigger Double Opt In (DOI) email sending in MIO. If contact already has a DOI set in Mail-In-One (permission 4 or 5), trigger is NOT executed!
     * @author jsr|treaction ag
     * @deprecated since version 3.0.2 do not trigger contact events in Initial Sync and Crons
     * @param  $mio_api_key api key of MIO defined in plenty user admin frontend
     * @param stdClass $plenty_event
     * @param array $result plenty contact information
     * @return int 0 if entry in MIO failed, 1 if entry was successful
     * @uses SyncHelper::mioHasContactDoiInMio()
     */
    private function mioIfNeededTriggerDoiEmail($mio_api_key, $plenty_event, $result): int
    {
        $ret = 1;

        $has_doi_in_mio = $this->mioHasContactDoiInMio($result['email']);

        if ((!$has_doi_in_mio) &&
            ($plenty_event->isDoi) &&
            (!empty($plenty_event->doiKey)) &&
            ($plenty_event->integration === self::PLENTY_CONT_EVENT_SEND_DOI )
        ) {
            try {
                // expects exception from $this->mio->setDoi !
                $this->mio_api_return_values[$plenty_event->id] =
                    $this->mio->setDoi([
                        'apiKey' => $mio_api_key,
                        'doiKey' => $plenty_event->doiKey,
                        'maileon' => $result,
                        ]
                    );
            } catch (\Exception $exc) {
                $this->getLogger('SyncHelper_TriggerDoiEmail' )
                    ->error('MailInOne::plentymiosync.errLog', [ 'tracelog' => $exc->getTraceAsString() ]);
                $ret = 0;
            }
        } else {
            $ret = 0;
        }

        return $ret;
    }


    /**
     * @internal triggers "Kontaktereignis" in Mail-In-One, if kontaktereignisID is set and and plenty contact event integration is set to 'Kontaktereignis'
     * @author jsr|treaction ag
     * @version 3.0.2
     * @param  $mio_api_key api key of MIO defined in plenty user admin frontend
     * @param stdClass $plenty_event
     * @param array $result plenty contact information
     * @return int 0 if entry in MIO failed, 1 if entry was successful
     * @return int
     */
    private function mioIfNeededTriggerContactEvent($mio_api_key, $plenty_event, $result): int
    {
        $ret = 1;

        // $has_contact_id         = (!empty((int)$result['contactId']) && $this->helperIsUnsignedInt($result['contactId']));
        $is_contact_event       = ($plenty_event->integration === self::PLENTY_CONT_EVENT_SEND_CE );
        $has_kontaktereignis_id = (!empty((int)$plenty_event->kontaktereignisID) && $this->helperIsUnsignedInt($plenty_event->kontaktereignisID));

        //if( $has_contact_id && $is_contact_event &&  $has_kontaktereignis_id ) {
        if( $is_contact_event &&  $has_kontaktereignis_id ) {

            try {
                /**
                 * @see https://dev.maileon.com/wp-content/uploads/Transactions_Cheatsheet.pdf Page 4
                 * Maileon compares the data with the specified transaction type and determines whether
                 * the contact may be sent at all (contact exists and has a valid permission).
                 * If everything is OK, the transaction is saved.
                 */
                $contact_event['apiKey'] = $mio_api_key;
                $contact_event['kontaktereignisID'] = (int)$plenty_event->kontaktereignisID;
                $contact_event['maileon'] = $this->helperGetMaileonContactData($result);

                /**
                 * uses MIO API-interfaces
                 * com_maileon_api_transactions_TransactionsService
                 * com_maileon_api_transactions_Transaction
                 * com_maileon_api_transactions_ContactReference
                 */
                $mio_res = $this->mio->setTransaction($contact_event);

            } catch (\Exception $exc) {
                $ret = 0;
                $this->getLogger('SyncHelper_TriggerContactEvent' )
                    ->error('MailInOne::plentymiosync.errLog', [ 'tracelog' => $exc->getTraceAsString() ]);
            }
        } else {
            $ret = 0;
        }
        $this->mio_api_return_values[] = [$plenty_event->id, $plenty_event->integration, $plenty_event->kontaktereignisID, $contact_event['maileon']['email'], $contact_event['maileon']['id'], $contact_event['maileon']['contactId'], $mio_res, $ret ];
        $this->log_stats['CE'][] = implode('; ', [$plenty_event->id, $plenty_event->integration, $plenty_event->kontaktereignisID, $contact_event['maileon']['email'], $contact_event['maileon']['id'], $contact_event['maileon']['contactId'], $mio_res, $ret ]);
        $this->log_stats['CE2'][] = implode(';', $contact_event['maileon'] );
        return $ret;
    }


    /**
     * @internal triggers "marketingAutomation" in Mail-In-One, if plenty contact event is marketingAutomation is set and plenty contact event integration is set to 'Marketing Program'
     * @author jsr|treaction ag
     * @version 3.0.2
     * @param  $mio_api_key api key of MIO defined in plenty user admin frontend
     * @param stdClass $plenty_event
     * @param array $result plenty contact information
     * @return int
     */
    public function mioIfNeededTriggerMarketingAutomation( $mio_api_key, $plenty_event, $result): int
    {
        $ret = 1;
        // $has_contact_id         = (!empty((int)$result['contactId']) && $this->helperIsUnsignedInt($result['contactId']));
        $is_marktg_automation   = ($plenty_event->integration === self::PLENTY_CONT_EVENT_SEND_MA );
        $has_marketing_autom_id = (!empty($plenty_event->marketingAutomation)); // datatype( marketingAutomation ) = string !!!

        //if( $has_contact_id && $is_marktg_automation &&  $has_marketing_autom_id ) {
        if( $is_marktg_automation &&  $has_marketing_autom_id ) {
            try {

                $mrktg_autom['apiKey'] = $mio_api_key;
                $mrktg_autom['programId'] = (int)$plenty_event->marketingAutomation;
                $mrktg_autom['maileon'] = $this->helperGetMaileonContactData($result);

                // uses MIO API-interface com_maileon_api_marketingAutomation_MarketingAutomationService
                $mio_res = $this->mio->setMarketingAutomation($mrktg_autom);

            } catch (\Exception $exc) {
                $ret = 0;
                $this->getLogger('SyncHelper_TriggerMarketingAutomation' )
                    ->error('MailInOne::plentymiosync.errLog', [ 'tracelog' => $exc->getTraceAsString() ]);
            }
        } else {
            $ret = 0;
        }
        $this->mio_api_return_values[] = [$plenty_event->id, $plenty_event->integration, $plenty_event->marketingAutomation, $mrktg_autom['maileon']['email'], $mio_res, $ret ];

        $this->log_stats['MA'][] = implode('; ', [$plenty_event->id, $plenty_event->integration, $plenty_event->marketingAutomation, $mrktg_autom['maileon']['email'], $ret ]);
        $this->log_stats['MA2'][] = implode(';', $result);
        return $ret;
    }


    /**
     * @internal extract special contact fields from $result
     * @param type $contact_data
     * @return array
     */
    private function helperGetMaileonContactData($contact_data):array
    {
        $maileon = [];
        $maileon['contactId'] = $contact_data['contactId']; // CRM contact Id
        $maileon['id'] = $contact_data['id']; // Newsletter contact Id
        $maileon['email'] = $contact_data['email'];
        $maileon['gender'] = $contact_data['gender'];
        $maileon['firstName'] = $contact_data['firstName'];
        $maileon['lastName'] = $contact_data['lastName'];
        $maileon['birthday'] = $contact_data['birthday'];
        $maileon['folderName'] = $contact_data['folderName'];
        return $maileon;
    }


    /**
     * @author jsr|treaction ag
     * @version 3.0.2
     * @param string $email
     * @return boolean true, if contact (email-adr) has permission 4 (DOUBLE_OPT_IN) or 5 (DOUBLE_OPT_IN_PLUS) in Mail-In-One, else return false
     */
    private function mioHasContactDoiInMio(string $email='')
    {
        $doi_permission = 0;

        $this->logLimited( 'HasContactDoiInMioParam', "email", "email #{$email}# in list".json_encode( $this->mio_emails_with_doi_permission, JSON_UNESCAPED_SLASHES));

        if(!empty($email) && array_key_exists($email, $this->mio_emails_with_doi_permission)) {
            $doi_permission = (int) $this->mio_emails_with_doi_permission[$email];
        }

        $this->logLimited( 'HasContactDoiInMioReturn', "email_permission", "{$email} = {$doi_permission}");

        if(in_array($doi_permission, [self::MIO_DOUBLE_OPT_IN, self::MIO_DOUBLE_OPT_IN_PLUS])){
            return true;
        } else {
            return false;
        }
    }

    /**
     * old / original version of folder deletion mioDeleteCustomFieldsInMioByPlentyFolders
     * @internal  old code from 3.0.1 (old: get all folders from plenty) new: get all custom fields (plenty-folders) from MIO, prefix them with string PLENTY_FOLDER_PREFIX, substitude specical of foldernames chars with "_" and delete their corresponding "custom fields" in MIO
     * @author jsr|treaction ag
     * @deprecated since version 3.0.2
     * @returns   void
     */
    public function mioDeleteCustomFieldsInMioByPlentyFolders()
    {

        $custom_fields_to_delete_in_mio = $this->plenty_folders;

        foreach ($custom_fields_to_delete_in_mio as $folder) {

            //replace special characters not supported in MIO and add PLENTY_FOLDER_PREFIX as prefix for mio custom fields
            $resultFolders = self::PLENTY_FOLDER_PREFIX.$this->helperSanitizePlentyFolderNamesForMio($folder['name']);
            // wrong all currenctly folders in MIO should be cleaned! now we clean folders based current folders in plenty, better would be to remove all existing folders / custom fields in MIO
            $results['apiKey'] = $this->plenty_settings->getSettings();
            $results['folder'] = $resultFolders;


            // delete each single custom field with assigned values, since there is no method to delete all custom fields in MIO with one call
            try {
                $this->mio->deleteCustomField($results);
            } catch (\Exception $exc) {
                $this->getLogger('SyncHelper_DeleteCustomFieldsInMio' )
                    ->error('MailInOne::plentymiosync.errLog', [ 'tracelog' => $exc->getTraceAsString() ]);
            }
        }
    }


    /**
     * mioDeleteCustomFieldsInMioByMioCustomFields
     * @internal  (old: get all folders from plenty) new: get all custom fields (plenty-folders) from MIO, prefix them with string PLENTY_FOLDER_PREFIX, substitude specical of foldernames chars with "_" and delete their corresponding "custom fields" in MIO
     * @author jsr|treaction ag
     * @version 3.0.2
     * @access public used outside SyncHelper::class
     * @returns   void
     */
    public function mioDeleteCustomFieldsInMioByMioCustomFields()
    {
        $custom_fields_to_delete_in_mio = $this->mioGetCustomFieldsFromMio();
        /*
        $this->getLogger('SyncHelper_mioDeleteCustomFieldsInMioByMioCustomFields')
                 ->debug('MailInOne::plentymiosync.execLog', [ 'MioCustomFields' => json_encode($custom_fields_to_delete_in_mio, JSON_PRETTY_PRINT)]);
        */
        $mio_api_key = $this->plentyGetMioApiKey(); //$this->plenty_settings->getSettings();

        if(is_array($custom_fields_to_delete_in_mio) && !empty($custom_fields_to_delete_in_mio)) {

            foreach ($custom_fields_to_delete_in_mio as $custom_field) {
               $this->helperDeleteMioCustomField($mio_api_key,$custom_field); // do not check return value here
            }

        } elseif(false === $custom_fields_to_delete_in_mio) {
            $this->getLogger('SyncHelper_DeleteCustomFieldsInMio' )
                ->error('MailInOne::plentymiosync.errLog', [ 'msg' => 'Line ('. __LINE__.'): Error getting custom field array from MIO.']);
        } else {
            $this->getLogger('SyncHelper_DeleteCustomFieldsInMio' )
                ->error('MailInOne::plentymiosync.errLog', [ 'msg' => 'Line ('. __LINE__.'): Empty custom field array received from MIO. Could not delete MIO custom fields.']);
        }
    }


    /**
     * mioCreateCustomFields
     * @internal  get all folders from plenty, prefix them with string PLENTY_FOLDER_PREFIX, substitude specical of foldernames chars with "_" and create their corresponding "custom fields" in MIO, collect and return plenty folder-IDs
     * @author jsr|treaction ag
     * @version 3.0.2
     * @access public used outside SyncHelper::class
     * @return array all added plenty folder Ids
     */
    public function mioCreateCustomFieldsFromPlentyFoldernames()
    {
        $ret = true;

        try {
            foreach ($this->plenty_folders as $folder) {

                $result['folderName'] = self::PLENTY_FOLDER_PREFIX.$this->helperSanitizePlentyFolderNamesForMio($folder["name"]);
                $mio_custom_field['apiKey']  = $this->plentyGetMioApiKey();
                $mio_custom_field['maileon'] = $result;
                $this->mio->createCustomField($mio_custom_field);
            }
        } catch (\Exception $exc) {
            $this->getLogger('SyncHelper_CreateCustomFieldsFromPlenty' )
                ->error('MailInOne::plentymiosync.errLog', [ 'tracelog' => $exc->getTraceAsString() ]);
            $ret = false;
        }
        return $ret;
    }


    /**
     * @internal add a separate custom field (in plenty language: "folders") to MIO), $standard_custom_field is prefixed with const PLENTY_FOLDER_PREFIX="plenty_"
     * @author jsr|treaction ag
     * @version 3.0.2
     * @param string $mio_standard_custom_field (e.g. 'plentyID', 'lastOrderAt')
     * @access public used outside SyncHelper::class
     * @return bool true = custom field added to mio, false = adding of custom field failed
     */
    public function mioCreateStandardCustomField(string $mio_standard_custom_field)
    {
        $ret = true;

        $mio_custom_field['apiKey']  = $this->mio_api_key;
        $mio_custom_field['maileon'] = array(
            'folderName' => self::PLENTY_FOLDER_PREFIX . $this->helperSanitizePlentyFolderNamesForMio($mio_standard_custom_field), // $this->plenty->validationCharacter($mio_standard_custom_field)
        );

        try {
            $this->mio->createCustomField($mio_custom_field);
        } catch (\Exception $exc) {
            $ret = false;
            $this->getLogger('SyncHelper_CreateStandardCustomField' )
                ->error('MailInOne::plentymiosync.errLog', [ 'tracelog' => $exc->getTraceAsString() ]);
        }
        return $ret;
    }


    /**
     * @internal initialize MIO target data (&$result) with plenty contact data, assign folderName by folderId for mioRunHandlingInstruction
     * @author jsr|treaction ag
     * @version 3.0.2
     * @param array $result => &$result: Call by reference! MIO target data.
     * @param array $contact plenty contact data
     * @return void
     */
    private function plentyEnrichResultContactData(array &$result, array $contact)
    {
        // basic data assignment (mio)$result[*] = (plenty)$contact[*]
        $result['email']      = $contact["email"];
        $result['firstName']  = $contact["firstName"];
        $result['lastName']   = $contact["lastName"];
        $result['contactId']  = $contact["contactId"];
        $result['id']         = $contact["id"];
        $result['gender']     = $contact["gender"];
        $result['birthday']   = $contact["birthday"];
        $result['timestamp']  = $contact["timestamp"];
        $result['lastSync']   = $this->last_sync_date;
        $result['SALUTATION'] = $this->helperPlentyGenderToMioSalutation($contact["gender"]);
        $result['folderName'] = '';

        /*
        if('1970-01-01' == $result['birthday']) {
            $result['birthday'] = '0000-00-00'; // removed, has no influence on custom field creation
        }
         */

        $has_valid_folder_id = (!empty((int)$contact['folderId']) && $this->helperIsUnsignedInt($contact['folderId']));

        // get plenty foldername and assign it to (mio)$result[folderName]
        if($has_valid_folder_id) {

            $sanitized_folder_name = $this->plentyGetFoldernameById($contact['folderId']); // returns folderName with PREFIX
            // $this->log_stats['FOLDERS'][] =
            $result['folderName'] = (!empty($sanitized_folder_name)) ? $sanitized_folder_name : '';
        }

        /* LOG
        $this->logLimited('ResultContactDataOky', 'has_valid_folder_id', ($has_valid_folder_id)?"Valid id#{$contact['folderId']}#":"Invalid id#{$contact['folderId']}#" );
        $this->logLimited('ResultContactDataRes1', 'result', $result);
        $this->logLimited('ResultContactDataFid', 'is_int contact[folderId]',($this->helperIsUnsignedInt($contact['folderId']))?'ja':'nein');
        $this->logLimited('ResultContactDataFnm', 'result[folderName]', $result['folderName']);
        */

        // try to get more details of plenty contact from plenty CRM by email-address
        try {
           $crm_contact_id = $this->plenty->getContactIdByEmail($contact['email']);
        } catch (\Exception $exc) {

            $this->getLogger('SyncHelper_Err_ResultContactData')
                ->error('MailInOne::plentymiosync.errLog', ['tracelog' => $exc->getTraceAsString()]);
        }

        // add plenty CRM contact information to (mio)$result, if a CRM contact-ID was found
        if (!empty((int)$crm_contact_id) && $this->helperIsUnsignedInt($crm_contact_id)) {

            $this->plentyAddMoreContactDetailsToResult($result, $crm_contact_id);
        } else {
            ; // $this->writeLogToMessageContainer('WARN' ,__FUNCTION__.'|'.__LINE__. "|contactId fetchted by contact email address is empty or not an unsigned int. cannot fetch further contact details for result[contactId] {$result['contactId']}.|");
        }

        /* LOG
        $this->logLimited('ResultContactDataCid', 'contact_id_by_email', "email={$contact['email']}; id={$crm_contact_id}");
        $this->logLimited('ResultContactDataRes2', 'result', $result );
         *
         */
    }


    /**
     * @internal  get all folders from plenty, prefix them with string PLENTY_FOLDER_PREFIX, substitude specical of foldernames chars with "_" and create their corresponding "custom fields" in MIO, collect and return plenty folder-IDs
     * @author jsr|treaction ag
     * @version 3.0.2
     * @return array array of plenty folder-IDs
     */
    private function plentyGetFolderIds(): array
    {
        $folderIds = [];
        $folderlist = $this->plenty_folders;

        foreach ($folderlist as $folder) {
            // new // array_push($folderIds, $folder["id"] );
            $folderIds[ $folder["id"] ] = $folder["id"]; // old //
        }
        /*
        $this->getLogger('SyncHelper_plentyGetFolderIds')
                ->debug('MailInOne::plentymiosync.execLog', ['RetPlentyFolderIds' => $folderIds]);
        */
        return $folderIds;
    }


    /**
     * @internal get MailInOne ApiKey, mandatory for calls to MailInOne-API
     * @author jsr|treaction ag
     * @version 3.0.2
     * @return string returns "" (empty string), if no ApiKey was set, else Mail In One ApiKey
     */
    private function plentyGetMioApiKey(): string
    {
        return ('--' === $this->mio_api_key) ? '' : $this->mio_api_key;
    }


    /**
     * @internal helper to get PREFIXED and SANITIZED plenty folder name for use as MIO custom field
     * @author jsr|treaction ag
     * @version 3.0.2
     * @param string $messa
     * @param type $plenty_folder_id a plenty folder-ID
     * @return mixed, FALSE if folder could not be found, else string sanitized plenty folder name for MIO custom field
     */
    private function plentyGetFoldernameById($plenty_folder_id) // test approved 2109-07-02 : works!
    {
        $ret = false;

        foreach( $this->plenty_folders as $folder ) {
            if( $folder['id'] == $plenty_folder_id ) {
                $ret = self::PLENTY_FOLDER_PREFIX.$this->helperSanitizePlentyFolderNamesForMio($folder['name']);
            }
        }

        $line = __LINE__; $this->log_stats['PLENTY_FOLDERS'][] = "id:{$plenty_folder_id};f:{$ret};l:{$line}";

        // $this->logLimited('GetFoldernameById', 'plentyGetFoldernameById', "not found: plenty_folder_id={$plenty_folder_id};ret={$ret};plenty_folders=".json_encode($this->plenty_folders, JSON_UNESCAPED_SLASHES));
        return $ret;
    }


    /**
     * @internal  init for SyncHelper::$count
     * @author jsr|treaction ag
     * @version 3.0.2
     * @access private
     */
    private function helperInitArrays()
    {
        $this->plenty_folder_ids = $this->plentyGetFolderIds();
        $this->log_stats=[];

        $this->count = [
            'contact'               => 0,
            'doi'                   => 0,
            'contactevent'          => 0,
            'marketinautomation'    => 0,
            'updateContactWithId'   => 0,
            'newContactWithOutId'   => 0,
            'newContactWithId'      => 0,
        ];

        $this->mio_api_return_values    = [];
        $this->syncContactsWithOutId    = [];
        $this->syncNewContactsWithId    = [];
        $this->syncUpdateContactsWithId = [];
    }


    /**
     * @internal helper to convert plenty gemnder to MIO gender
     * @author jsr|treaction ag
     * @version 3.0.2
     * @param string $gender from plenty (m|f|d)
     * @return string gender for MIO (Herr|Frau|Herr)
     */
    private function helperPlentyGenderToMioSalutation(string $gender): string
    {
        $ret_gender = '';

        switch ($gender) {
            case 'male':
            case 'm':
                $ret_gender = 'Herr';
                break;
            case 'female':
            case 'f':
                $ret_gender = 'Frau';
                break;
            default:
                $ret_gender = self::UNKNOWN_PLENTY_GENDER; // default for "Herr", as used for "Genders" or undefined in plenty, since "d" cannot be processed by MIO
                break;
        }

        return $ret_gender;
    }


    /**
     * @internal helper to sanitize plenty folder names into MIO custom field names. Allowed chars: ([A-Za-z_ \-]+) not allowed chars are replaced by underscore "_"
     * @author jsr|treaction ag
     * @version 3.0.2
     * @param type $plenty_folder_name plenty foldername
     * @return string trimmed and sanitized folder name as MIO custom field (chars, underscore(_), blank( ), numbers(0-9) for replaced chars)
     */
    private function helperSanitizePlentyFolderNamesForMio($plenty_folder_name): string
    {
        // return trim(preg_replace('/[^a-z^0-9^_ ]+/i', '_', $plenty_folder_name));
        return trim(preg_replace('/[^a-z^0-9]+/i', '', $plenty_folder_name));
    }


    /**
     * @internal get handling instruction text by integer representation
     * @author jsr|treaction ag
     * @version 3.0.2
     * @param int $handling_instruction
     * @return string text representation of handling instruction
     */
    private function helperGetHandlingInstructionTxt(int $handling_instruction)
    {
        switch ($handling_instruction) {
            case self::CASE_ONE:
                return "sync_contacts_without_id";
            case self::CASE_TWO:
                return "sync_existing_contacts_with_id";
            case self::CASE_THREE:
                return "sync_new_contacts_with_id";
            case self::DO_NOTHING:
            default:
                return "do nothing";
        }
    }


    /**
     * @internal delete a custom field in MIO
     * @author jsr|treaction ag
     * @version 3.0.2
     * @param type $mio_api_key MIO ApiKey
     * @param type $custom_field MIO custom field
     * @return bool true, if custom field was deleted, false if deletion failed
     */
    private function helperDeleteMioCustomField( $mio_api_key, $custom_field)
    {
        $ret = true;

        if( !empty($custom_field)) {
            $results['apiKey'] = $mio_api_key;
            $results['folder'] = $custom_field['name'];

            try {
                $this->mio->deleteCustomField($results);
            } catch (\Exception $exc) {
                $ret = false;
                $this->getLogger('SyncHelper_DeleteMioCustomField')
                    ->error('MailInOne::plentymiosync.errLog', ['tracelog' => $exc->getTraceAsString()]);
            }
        }
        else {
            $ret = false;
            // $this->writeLogToMessageContainer('ERR',__FUNCTION__.'|'.__LINE__."|MIO empty custom field cannot be deleted|");
        }

        return $ret;
    }


    /**
     * @internal a new created contact in plenty is assigned two timestamps: one for createdAt and one for updatedAt with a time delta of ~1 second.<br>helperIsNewContact determines if a contact is created or updated based on this timestamps and a given time tolerance in seconds
     * @param string $createdAt plenty contact createdAt timestamp
     * @param string $updatedAt plenty contact updateAt timestamp
     * @param int $time_tolerance_in_sec optional, default=60 [s]
     * @return bool <b>true</b>, if created (new contact), otherwise <b>false</b> (updated contact)
     */
    public function helperIsNewContact($createdAt, $updatedAt, int $time_tolerance_in_sec=60)
    {
        $ret = false;
        $is_set_only_created_at = (!empty($createdAt) && empty($updatedAt));
        $both_ts_have_values = (!empty($createdAt) && !empty($updatedAt));
        $both_ts_are_empty = (empty($createdAt) && empty($updatedAt));

        if ($both_ts_have_values) {
            $ret = (strtotime($updatedAt) - strtotime($createdAt) <= $time_tolerance_in_sec ) ? true : false;
        } elseif ($is_set_only_created_at) {
            $ret = true;
        } elseif ($both_ts_are_empty) {
            // assume contact was created => there was no data enrichment by CRM, since contact data was not found in CRM by email address
            $ret = true;
        } else {
            // only updated_at was set => assume contact was updated
            $ret = false;
        }

        /*
        $ret_msg = ($ret) ? "TRUE" : "FALSE";
        $this->writeLogToMessageContainer('DEBUG',__FUNCTION__.'|'.__LINE__."|createdAt={$createdAt}, updatedAt={$updatedAt}, ret(helperIsNewContact)={$ret_msg}|");
         */
        return $ret;
    }


    /**
     * @internal inverse of SyncUtils::helperIsNewContact
     * @param string $createdAt
     * @param string $updatedAt
     * @param int $time_tolerance_in_sec, optional, default=60 [s]
     * @return bool
     */
    public function helperIsUpdatedContact( $createdAt,  $updatedAt, int $time_tolerance_in_sec=60)
    {
        return (!$this->helperIsNewContact($createdAt, $updatedAt, $time_tolerance_in_sec));
    }


    /**
     *
     * @param type $int_value
     * @return bool true if is unsigend int, else false
     */
    private function helperIsUnsignedInt( $int_value )
    {
        return (1 === preg_match('/^[0-9]+$/', $int_value)) ? true : false;
    }


    /**
     * @internal returns inverse value of SyncHelper::helperIsUnsignedInt
     * @param type $int_value
     * @return bool true if $int_value is invalid contactId (not an unsigned int), false if $int_value was a valid unsigned int (or contactId)
     */
    private function helperIsContactWithoutId( $int_value )
    {
        return (empty((int)$int_value) || !$this->helperIsUnsignedInt($int_value));
    }


    /**
     * @internal gets key(email) value(mio-doi-permission) pairs from MIO and returns array to check if DOI's can be triggered (not MIO-permission 4 or 5) or not
     * @author jsr|treaction ag
     * @version 3.0.2
     * @return array ['email_1'] = <mio-doi-permission>, ... ['email_n'] = <mio-doi-permission>
     */
    private function helperGetMioContactsWithDoiPermission():array
    {
        $temp = json_encode($this->mio->getAllMioEmailContactsWithDoiPermission($this->mio_api_key));

        $ret_arr = [];
        foreach( json_decode($temp) as $record) {
            $ret_arr[$record->email[0]] = $record->permission[0];
        }
        return $ret_arr;
    }


    /**
     * @internal helper to provide Logging of actions that took place
     * @author jsr|treaction ag
     * @version 3.0.2
     * @param string $loglevel ("ERR" | "WARN" | "INFO")
     * @param string $message provide an message for error logging e.g.<br><b>["ERR |WARN |INFO |" .__FUNCTION__ .__LINE__ . "some text ..." ]</b>
     * @return void
     * TODO: complete coding !!!!
     */
    public function writeLogToMessageContainer( string $loglevel='INFO', string $message = '')
    {
        if( self::WRITE_MESSAGE_CONTAINER_LOGS && ! $this->mcAddMessage($loglevel, $message ) ) {
            // if current message could'nt be written to message container
            // $this->helperSendLogPerEmail(); // send all collected messages
            $this->mcAddEntriesToPlentyLog();
            $this->mcInit(); // cleanup message container
            $this->mcAddMessage($loglevel,$message); // add message
        }
    }


    /**
     * @internal send all log messages collected in message container
     * @author jsr|treaction ag
     * @version 3.0.2
     * @access public
     * @return void
     */
    public function helperSendLogPerEmail()
    {
        $this->plenty_settings->sendLog($this->mcGetAllMessages('CSV'));
    }


    /**
     * ===========================================
     * MessageContainer for internal debug logging
     * ===========================================
     */

    /**
     * @internal adds a message to messagecontainer with timestamp
     * @author jsr|treaction ag
     * @version since 3.0.2
     * @param string $message
     * @return bool true, if no message did not exceed 1000 Messages, otherwise false
     */
    public function mcAddMessage( string $loglevel, string $message )
    {

        if( $this->mcGetNoOfMessages() < self::MC_MAX_NUMBER_OF_MESSAGES )  {
            array_push( $this->mc_messages, ['time' => date('Y-m-d H:i:s'), 'loglevel'=>$loglevel, 'msg'=>$message] );
            return true;
        }
        else {
            return false;
        }
    }

    /**
     *
     * @param type $loglevel ['ERR', 'WARN', 'INFO'], default = 'INFO'
     */
    public function mcAddEntriesToPlentyLog($loglevel='INFO')
    {
        $all = [];
        $warn_err = [];
        $err =[];
        $index=0;


        foreach( $this->mc_messages as $mcm) {

             if (in_array($mcm['loglevel'], ['INFO'])) {
                 $info[] = [ "info#{$index}" => $mcm['msg'] ];

             } elseif ( in_array($mcm['loglevel'], ['WARN'])) {
                 $warn[] = [ "warn#{$index}" => $mcm['msg'] ];

             } elseif ( in_array($mcm['loglevel'], ['ERR'])) {
                 $err[] = [ "warn#{$index}" => $mcm['msg'] ];

             } elseif (in_array($mcm['loglevel'], ['DEBUG',])) {
                 $debug[] = [ "err#{$index}" => $mcm['msg'] ];
             } else {
                 // do nothing
             }
            $index++;
        }

        // schreib nur in debug !!!!

        if ($loglevel === 'INFO') {

            $this->getLogger('SyncHelper_MsgContInfo')
                 ->debug('MailInOne::plentymiosync.execLog', [$loglevel => json_encode($info)]);

        } elseif( $loglevel === 'DEBUG') {

            $this->getLogger( 'SyncHelper_MsgContDebug')
                 ->debug('MailInOne::plentymiosync.warnLog', [$loglevel => json_encode($debug)]);

        } elseif ( $loglevel === 'WARN') {

            $this->getLogger( 'SyncHelper_MsgContWarning')
                 ->debug('MailInOne::plentymiosync.warnLog', [$loglevel => json_encode($warn)]);

        } elseif ($loglevel ==='ERR') {

            $this->getLogger('SyncHelper_MsgContError')
                 ->debug('MailInOne::plentymiosync.errLog', [$loglevel => json_encode($err)]);

        } else {
            // do nothing
        }
    }


   /**
   * @internal reset the message container. all messages are deleted
   * @author jsr|treaction ag
   * @version since 3.0.2
   * @return void
   */
    public function mcInit()
    {
      $this->mc_messages = [];
    }


    /**
     * @author jsr|treaction ag
     * @version since 3.0.2
     * @param string $format JSON|CSV
     * @return string all container messages in either JSON or CSV format. E.g. CSV: seqno;timestamp;message
     */
    public function mcGetAllMessages(string $format='JSON'):string
    {
        if( $format === 'JSON') {

            return json_encode( $this->mc_messages, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE|JSON_FORCE_OBJECT);

        } elseif ($format==='CSV') {

            $ret = '';

            foreach( $this->mc_messages as $k => $v ) {
                $ret .= $k.';'.$v['time'].';'.$v['msg']."\n";
            }

            return $ret;

        } else {

            return '';
        }
    }


    /**
     * @author jsr|treaction ag
     * @version since 3.0.2
     * @return int
     */
    public function mcGetNoOfMessages():int
    {
        return count($this->mc_messages);
    }


    /** ======================== **/
    /** KEPT, BUT UNUSED METHODS **/
    /** ======================== **/

    /**
     * @internal writes a limited number (see self::MAX_LOG_ENTRIES) of log entrie to plenty "info" log. Logging in plenty must be set to "active" for plugin MailInOne (UI: Daten -> Log -> cogwheel = configure Logs)
     * @param string $identificator plenty log indentificator
     * @param string $key plenty log key
     * @param type $value plenty log value
     * @return void
     */

    private function logLimited(string $identificator, string $key, $value)
    {
        if(empty($identificator) || empty($key) || empty($value)) {
            return; // do noting and exit
        }

        if($this->count_log_entries < self::MAX_LOG_ENTRIES) {
            $this->getLogger('SyncHelper_'.$identificator)
                ->info('MailInOne::plentymiosync.execLog', [ $key => $value ]);
            $this->count_log_entries++;
        }
    }



    /**
    * @internal dev debugging only: commented
    *
    */
    private function helperLogHndlgDcsn() {
        $do_nothing='!';
        /* DEV LOG -> turned off for production
        $this->getLogger('SyncHelper_RebeccaFolders')
         ->info('MailInOne::plentymiosync.execLog', ['handling_decisions' => $this->log_stats['REBECCA_FOLDERS'] ]);

        $this->getLogger('SyncHelper_RebeccaEvent')
         ->info('MailInOne::plentymiosync.execLog', ['handling_decisions' => $this->log_stats['REBECCA_EVENT'] ]);

        $this->getLogger('SyncHelper_RebeccaResult')
         ->info('MailInOne::plentymiosync.execLog', ['handling_decisions' => $this->log_stats['REBECCA_RESULT'] ]);


        $loop=0;
        $temp=[];
        foreach( $this->log_stats['MA'] as $hd ) {
            if( $loop < 25 ) {
                $temp[] = $hd;
                $loop++;
            } else {
                $this->getLogger('SyncHelper_MA')
                     ->info('MailInOne::plentymiosync.execLog', ['MA' => $temp ]);
                $loop = 0;
                $temp=[];
            }
        }
        $this->getLogger('SyncHelper_MA')
            ->info('MailInOne::plentymiosync.execLog', ['MA' => $temp ]);


        $loop=0;
        $temp=[];
        foreach( $this->log_stats['MA2'] as $hd ) {
            if( $loop < 25 ) {
                $temp[] = $hd;
                $loop++;
            } else {
                $this->getLogger('SyncHelper_MA2')
                     ->info('MailInOne::plentymiosync.execLog', ['MA2' => $temp ]);
                $loop = 0;
                $temp=[];
            }
        }
        $this->getLogger('SyncHelper_MA2')
            ->info('MailInOne::plentymiosync.execLog', ['MA2' => $temp ]);



        $loop=0;
        $temp=[];
        foreach( $this->log_stats['CE'] as $hd ) {
            if( $loop < 25 ) {
                $temp[] = $hd;
                $loop++;
            } else {
                $this->getLogger('SyncHelper_CE')
                     ->info('MailInOne::plentymiosync.execLog', ['CE' => $temp ]);
                $loop = 0;
                $temp=[];
            }
        }
        $this->getLogger('SyncHelper_CE')
            ->info('MailInOne::plentymiosync.execLog', ['CE' => $temp ]);


        $loop=0;
        $temp=[];
        foreach( $this->log_stats['CE2'] as $hd ) {
            if( $loop < 25 ) {
                $temp[] = $hd;
                $loop++;
            } else {
                $this->getLogger('SyncHelper_CE2')
                     ->info('MailInOne::plentymiosync.execLog', ['CE2' => $temp ]);
                $loop = 0;
                $temp=[];
            }
        }

        $this->getLogger('SyncHelper_CE2')
            ->info('MailInOne::plentymiosync.execLog', ['CE2' => $temp ]);



        $loop=0;
        $temp=[];
        foreach( $this->log_stats['PLENTY_FOLDERS'] as $hd ) {
            if( $loop < 25 ) {
                $temp[] = $hd;
                $loop++;
            } else {
                $this->getLogger('SyncHelper_PlentyFolderCheck')
                     ->info('MailInOne::plentymiosync.execLog', ['handling_decisions' => $temp ]);
                $loop = 0;
                $temp=[];
            }
        }


        $loop=0;
        $temp=[];
        foreach( $this->log_stats['HNDLNG_INST_FOLDEREXIT'] as $hd ) {
            if( $loop < 100 ) {
                $temp[] = $hd;
                $loop++;
            } else {
                $this->getLogger('SyncHelper_HndlngDcsnEXIT')
                     ->info('MailInOne::plentymiosync.execLog', ['handling_decisions' => $temp ]);
                $loop = 0;
                $temp=[];
            }
        }

        $loop=0;
        $temp=[];
        foreach( $this->log_stats['FOUND_HNDLNG_INST_DN'] as $hd ) {
            if( $loop < 50 ) {
                $temp[] = $hd;
                $loop++;
            } else {
                $this->getLogger('SyncHelper_HndlngDcsnDN')
                     ->info('MailInOne::plentymiosync.execLog', ['handling_decisions' => $temp  ]);
                $loop = 0;
                $temp=[];
            }
        }


        $loop=0;
        $temp=[];
        foreach(  $this->log_stats['FOUND_HNDLNG_INST'] as $hd ) {
            if( $loop < 25) {
                $temp[] = $hd;
                $loop++;
            } else {
                $this->getLogger('SyncHelper_HndlngDcsnFound')
                     ->info('MailInOne::plentymiosync.execLog', ['handling_decisions' => $temp  ]);
                $loop = 0;
                $temp=[];
            }
        }


        $loop=0;
        $temp=[];
        foreach(  $this->log_stats['COUNT'] as $hd ) {
            if( $loop < 25 ) {
                $temp[] = $hd;
                $loop++;
            } else {
                $this->getLogger('SyncHelper_HndlngDcsnCount')
                     ->info('MailInOne::plentymiosync.execLog', ['count' => $temp  ]);
                $loop = 0;
                $temp=[];
            }
        }



        $loop=0;
        $temp=[];
        foreach( $this->log_stats['CONTACT_DATA'] as $hd ) {
            if( $loop < 20 ) {
                $temp[] = $hd;
                $loop++;
            } else {
                $this->getLogger('SyncHelper_ContactData')
                     ->info('MailInOne::plentymiosync.execLog', ['contacts' => $temp  ]);
                $loop = 0;
                $temp=[];
            }
        }
         *
         */
    }




    /**
     * @internal run inital data synchronisation, if neccessary => automated version, since gui code
     * @author jsr|treaction ag
     * @version 3.0.2
     * @deprecated <b>!!currently unused!!</b>
     * @access private <i> change to public if might be used outside of SyncHelper::class</i>
     * @return bool
     */

    private function mioCheckForInitalDataSynchronisation()
    {
        $ret = false;

        $res = $this->mioHasInitalDataFromPlenty();

        switch ($res) {
            case (-1):
                // either no custom fields in mio or error occured => raise error / write log / send email
                $this->writeLogToMessageContainer('ERR',__FUNCTION__.'|'.__LINE__."|mioHasInitalDataFromPlenty indicated error in mioGetCustomFieldsFromMio |");
                break;
            case (0):
                $this->writeLogToMessageContainer('INFO',__FUNCTION__.'|'.__LINE__."|mioHasInitalDataFromPlenty indicated run inital snyc|");
                $ret = ($this->mioRunDataSynchronisationInitial()) ? true : false; // there where custom fields in MIO, but inital transfer indicator custom field wasn't set
                // if run was successfull call mioConfirmInitalDataSynchronisation() and set $ret = true
                break;
            case (1):
                $this->writeLogToMessageContainer('INFO',__FUNCTION__.'|'.__LINE__."|mioHasInitalDataFromPlenty indicated do not run inital snyc|");
                $ret = true; // do nothing,
                break;
            default:
                break;
        }

        return $ret;
    }

    /**
     * commended -> requirement change. leave code for futher use
     * @deprecated 3.0.2
     */
    private function helperGetMioContactsWithCustomFields():array
    {
        $cf = json_decode(json_encode($this->mioGetCustomFieldsFromMio()));
        $custom_fields = [];
        // get only MIO custom_fields that begin with "plenty"
        foreach( $cf->custom_fields as $name => $v){

           if(1 === preg_match('#^(plenty){1}.*#', $name)) $custom_fields[]=$name;
        }

        $this->getLogger('SyncHelper_MioPlentyCustFields')
                 ->info('MailInOne::plentymiosync.execLog', ['mio_plentyonly_cust_fields' => $custom_fields ]);

        try {
            $temp = $this->mio->getAllMioContactsWithCustomFields(['apiKey'=>$this->mio_api_key, 'custom_fields'=> $custom_fields]);
        } catch (\Exception $exc) {
            $this->getLogger('SyncHelper_MioContCustFldErr')
                 ->error('MailInOne::plentymiosync.errLog', ['tracelog' => $exc->getTraceAsString() ]);
            return [];
        }


        $ret_arr = [];
        foreach( $temp as $record) {
            $ret_arr[$record->email[0]] = [ 'mio_id' => $record->id[0], 'mio_custom_fields' => $record->custom_fields[0] ];
        }
        return $ret_arr;
    }

    /**
     * @internal DEPRECATED: senseless to post single records, since Apache allows max. 2GB in post body and nginx up to 2^63GB. Runs single record data synchronisation as delta snyc and set indicator custom field (see const INITAL_DATATRANSFER_CUST_FLD) in MIO
     * @author jsr|treaction ag
     * @deprecated since version 3.0.1 / 3.0.2
     * @access private
     * @return bool
     */

    private function mioExecSingleRecordDeltaDataSynchronisation()
    {
        $ret = false;

        // ==============
        //   DEPRECATED
        // ==============
        //
        // inital data synchronisation would go here
        // $ret = success

        $mio_inital_indicator_custom_field_set = $this->mioConfirmInitalDataSynchronisation();

        return ($ret && $mio_inital_indicator_custom_field_set);
    }


}