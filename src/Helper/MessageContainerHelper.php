<?php
namespace MailInOne\Helper;
// use Plenty\Modules\Helper;

/**
 * @deprecated since 3.0.2 
 * MessageContainerHelper (removed singleton implementation)
 * @internal use MessageContainerHelper::getSizeInBytes() to control maximum size of messagecontainer.<br>MAXIMUM number of messages=1.000.<br>1.000 messages with average message length of 200 chars need aprox. 300KB mem
 * @example $mc=MessageContainerHelper::getInstance(); $mc::addMessage('your message goes here'); $csv=$mc::getAllMessages('CSV'); $mc::restAll();
 * @author jsr|treaction ag
 * @version since 3.0.2
 */
class MessageContainerHelper // extends Helper
{

  const MC_MAX_NUMBER_OF_MESSAGES=1000;

  private $mc_messages;

  private $mc_index;


  private function __construct()
  {
      $this->mcInit();
  }


  /**
   * @internal reset the message container. all messages are deleted
   * @author jsr|treaction ag
   * @version since 3.0.2
   * @return void
   */
  public function mcInit()
  {
    $this->mc_index=0;
    $this->mc_messages = [];
  }

  /**
   * @internal adds a message to messagecontainer with timestamp
   * @author jsr|treaction ag
   * @version since 3.0.2
   * @param string $message
   * @return bool true, if no message did not exceed 1000 Messages, otherwise false
   */
  public function mcAddMessage( string $message ):bool
  {
    if( $this->mcGetNoOfMessages() < self::MC_MAX_NUMBER_OF_MESSAGES )  {
        array_push( $this->mc_messages, ['t' => date('Y-m-d H:i:s'),'m'=>$message] );
        return true;
    }
    else {
        return false;
    }
  }


  /**
   * @author jsr|treaction ag
   * @version since 3.0.2
   * @param string $format JSON|CSV
   * @return string all container messages in either JSON or CSV format. E.g. CSV: seqno;timestamp;message
   */
  public function mcGetAllMessages(string $format='JSON'):string
  {
      if( $format === 'JSON') {

          return json_encode( $this->mc_messages, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE|JSON_FORCE_OBJECT);

      } elseif ($format==='CSV') {

          $ret = '';

          foreach( $this->mc_messages as $k => $v ) {
              $ret .= $k.';'.$v['t'].';'.$v['m']."\n";
          }

          return $ret;

      } else {

          return '';
      }
  }


  /**
   * @author jsr|treaction ag
   * @version since 3.0.2
   * @return int
   */
  public function mcGetNoOfMessages():int
  {
      return count($this->mc_messages);
  }
}