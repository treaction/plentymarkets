<?php
namespace MailInOne\Services;

use MailInOne\Services\LibService;

/**
 * Class SynchronizationService
 *
 * @package MailInOne\Services
 */
class SynchronizationService
{

    /**
     *
     * @var LibService
     */
    private $libService;

    /**
     * SynchronizationService constructor.
     *
     * @param libService $libService
     *
     */
    public function __construct(LibService $libService)
    // JSR corrected: public function __construct(libService $libService)

    {
        $this->libService = $libService;
    }

    /**
     *
     * @param array $parameter
     * @return array
     */
    public function syncContacts($parameter)
    {
        $response = $this->libService->libSyncContacts($parameter);

        return $response;
    }
    
    /**
     *
     * @param array $parameter
     * @return array
     */
    public function syncSinglesContact($parameter)
    {
        $response = $this->libService->libSyncSinglesContact($parameter);

        return $response;
    }

    /**
     *
     * @param array $parameter
     * @return array
     */
    public function setMarketingAutomation($parameter)
    {
        $response = $this->libService->libSetMarketingAutomation($parameter);

        return $response;
    }

    /**
     *
     * @param array $parameter
     * @return array
     */
    public function setTransaction($parameter)
    {
        $response = $this->libService->libSetTransaction($parameter);

        return $response;
    }


    /**
     *
     * @param $parameter = <b>[</b>
	$maileon["contactId"],
	$maileon['email'],
	$maileon["firstName"],
	$maileon["lastName"],
	$maileon["orderId"],
	$maileon['gender'],
	$maileon['netTotal'],
	$maileon['grossTotal'],
	$maileon['item']['id'],
	$maileon['item']['orderItemName'],
	$maileon['item']['quantity'],
	$maileon['item']['currency'],
	$maileon['item']['priceGross'],
      <b>]</b>,
      <b>[</b>'kontaktereignisID'<b>]</b>,
      <b>[</b>'apiKey'<b>]</b>,
     * @return array
     */
    public function setTransactionEvent($parameter)
    {
        $response = $this->libService->libSetTransactionEvent($parameter);
        return $response;
    }


    /**
     *
     * @param $parameter = <b>[</b>
	$maileon["contactId"],
	$maileon['email'],
	$maileon["firstName"],
	$maileon["lastName"],
	$maileon["orderId"],
	$maileon['gender'],
	$maileon['netTotal'],
	$maileon['grossTotal'],
	$maileon['item']['id'],
	$maileon['item']['orderItemName'],
	$maileon['item']['quantity'],
	$maileon['item']['currency'],
	$maileon['item']['priceGross'],
      <b>]</b>,
      <b>[</b>'kontaktereignisID'<b>]</b>,
      <b>[</b>'apiKey'<b>]</b>,
     * @return array
     */
    public function triggerTransactionEvent($parameter)
    {
        $response = $this->libService->libSetTriggerTransactionEvent($parameter);
        return $response;
    }


    /**
     *
     * @param array $parameter
     * @return array
     */
    public function setDoi($parameter)
    {
        $response = $this->libService->libSetDoi($parameter);

        return $response;
    }
    /**
     *
     * @param array $parameter
     * @return array
     */
    public function unsubscribe($parameter)
    {
        $response = $this->libService->libUnsubscribe($parameter);

        return $response;
    }

    /**
     *
     * @param array $parameter
     * @return array
     */
    public function getCampaign($parameter)
    {
        $response = $this->libService->libGetCampaign($parameter);

        return $response;
    }

       /**
     *
     * @param array $parameter
     * @return array
     */
    public function getTransactionType($parameter)
    {
        $response = $this->libService->libGetTransactionType($parameter);

        return $response;
    }


        /**
     *
     * @param array $parameter
     * @return array
     */
    public function getDoiName($parameter)
    {
        $response = $this->libService->libGetDoiName($parameter);

        return $response;
    }
    /**
     *
     * @param array $parameter
     * @return array
     */
    public function getConatctEvent($parameter)
    {
        $response = $this->libService->libGetConatctEvent($parameter);

        return $response;
    }

        /**
     *
     * @param array $parameter
     * @return array
     */
    public function getDoiMailings($parameter)
    {
        $response = $this->libService->libGetDoiMailings($parameter);

        return $response;
    }
    /**
     *
     * @param array $parameter
     * @return array
     */
    public function getTargetGroup($parameter)
    {
        $response = $this->libService->libGetTargetGroup($parameter);

        return $response;
    }

    /**
     *
     * @param array $parameter
     * @return array
     */
    public function getContactFilter($parameter)
    {
        $response = $this->libService->libGetContactFilter($parameter);

        return $response;
    }

    /**
     *
     * @param array $parameter
     * @return array
     */
    public function createCustomField($parameter)
    {
        $response = $this->libService->libCreateCustomField($parameter);

        return $response;
    }

    /**
     *
     * @param array $parameter
     * @return array
     */
    public function deleteCustomField($parameter)
    {
        $response = $this->libService->libDeleteCustomField($parameter);

        return $response;
    }


    /**
     * @author JSR|treaction ag
     * @param array $parameter
     * @return array an array of strings containing MIO custom fields
     */
    public function getCustomFields($parameter)
    {
        return $this->libService->libGetCustomFields($parameter);
    }


    /**
     * @author JSR|treaction ag
     * @param string $mio_contact_email email address to get MIO Double Opt In permission from
     * @return string|int mail-in-one contact DOI permission
     */
    public function getDoiPermissionByEmail($mio_contact_email)
    {
        return $this->libService->libGetDoiPermissionByEmail($mio_contact_email);
    }


    /**
     * @author JSR|treaction ag
     * @param string MIO ApiKey
     * @return array mail-in-one contact DOI permission as $arr[(string)<unique_email_address>] = (int)<permission>
     */
    public function getAllMioEmailContactsWithDoiPermission($mio_api_key)
    {
        return $this->libService->libGetAllMioEmailContactsWithDoiPermission(['apiKey'=>$mio_api_key]);
    }

    /**
     * @author jsr|treaction ag
     * @param string $mio_api_key
     * @return type
     */
    public function getAllMioContactsWithCustomFields($param)
    {
        try {
            $ret =  $this->libService->libGetAllMioContactsWithCustomFields(['apiKey'=>$param['apiKey'], 'custom_fields'=>$param['custom_fields'] ]);
        } catch (\Exception $exc) {
            throw $exc;
        }
        return $ret; //json_decode(json_encode($ret));
    }

    /**
     * @internal returns mio <b>transaction types</b> as array with id and name. Caution: <b>transaction types</b> are also called <b>Kontaktereignisse</b> or <b>contact events</b>!
     * @return array $arr[(string)<unique_transaction_type_name>] = (int)<transaction_type_id>
     */
    public function getAllMioTransactionTypes()
    {
        return $this->libService->libGetAllMioTransactionTypes();
    }


}
