<?php
namespace MailInOne\Services;

use Plenty\Modules\Account\Contact\Contracts\ContactRepositoryContract;
use Plenty\Modules\Account\Newsletter\Contracts\NewsletterRepositoryContract;
use Plenty\Modules\Order\Status\Contracts\OrderStatusRepositoryContract;
use Plenty\Modules\Account\Contact\Models\Contact;

/**
 * Class AccountService
 *
 * @package MailInOne\Services
 */
class AccountService
{

    /**
     * AccountService constructor.
     */
    public function __construct()
    {}

    /**
     *
     * @return array
     */
    public function listAllRecipients()

    {

        /** @var NewsletterRepositoryContract $contactList */
        $contactList = pluginApp(\Plenty\Modules\Account\Newsletter\Contracts\NewsletterRepositoryContract::class);

        $listAllRecipients = $contactList->listAllRecipients();

        return $listAllRecipients;
    }

    /**
     * @see https://developers.plentymarkets.com/api-doc/account#account_models_contact
     * @param int $contactId
     * @return array
     */
    public function findContactById($contactId)
    {

        /** @var ContactRepositoryContract $contacts */
        $contacts = pluginApp(\Plenty\Modules\Account\Contact\Contracts\ContactRepositoryContract::class);

        $contact = $contacts->findContactById($contactId, []);

        return $contact->toArray();
    }

    /**
     *
     * @param string $email
     * @return int
     */
    public function getContactIdByEmail($email)

    {

        /** @var ContactRepositoryContract $contacts */
        $contacts = pluginApp(\Plenty\Modules\Account\Contact\Contracts\ContactRepositoryContract::class);

        $contactId = $contacts->getContactIdByEmail($email);

        return $contactId;
    }
    /**
     *
     * @return array
     */
    public function listAllFolders()

    {

        /** @var NewsletterRepositoryContract $list */
        $list = pluginApp(\Plenty\Modules\Account\Newsletter\Contracts\NewsletterRepositoryContract::class);

        $listAllFolders = $list->listAllFolders();

        return $listAllFolders;
    }

     /**
     *
     * @return mixed
     */
    public function getAllStatusOrder()

    {

        /** @var NewsletterRepositoryContract $listStatus */
        $listStatus = pluginApp(\Plenty\Modules\Order\Status\Contracts\OrderStatusRepositoryContract::class);

        $allStatusOrder = $listStatus->all();

        return $allStatusOrder;
    }
    /**
     * @param float $statusId
     * @return mixed
     */
    public function getStatusOrder($statusId)
    {

        /** @var OrderStatusRepositoryContract $listStatus */
        $listStatus = pluginApp(\Plenty\Modules\Order\Status\Contracts\OrderStatusRepositoryContract::class);

        $statusOrder = $listStatus->get($statusId);

        return $statusOrder;
    }
    /**
     *
     * @param int $folderId
     * @return mixed
     */
    public function listFolderById($folderId)

    {

        /** @var NewsletterRepositoryContract $list */
        $list = pluginApp(\Plenty\Modules\Account\Newsletter\Contracts\NewsletterRepositoryContract::class);

        $folder = $list->listFolderById($folderId);

        return $folder;
    }


    /**
     *
     * @return array all plenty contacts from contact model see https://developers.plentymarkets.com/api-doc/Account#element_23
     */
    public function getAllPlentyAccountContacts():array
    {
        $cntct = pluginApp(\Plenty\Modules\Account\Contact\Models\Contact::class);
        return $cntct->toArray();  // don't use \Plenty\Modules\Account\Contact\Models\Contact::all()->toArray(); ==> thats undefined!!!
    }

    /**
     *
     * @param string $name
     * @return string
     */
    public function validationCharacter($name)
    {
       # $input = htmlspecialchars_decode($name);
       $input = $name;
        $character = array(
            '&' => '-',
            '$' => '-',
            '%' => '-',
            '{' => '-',
            '}' => '-',
            '\\' => '-',
            '/' => '-',
            '`' => '-',
            'â‚¬' => '-',
            '*' => '-',
            '+' => '-',
            '~' => '-',
            '<' => '-',
            '>' => '-',
            ';' => '-',
            '|' => '-',
            '#' => '-'
        );
        foreach ($character as $key => $value) {
            $ouput = \str_replace($key, $value, $input);
            $input = $ouput;
        }
        $strlen = strlen($ouput);
          if($strlen < 3){
              $ouputlen = $ouput.'--';
          }else{
              $ouputlen = $ouput;
          }
          return $ouputlen;

    }
}