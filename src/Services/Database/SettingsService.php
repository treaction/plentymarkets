<?php

//strict

namespace MailInOne\Services\Database;

use MailInOne\Services\SynchronizationService;
use MailInOne\Services\AccountService;
use MailInOne\Models\Database\Settings;
use MailInOne\Models\Database\LoginUrl;
use MailInOne\Models\Database\Cronjob;
use MailInOne\Models\Database\LogSync;
use MailInOne\Models\Database\Synchronisation;
use MailInOne\Models\Database\ContactEvent;
use MailInOne\Models\Database\OrderEvent;
use Plenty\Modules\Plugin\DataBase\Contracts\DataBase;
use MailInOne\Services\LibService;

/**
 * Class SettingsService
 * @package MailInOne\Services
 */
class SettingsService extends DatabaseBaseService {

    /**
     * @var LibService
     */
    private $libService;
    protected $tableName = 'settings';

    /**
     *
     * @var SynchronizationService
     */
    private $synchronizationService;

    /**
     *
     * @var AccountService
     */
    private $accountService;

    /**
     * SettingsService constructor.
     * @param DataBase $dataBase
     * @param libService $libService
     * @param SynchronizationService $SynchronizationService
     * @param AccountService $accountService
     */
    public function __construct(
    DataBase $dataBase, LibService $libService, SynchronizationService $SynchronizationService, AccountService $accountService
    ) {
        parent::__construct($dataBase);
        $this->libService = $libService;
        $this->synchronizationService = $SynchronizationService;
        $this->accountService = $accountService;
    }

    /**
     * @param $setting
     * @return bool|null
     */
    public function saveSetting($setting) {
        if ($setting) {
            /** @var Settings $settingModel */
            $settingModel = pluginApp(Settings::class);
            $settingModel->apiKey = $setting['apiKey'];
            $settingModel->createdAt = date('Y-m-d H:i:s');
            if ($settingModel instanceof Settings) {
                $this->setValue($settingModel);
            }

            /** @var LoginUrl $loginModel */
            $loginModel = pluginApp(LoginUrl::class);
            $loginModel->createdAt = date('Y-m-d H:i:s');
            $loginModel->loginurl = $setting['loginurl'];
            if ($loginModel instanceof LoginUrl) {
                $this->setValue($loginModel);
            }

            /** @var Cronjob $cronjobModel */
            $cronjobModel = pluginApp(Cronjob::class);
            $cronjobModel->createdAt = date('Y-m-d H:i:s');
            $cronjobModel->cronjob = $setting['cronjob'];
            if ($cronjobModel instanceof Cronjob) {
                $this->setValue($cronjobModel);
            }
            /** @var Synchronisation $synchronisationModel */
            $synchronisationModel = pluginApp(Synchronisation::class);
            $synchronisationModel->createdAt = date('Y-m-d H:i:s');
            $synchronisationModel->modus = $setting['pickedValueModus'];
            $synchronisationModel->quantity = $setting['quantity'];
            if ($synchronisationModel instanceof Synchronisation) {
                $this->setValue($synchronisationModel);
            }
        }
        return TRUE;
    }

    /**
     * @param $eventId
     * @return bool|mixed
     */
    public function deleteEvent($eventId) {
        $eventModel = $this->getValue(ContactEvent::class, $eventId);
        $result = $this->deleteValue($eventModel);
        return json_encode($result);
    }

    /**
     * @param $eventcontact
     * @return bool|null
     */
    public function saveEvent($eventcontact) {

        if ($eventcontact) {
            /** @var ContactEvent $eventModel */
            $eventModel = pluginApp(ContactEvent::class);
            $eventModel->event = $eventcontact['event'];
            $eventModel->ordner = $eventcontact['ordner'];
            $eventModel->integration = $eventcontact['integration'];
            if (!empty($eventcontact['kontaktereignis'])) {
                $kontaktereignisID = $eventcontact['kontaktereignis'];
                $kontaktereignis['apiKey'] = $this->getSettings();
                $kontaktereignis['kontaktereignisID'] = $kontaktereignisID;
                $kontaktereignisArray = (array) $this->synchronizationService->getTransactionType($kontaktereignis);
                $kontaktereignisName = $kontaktereignisArray["name"];
                $eventModel->kontaktereignisID = $kontaktereignisID;
                $eventModel->kontaktereignis = $kontaktereignisName;
            } else {
                $eventModel->kontaktereignis = '';
                $eventModel->kontaktereignisID = 0;
            }

            if (!empty($eventcontact['marketingAutomation'])) {
                $eventModel->marketingAutomation = $eventcontact['marketingAutomation'];
            } else {
                $eventModel->marketingAutomation = '';
            }
            if (!empty($eventcontact['doiKey'])) {
                $doiKey = $eventcontact['doiKey'];
                $doiMailing['apiKey'] = $this->getSettings();
                $doiMailing['doiKey'] = $doiKey;
                $doiName = $this->synchronizationService->getDoiName($doiMailing);

                $eventModel->doiName = (string) $doiName;
                $eventModel->doiKey = $doiKey;
            } else {
                $eventModel->doiKey = '';
            }
            if (!empty($eventcontact['isDoi'])) {
                $eventModel->isDoi = $eventcontact['isDoi'];
            } else {
                $eventModel->isDoi = 0;
            }
            $eventModel->createdAt = date('Y-m-d H:i:s');
            if ($eventModel instanceof ContactEvent) {
                $this->setValue($eventModel);
            }
        }
        return TRUE;
    }

    /**
     *
     * @param string $eventTyp
     * @return DataBase $mioEvent
     */
    public function getEventByTyp($eventTyp) {
        /**
         * @var DataBase $database
         */
        $database = pluginApp(DataBase::class);
        $mioEvent = $database->query(OrderEvent::class)
                ->where('mioEvent', '=', $eventTyp)
                ->where('isActiv', '=', 1)
                ->get();
        return $mioEvent;
    }

    /**
     * @param int $id
     * @param $eventcontact
     * @return bool
     */
    public function saveEventEdit($id, $eventcontact) {
        $result = false;

        if ($eventcontact) {
            /**
             * @var DataBase $database
             */
            $database = pluginApp(DataBase::class);
            $eventUpdateModel = $database->query(ContactEvent::class)
                    ->where('id', '=', $id)
                    ->get();
            $eventUpdate = $eventUpdateModel[0];

            if (!empty($eventcontact['event'])) {
                $eventUpdate->event = $eventcontact['event'];
            }
            if (!empty($eventcontact['ordner'])) {
                $eventUpdate->ordner = $eventcontact['ordner'];
            }
            if (!empty($eventcontact['integration'])) {
                $eventUpdate->integration = $eventcontact['integration'];
            }
            if (!empty($eventcontact['kontaktereignis'])) {
                $kontaktereignisId = $eventcontact['kontaktereignis'];
                $kontaktereignis['apiKey'] = $this->getSettings();
                $kontaktereignis['kontaktereignisID'] = $kontaktereignisId;
                $kontaktereignisArray = (array) $this->synchronizationService->getTransactionType($kontaktereignis);
                $kontaktereignisName = $kontaktereignisArray["name"];
                $eventUpdate->kontaktereignisID = $kontaktereignisId;
                $eventUpdate->kontaktereignis = $kontaktereignisName;
            }

            if (!empty($eventcontact['marketingAutomation'])) {
                $eventUpdate->marketingAutomation = $eventcontact['marketingAutomation'];
            }

            if ((!empty($eventcontact['doiKey'])) && ($eventcontact['isDoi'] == 1)) {
                $doiKey = $eventcontact['doiKey'];
                $doiMailing['apiKey'] = $this->getSettings();
                $doiMailing['doiKey'] = $doiKey;
                $doiName = $this->synchronizationService->getDoiName($doiMailing);

                $eventUpdate->doiName = (string) $doiName;
                $eventUpdate->doiKey = $doiKey;
                $eventUpdate->isDoi = 1;
            } else {
                $eventUpdate->doiName = ' ';
                $eventUpdate->doiKey = ' ';
            }

            if ($eventcontact['isDoi'] == 0) {
                $eventUpdate->isDoi = 0;
            }

            if ($eventUpdate instanceof ContactEvent) {
                $result = $this->setValue($eventUpdate);
            }
        }
        return $result;
    }

    /**
     * TODO (Info by JSR): change 'aktualisierte Kontakt' to 'aktualisierter Kontakt'
     * @return bool|null
     */
    public function saveEventInit() {
        $events = array(
            array(
                'event' => 'Neuer Kontakt',
                'ordner' => 'Kunde',
                'isDoi' => 0,
                'doiKey' => ''
            ),
            array(
                'event' => 'aktualisierte Kontakt',
                'ordner' => 'Kunde',
                'isDoi' => 0,
                'doiKey' => ''
            ),
            array(
                'event' => 'Neuer Kontakt',
                'ordner' => 'Interessent',
                'isDoi' => 1,
                'doiKey' => ''
            ),
            array(
                'event' => 'aktualisierte Kontakt',
                'ordner' => 'Interessent',
                'isDoi' => 1,
                'doiKey' => ''
            )
        );

        foreach ($events as $event) {

            /** @var ContactEvent $eventModel */
            $eventModel = pluginApp(ContactEvent::class);
            $eventModel->event = $event['event'];
            $eventModel->ordner = $event['ordner'];
            $eventModel->integration = 0;
            $eventModel->marketingAutomation = '';
            $eventModel->kontaktereignis = '';
            $eventModel->kontaktereignisID = 0;
            $eventModel->isDoi = $event['isDoi'];
            $eventModel->doiKey = $event['doiKey'];
            $eventModel->createdAt = date('Y-m-d H:i:s');

            if ($eventModel instanceof ContactEvent) {
                $this->setValue($eventModel);
            }
        }
        return TRUE;
    }

    /**
     * @param $countContact
     * @return bool|null
     */
    public function saveLogSync($countContact, $countDoi, $countEvent, $countMa) {
        if ((int) $countContact > 0) {
            /** @var LogSync $syncLogModel */
            $syncLogModel = pluginApp(LogSync::class);
            $syncLogModel->countContact = $countContact;
            $syncLogModel->event = $countEvent;
            $syncLogModel->marketingAutomation = $countMa;
            $syncLogModel->doi = $countDoi;
            $syncLogModel->date = date('Y-m-d H:i:s');
            if ($syncLogModel instanceof LogSync) {
                $this->setValue($syncLogModel);
            }
        }
        return TRUE;
    }

    /**
     * @return bool|null
     */
    public function resetLogSync() {

        /** @var LogSync $syncLogModel */
        $syncLogModel = pluginApp(LogSync::class);
        $syncLogModel->countContact = 0;
        $syncLogModel->event = 0;
        $syncLogModel->marketingAutomation = 0;
        $syncLogModel->doi = 0;
        #$syncLogModel->date = date('Y-m-d H:i:s');
        $syncLogModel->date = '2000-01-01 00:00:00';
        if ($syncLogModel instanceof LogSync) {
            $result = $this->setValue($syncLogModel);
        }

        return $result;
    }

    /**
     * Load Sync Log from the database
     *
     * @return array
     */
    public function getSyncInfo() {
        $results = $this->getValuesOrderBy(LogSync::class, 'id', 'desc');
        return $results;
    }

    /**
     * Load Event from the database
     *
     * @return array
     */
    public function getEvent() {
        $results = $this->getValuesOrderBy(ContactEvent::class, 'id', 'desc');
        return $results;
    }

    /**
     * Load Order Event from the database
     *
     * @return array
     */
    public function getAllOrderEvent() {
        $results = $this->getValuesOrderBy(OrderEvent::class, 'id', 'desc');
        return $results;
    }

    /**
     * @internal (comment by JSR): <b>unsafe function</b> -> random access on table rows -> expects that there is only one entry in table ... <br><b>TODO:</b> change setting table to a plugin's option table with key-value pairs (id unsigned int, option_key string, option_value string, createdAt date, updatedAt date) to be able to add further settings beside the one and only entry for ApiKey
     * Load apikey from the database
     * @return string Mail In One API Key entered by PlugIn-User, returns '--', if ApiKey was not set or found
     */
    public function getSettings() {
        $settings = array();
        $results = $this->getValuesOrderBy(Settings::class, 'id', 'desc');
        if ($results) {
            foreach ($results as $setting) {
                if ($setting instanceof Settings) {
                    $settings[] = $setting->apiKey; // whats that?  select by identifier or surrogate key.
                }
            }
        }

        if (is_null($settings[0])) {
            return '--';
        } else {
            return $settings[0]; // here the last ApiKey of settings-table is returned -> witch might be the wrong one ...
        }
    }

    /**
     * Load Cronjob from the database
     *
     * @return int
     */
    public function getCronjob() {
        $cronjob = array();
        $results = $this->getValuesOrderBy(Cronjob::class, 'id', 'desc');
        if ($results) {
            foreach ($results as $cron) {
                if ($cron instanceof Cronjob) {
                    $cronjob[] = $cron->cronjob;
                }
            }
        }
        if (is_null($cronjob[0])) {
            return 5;
        } else {
            return (int) $cronjob[0];
        }
    }

    /**
     * Load Login URL from the database
     *
     * @return string
     */
    public function getLoginUrl() {
        $loginurl = array();
        $results = $this->getValuesOrderBy(LoginUrl::class, 'id', 'desc');
        if ($results) {
            foreach ($results as $logurl) {
                if ($logurl instanceof LoginUrl) {
                    $loginurl[] = $logurl->loginurl;
                }
            }
        }
        if (is_null($loginurl[0])) {
            return '--';
        } else {
            return $loginurl[0];
        }
    }

    /**
     * Load apikey from the database
     *
     * @return array
     */
    public function getCreateDatum() {
        $settings = array();
        $results = $this->getValuesOrderBy(Settings::class, 'id', 'desc');
        if ($results) {
            foreach ($results as $setting) {
                if ($setting instanceof Settings) {
                    $settings[] = $setting->createdAt;
                }
            }
        }

        return $settings[0];
    }

    /**
     * Load getLastSync from the database
     *
     * @return string LogSync date
     */
    public function getLastSync() {
        $sync = array();
        $results = $this->getValuesOrderBy(LogSync::class, 'id', 'desc');
        if ($results) {
            foreach ($results as $syn) {
                if ($syn instanceof LogSync) {
                    $sync[] = $syn->date;
                }
            }
        }

        return $sync[0];
    }

    public function sendmails($account) {

        $parmsArray = ['account' => $account];

        $response = $this->libService->libSendmails($parmsArray);

        return $response;
    }

    public function getCampaign($apiKey) {

        $parmsArray = ['apiKey' => $apiKey];
        $response = $this->libService->libGetCampaign($parmsArray);

        return $response;
    }

    public function sendTestmail($param) {

        $response = $this->libService->libSendTestmails($param);

        return $response;
    }

    /**
     * @param $orderEvent
     * @return bool|null
     */
    public function saveOrderEvent($orderEvent) {

        if ($orderEvent) {
            /** @var OrderEvent $orderEventModel */
            $orderEventModel = pluginApp(OrderEvent::class);
            $orderEventModel->statusId = 0;
            if (!empty($orderEvent['event'])) {
                $orderEventModel->name = $orderEvent['event'];
            }
            if (!empty($orderEvent['statusId'])) {
                $orderEventModel->event = (string) $orderEvent['statusId'];
            }

            $orderEventModel->integration = $orderEvent['integration'];
            $orderEventModel->mioEvent = $orderEvent['mioEvent'];
            if (!empty($orderEvent['kontaktereignis'])) {
                $kontaktereignisID = $orderEvent['kontaktereignis'];
                $kontaktereignis['apiKey'] = $this->getSettings();
                $kontaktereignis['kontaktereignisID'] = $kontaktereignisID;
                $kontaktereignisArray = (array) $this->synchronizationService->getTransactionType($kontaktereignis);
                $kontaktereignisName = $kontaktereignisArray["name"];
                $orderEventModel->kontaktereignisID = $kontaktereignisID;
                $orderEventModel->kontaktereignis = $kontaktereignisName;
            } else {
                $orderEventModel->kontaktereignis = '';
                $orderEventModel->kontaktereignisID = 0;
            }

            if (!empty($orderEvent['marketingAutomation'])) {
                $orderEventModel->marketingAutomation = $orderEvent['marketingAutomation'];
            } else {
                $orderEventModel->marketingAutomation = '';
            }

            if (!empty($orderEvent['isActiv'])) {
                $orderEventModel->isActiv = $orderEvent['isActiv'];
            } else {
                $orderEventModel->isActiv = 0;
            }
            $orderEventModel->createdAt = date('Y-m-d H:i:s');
            if ($orderEventModel instanceof OrderEvent) {
                $this->setValue($orderEventModel);
            }
        }
        return TRUE;
    }

    /**
     * @param int $id
     * @param $orderEvent
     * @return bool
     */
    public function saveOrderEventEdit($id, $orderEvent) {
        $result = false;

        if ($orderEvent) {
            /**
             * @var DataBase $database
             */
            $database = pluginApp(DataBase::class);
            $eventUpdateModel = $database->query(OrderEvent::class)
                    ->where('id', '=', $id)
                    ->get();
            $eventUpdate = $eventUpdateModel[0];

            if (!empty($orderEvent['event'])) {
                $eventUpdate->name = $orderEvent['event'];
            }

            if (!empty($orderEvent['statusId'])) {
                # $eventUpdate->statusId = $orderEvent['statusId'];
                $eventUpdate->event = (string) $orderEvent['statusId'];
            }
            if (!empty($orderEvent['integration'])) {
                $eventUpdate->integration = $orderEvent['integration'];
            }
            if (!empty($orderEvent['mioEvent'])) {
                $eventUpdate->mioEvent = $orderEvent['mioEvent'];
            }
            if (!empty($orderEvent['kontaktereignis'])) {
                $kontaktereignisID = $orderEvent['kontaktereignis'];
                $kontaktereignis['apiKey'] = $this->getSettings();
                $kontaktereignis['kontaktereignisID'] = $kontaktereignisID;
                $kontaktereignisArray = (array) $this->synchronizationService->getTransactionType($kontaktereignis);
                $kontaktereignisName = $kontaktereignisArray["name"];
                $eventUpdate->kontaktereignisID = $kontaktereignisID;
                $eventUpdate->kontaktereignis = $kontaktereignisName;
            }

            if (!empty($orderEvent['marketingAutomation'])) {
                $eventUpdate->marketingAutomation = $orderEvent['marketingAutomation'];
            }

            if (!empty($orderEvent['isActiv'])) {
                $eventUpdate->isActiv = $orderEvent['isActiv'];
            }

            if ($eventUpdate instanceof OrderEvent) {
                $result = $this->setValue($eventUpdate);
            }
        }
        return $result;
    }

    /**
     * @param $eventId
     * @return bool|mixed
     */
    public function deleteOrderEvent($eventId) {
        $eventModel = $this->getValue(OrderEvent::class, $eventId);
        $result = $this->deleteValue($eventModel);
        return json_encode($result);
    }

    /**
     * @internal send log data for error-, warning-, info-logging
     * @author jsr|treaction ag
     * @version 3.0.2
     * @param string $log_data
     * @return array
     */
    public function sendLog(string $log_data) {

        $parmsArray = ['log_data' => $log_data];

        $response = $this->libService->libSendLog($parmsArray);

        return $response;
    }

    public function getSynchronisation() {
        $synchronisations = array();
        $results = $this->getValuesOrderBy(Synchronisation::class, 'id', 'asc');
        if ($results) {
            foreach ($results as $synchronisation) {
                if ($synchronisation instanceof Synchronisation) {
                   $synchronisations[0] = $synchronisation->modus;
                   $synchronisations[1] = $synchronisation->quantity;
                    
                }
            }
        }
        return $synchronisations;
    }

}
