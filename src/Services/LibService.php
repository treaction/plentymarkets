<?php
namespace MailInOne\Services;

use Plenty\Modules\Plugin\Libs\Contracts\LibraryCallContract;

class LibService
{

    /**
     *
     * @var LibraryCallContract
     */
    private $libraryCall;

    /**
     * LibService constructor.
     *
     * @param LibraryCallContract $libraryCallContract
     */
    public function __construct(LibraryCallContract $libraryCallContract)
    {
        $this->libraryCall = $libraryCallContract;
    }

    /**
     * execute the lib call
     *
     * @param  $params
     * @return array
     */
    public function libSyncContacts($params)
    {
        return $this->executeLibCall('MailInOne::syncContacts', [
            'apiKey' => $params['apiKey'],
            'syncContactsWithOutId' => $params['syncContactsWithOutId'],
            'syncUpdateContactsWithId' => $params['syncUpdateContactsWithId'],
            'syncNewContactsWithId' => $params['syncNewContactsWithId']
        ]);
    }

       /**
     * execute the lib call
     *
     * @param  $params
     * @return array
     */
    public function libSyncSinglesContact($params)
    {
        return $this->executeLibCall('MailInOne::syncSinglesContact', [
            'apiKey' => $params['apiKey'],
            'syncContactsWithOutId' => $params['syncContactsWithOutId'],
            'syncUpdateContactsWithId' => $params['syncUpdateContactsWithId'],
            'syncNewContactsWithId' => $params['syncNewContactsWithId']
        ]);
    }
    /**
     * execute the lib call
     *
     * @param $params
     * @return array
     */
    public function libUnsubscribe($params)
    {
        return $this->executeLibCall('MailInOne::unsubscribe', [
            'maileon' => $params['maileon'],
            'apiKey' => $params['apiKey']
        ]);
    }

    /**
     * execute the lib call
     *
     * @param $params
     * @return array
     */
    public function libSendmails($params)
    {
        return $this->executeLibCall('MailInOne::sendmails', [
            'account' => $params['account']
        ]);
    }


    /**
     *
     * @param string $mio_contact_email email address to get MIO Double Opt In permission from
     * @return string|int mail-in-one contact DOI permission
     */
    public function libGetDoiPermissionByEmail($mio_contact_email)
    {
        return $this->executeLibCall('MailInOne::getDoiPermissionByEmail', [
            'mio_contact_email' => $mio_contact_email
        ]);
    }

    /**
     * @param array $params ['apiKey' => Mail-In-One-APIKEY]
     * @return array arr[<unique-email-adr>] = <doi-permission>
     */
    public function libGetAllMioEmailContactsWithDoiPermission(array $params)
    {
        return $this->executeLibCall('MailInOne::getAllMioEmailContactsWithDoiPermission',[
            'apiKey' => $params['apiKey']
        ]);
    }

    /**
     *
     * @param array $params ['apiKey' => Mail-In-One-APIKEY]
     * @return type
     */
    public function libGetAllMioContactsWithCustomFields(array $params)
    {
        try {
            $ret = $this->executeLibCall('MailInOne::getAllMioContactsWithCustomFields',[
                'apiKey' => $params['apiKey'],
                'custom_fields' => $params['custom_fields'],
            ]);
        } catch (\Exception $exc) {
            throw $exc;
        }
        return $ret;
    }


    /**
     *
     * @return array arr['unique_transaction_type_name'] = (int)<transaction_type_id>
     */
    public function libGetAllMioTransactionTypes()
    {
        return $this->executeLibCall('MailInOne::getAllMioTransactionTypes',[]);
    }

    /**
     * execute the lib call
     *
     * @param $params
     * @return array
     */
    public function libSendTestmails($params)
    {
        return $this->executeLibCall('MailInOne::sendTestmails', [
            'daten' => $params
        ]);
    }
    /**
     * execute the lib call
     *
     * @param $params
     * @return array
     */
    public function libGetCampaign($params)
    {
        return $this->executeLibCall('MailInOne::campaign', [
            'apiKey' => $params['apiKey']
        ]);
    }



     /**
     * execute the lib call
     *
     * @param $params
     * @return array
     */
    public function libGetTransactionType($params)
    {
        return $this->executeLibCall('MailInOne::transactionType', [
            'apiKey' => $params['apiKey'],
             'kontaktereignisID' => $params['kontaktereignisID']
        ]);
    }

      /**
     * execute the lib call
     *
     * @param $params
     * @return array
     */
    public function libSetTransaction($params)
    {
        return $this->executeLibCall('MailInOne::transaction', [
            'apiKey' => $params['apiKey'],
            'kontaktereignisID' => $params['kontaktereignisID'],
            'maileon' => $params['maileon']
        ]);
    }

        /**
     * execute the lib call
     *
     * @param $params
     * @return array
     */
    public function libSetTransactionEvent($params)
    {
        return $this->executeLibCall('MailInOne::transactionEvent', [
            'apiKey' => $params['apiKey'],
            'kontaktereignisID' => $params['kontaktereignisID'],
            'maileon' => $params['maileon']
        ]);
    }


    /**
     *
     * @param type $params
     * @return type
     */
    public function libSetTriggerTransactionEvent($params)
    {
        return $this->executeLibCall('MailInOne::triggerTransactionEvent', [
            'apiKey' => $params['apiKey'],
            'kontaktereignisID' => $params['kontaktereignisID'],
            'maileon' => $params['maileon']
        ]);
    }

        /**
     * execute the lib call
     *
     * @param $params
     * @return array
     */
    public function libSetDoi($params)
    {
        return $this->executeLibCall('MailInOne::setDOI', [
            'apiKey' => $params['apiKey'],
            'doiKey' => $params['doiKey'],
            'maileon' => $params['maileon']
        ]);
    }
         /**
     * execute the lib call
     *
     * @param $params
     * @return array
     */
    public function libGetDoiName($params)
    {
        return $this->executeLibCall('MailInOne::doiName', [
              'apiKey' => $params['apiKey'],
              'doiKey' => $params['doiKey']
        ]);
    }

          /**
     * execute the lib call
     *
     * @param $params
     * @return array
     */
    public function libSetMarketingAutomation($params)
    {
        return $this->executeLibCall('MailInOne::marketingAutomation', [
              'apiKey' => $params['apiKey'],
              'programId' => $params['programId'],
              'maileon' => $params['maileon']
        ]);
    }
     /**
     * execute the lib call
     *
     * @param $params
     * @return array
     */
    public function libGetConatctEvent($params)
    {
        return $this->executeLibCall('MailInOne::conatctEvent', [
            'apiKey' => $params['apiKey']
        ]);
    }


    /**
     * execute the lib call
     *
     * @param $params
     * @return array
     */
    public function libGetDoiMailings($params)
    {
        return $this->executeLibCall('MailInOne::doiMailing', [
            'apiKey' => $params['apiKey']
        ]);
    }
    /**
     * execute the lib call
     *
     * @param
     *            $params
     * @return array
     */
    public function libGetTargetGroup($params)
    {
        return $this->executeLibCall('MailInOne::targetGroup', [
            'apiKey' => $params['apiKey']
        ]);
    }

    /**
     * execute the lib call
     *
     * @param
     *            $params
     * @return array
     */
    public function libGetContactFilter($params)
    {
        return $this->executeLibCall('MailInOne::contactFilter', [
            'apiKey' => $params['apiKey']
        ]);
    }

    /**
     * execute the lib call
     *
     * @param
     *            $params
     * @return array
     */
    public function libCreateCustomField($params)
    {
        return $this->executeLibCall('MailInOne::createCustomField', [
            'maileon' => $params['maileon'],
            'apiKey' => $params['apiKey']
        ]);
    }
    /**
     * execute the lib call
     *
     * @param
     *            $params
     * @return array
     */
    public function libDeleteCustomField($params)
    {
        return $this->executeLibCall('MailInOne::deleteCustomField', [
            'maileon' => $params['maileon'],
            'apiKey' => $params['apiKey']
        ]);
    }


    /**
     * Call the given libCall
     *
     * @param
     *            $libCall
     * @param
     *            $params
     * @return array
     */
    private function executeLibCall($libCall, $params)
    {
        return $this->libraryCall->call($libCall, $params);
    }


    /**
     * execute the lib call SendLog
     * @author jsr|treaction ag
     * @version 3.0.2
     * @param array $params $params['log_data']
     * @return array
     */
    public function libSendLog($params)
    {
        return $this->executeLibCall('MailInOne::sendLog', [
            'log_data' => $params['log_data']
        ]);
    }


    /**
     * execute lib call GetCustomFields
     * @author JSR|treaction ag
     * @version 3.0.2
     * @param type $params
     * @return array of mio custom fields
     */
    public function libGetCustomFields($params)
    {
        return $this->executeLibCall('MailInOne::getCustomFields', [
            'apiKey' => $params['apiKey']
        ]);
    }
}
