<?php

namespace MailInOne\Crons;

use Plenty\Modules\Cron\Contracts\CronHandler as Cron;
use MailInOne\Services\SynchronizationService;
use MailInOne\Services\AccountService;
use MailInOne\Services\Database\SettingsService;
use MailInOne\Helper\SyncHelper;
use Plenty\Plugin\Log\Loggable;

/**
 * Class ContactSyncCron
 */
class ContactSyncCron extends Cron
{
    use Loggable;

    /**
     *
     * @var MailInOne\Helper\SyncHelper $sync_utils
     */
    private $sync_utils;

    /**
     *
     * @param SynchronizationService $synchronizationService
     * @param AccountService $accountService
     * @param SettingsService $settingsService
     */
    public function __construct(
        SynchronizationService $synchronizationService,
        AccountService $accountService,
        SettingsService $settingsService
    ){
        /*
        $this->sync_utils = new SyncUtils(
            $synchronizationService,
            $accountService,
            $settingsService
        );
         * // CHANGED TO
         */
        $this->sync_utils = pluginApp(SyncHelper::class);
    }


    /**
     * Run the Contact delta sync process.
     * author     jsr|treaction ag
     * version    V3.0.2 (modularisation, refactoring)
     */
    public function handle()
    {
        $this->getLogger('SyncController_StartContactSyncCron')
            ->info('MailInOne::plentymiosync.execLog', [ 'Starttime' => date('Y-m-d H:i:s') ]);

        try {
            $this->sync_utils->mioDeleteCustomFieldsInMioByMioCustomFields();
            $this->sync_utils->mioCreateStandardCustomField('ID'); // former: "plenty_ID", now a prefix ("plenty_") is added to each custom field
            $this->sync_utils->mioCreateStandardCustomField('lastOrderAt'); // plenty_lastOrderAt
            $this->sync_utils->mioCreateCustomFieldsFromPlentyFoldernames();
            return $this->sync_utils->mioRunDataSynchronisationDelta();
        } catch (\Exception $exc) {
            $this->getLogger('SyncController_ErrContactSyncCron')
                ->error('MailInOne::plentymiosync.errLog', [ 'tracelog' => $exc->getTraceAsString() ]);
        }
    }
}
