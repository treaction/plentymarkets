<?php

namespace MailInOne\Crons;

use Plenty\Modules\Cron\Contracts\CronHandler as Cron;
use MailInOne\Services\SynchronizationService;
use MailInOne\Services\AccountService; // Plenty
use MailInOne\Services\Database\SettingsService;
use MailInOne\Helper\SyncHelper;
use Plenty\Plugin\Log\Loggable;

/**
 * Class CreateCustomFieldCron
 * @author jsr|treaction ag
 * @version 3.0.2
 */
class CreateCustomFieldCron extends Cron {

    use Loggable;

    /**
     *
     * @var \MailInOne\Helper\SyncHelper as SyncUtils Instance
     */
    private $sync_utils;


    /**
     *
     * @param SynchronizationService $synchronizationService
     * @param AccountService $accountService
     * @param SettingsService $settingsService
     */
    public function __construct(
        SynchronizationService $synchronizationService,
        AccountService $accountService,
        SettingsService $settingsService
    ){
        $this->sync_utils = pluginApp(SyncHelper::class);
    }


    /**
     * @internal Run the Create Custom Field process.
     * @author jsr|treaction ag
     * @version 3.0.2
     */
    public function handle()
    {
        // TODO (comment by jsr): @sjd: check: Custom fields are synced by ContactSyncCron. Why here again? CreateCustomFieldCron should be removed!
        $this->getLogger('SyncController_StartCreateCustomFieldCron')
             ->info('MailInOne::plentymiosync.execLog', [ 'Starttime' => date('Y-m-d H:i:s') ]);

        try {
            $this->sync_utils->mioDeleteCustomFieldsInMioByMioCustomFields();
            $this->sync_utils->mioCreateStandardCustomField('ID'); // former: "plenty_ID", now a prefix ("plenty_") is added to each custom field
            $this->sync_utils->mioCreateStandardCustomField('lastOrderAt'); // plenty_lastOrderAt
            $this->sync_utils->mioCreateCustomFieldsFromPlentyFoldernames();
        } catch (\Exception $exc) {
             $this->getLogger('SyncController_ErrCustomFieldCron')
                ->error('MailInOne::plentymiosync.errLog', [ 'tracelog' => $exc->getTraceAsString() ]);
        }
    }
}
