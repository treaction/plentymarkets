<?php

namespace MailInOne\Procedures;


use Plenty\Modules\EventProcedures\Events\EventProceduresTriggered;
use Plenty\Modules\Order\Models\OrderRelationReference;
use Plenty\Modules\Order\Models\Legacy\Order;
use Plenty\Modules\Order\Property\Models\OrderPropertyType;
use MailInOne\Services\AccountService;
use MailInOne\Services\SynchronizationService;

use MailInOne\Services\Database\SettingsService;
/**
 * Description of OrderEventProcedure
 *
 * @author Sami Jarmoud
 */
class OrderEventProcedure {

    /**
     *
     * @var SettingsService
     */
    private $settingsService;

     /**
     *
     * @var AccountService
     */
    private $accountService;
     /**
     *
     * @var SynchronizationService
     */
    private $synchronizationService;
     /**
     * SettingsController constructor.
     *
     * @param SettingsService $settingsService
     * @param AccountService $accountService
     * @param SynchronizationService $synchronizationService
     */
    public function __construct(SettingsService $settingsService, AccountService $accountService, SynchronizationService $synchronizationService) {
        $this->settingsService = $settingsService;
        $this->accountService = $accountService;
        $this->synchronizationService = $synchronizationService;
    }

    /**
     * @param EventProceduresTriggered $event
     * @return mixed
     */
    public function newOrder(EventProceduresTriggered $event)
    {
        // getEvent
        $mioEvents ='';
        $mioEventsArray = $this->settingsService->getEventByTyp('MIO-Neuer Auftrag');
        if(count($mioEventsArray) > 0){
           $mioEvents = $mioEventsArray;
        }else{
           $mioEventsArray = $this->settingsService->getEventByTyp('MIO New Order');
           $mioEvents = $mioEventsArray;
        }
        if(!empty($mioEvents) && count($mioEvents) > 0){
        /** @var Order $order */
        $order = $event->getOrder();
         $contactId = -1;
        $orderItems = $order->orderItems;
        foreach ($orderItems as $orderItem) {
            if($orderItem->typeId == 1){
                $id = $orderItem->id;
                $orderItemName = $orderItem->orderItemName;
                $quantity = $orderItem->quantity;
                $amounts = $orderItem->amounts;
                foreach ($amounts as $amount) {
                    $currency = $amount->currency;
                    $priceGross = $amount->priceGross;
                }
                $item[$id] = array(
                    'id' => $id,
                    'orderItemName' => $orderItemName,
                    'quantity' => $quantity,
                    'currency' => $currency,
                    'priceGross' => $priceGross
                );
            }
        }
        $result['item'] = $item;
        $amountsSummen = $order->amounts;
        foreach ($amountsSummen as $amountsSumme) {
            $result['netTotal'] = $amountsSumme->netTotal;
            $result['grossTotal'] = $amountsSumme->grossTotal;
        }
        /** @var OrderRelationReference[] $r */
        $relations = $order->relations;
        /** @var OrderRelationReference $a */
        foreach($relations as $relation)
        {
            if($relation->relation == 'receiver')
            {
                $contactId = $relation->referenceId;
                $contact = $this->accountService->findContactById($contactId);
                $result['firstName'] = $contact['firstName'];
                $result['lastName'] = $contact['lastName'];
                $result['email'] = $contact['email'];
                $result['gender'] = $contact['gender'];
                $result['contactId'] = $contactId;
                #$result['birthday'] = $contact['birthdayAt'];
                $result['orderId'] = $relation->orderId;
                foreach ($mioEvents as $mioEvent) {
                    $integration = $mioEvent->integration;
                    switch ($integration) {
                        case 'Kontaktereignis':
                            if(!empty($mioEvent->kontaktereignisID)){
                                $Kontaktereignis['apiKey'] = $this->settingsService->getSettings();
                                $Kontaktereignis['kontaktereignisID'] = (int)$mioEvent->kontaktereignisID;
                                $Kontaktereignis['maileon'] = $result;
                                $resultNewOrder['Kontaktereignis'] = $this->synchronizationService->setTransactionEvent($Kontaktereignis);
                            }
                            break;
                         case 'Marketing Program':
                            if(!empty($mioEvent->marketingAutomation)){
                                $marketingAutomation['apiKey'] = $this->settingsService->getSettings();
                                $marketingAutomation['programId'] = (int)$mioEvent->marketingAutomation;
                                $marketingAutomation['email'] = $result['email'];
                                $resultNewOrder['marketingAutomation'] = $this->synchronizationService->setMarketingAutomation($marketingAutomation);
                            }
                            break;

                        default:
                            break;
                    }
                }
            }
        }
        return json_encode($resultNewOrder);
        }
    }


   /**
     * @param EventProceduresTriggered $event
     * @return mixed
     */
    public function dataMissing(EventProceduresTriggered $event)
    {
          // getEvent
        $mioEvents ='';
        $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[1] Unvollständige Daten');

        if(count($mioEventsArray) > 0){
           $mioEvents = $mioEventsArray;
        }else{
           $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[1] Data missing');
           $mioEvents = $mioEventsArray;
        }
        if(!empty($mioEvents) && count($mioEvents) > 0){
        /** @var Order $order */
        $order = $event->getOrder();
         $contactId = -1;
         $orderItems = $order->orderItems;
        foreach ($orderItems as $orderItem) {
            if($orderItem->typeId == 1){
                $id = $orderItem->id;
                $orderItemName = $orderItem->orderItemName;
                $quantity = $orderItem->quantity;
                $amounts = $orderItem->amounts;
                foreach ($amounts as $amount) {
                    $currency = $amount->currency;
                    $priceGross = $amount->priceGross;
                }
                $item[$id] = array(
                    'id' => $id,
                    'orderItemName' => $orderItemName,
                    'quantity' => $quantity,
                    'currency' => $currency,
                    'priceGross' => $priceGross
                );
            }
        }
        $result['item'] = $item;
        $amountsSummen = $order->amounts;
        foreach ($amountsSummen as $amountsSumme) {
            $result['netTotal'] = $amountsSumme->netTotal;
            $result['grossTotal'] = $amountsSumme->grossTotal;
        }
        /** @var OrderRelationReference[] $r */
        $relations = $order->relations;
        /** @var OrderRelationReference $a */
        foreach($relations as $relation)
        {
            if($relation->relation == 'receiver')
            {
                $contactId = $relation->referenceId;
                $contact = $this->accountService->findContactById($contactId);
                $result['firstName'] = $contact['firstName'];
                $result['lastName'] = $contact['lastName'];
                $result['email'] = $contact['email'];
                $result['gender'] = $contact['gender'];
                $result['contactId'] = $contactId;
                #$result['birthday'] = $contact['birthdayAt'];
                $result['orderId'] = $relation->orderId;
            }
            if(isset($result) && !empty($result)){
                foreach ($mioEvents as $mioEvent) {
                    $integration = $mioEvent->integration;
                    switch ($integration) {
                        case 'Kontaktereignis':
                            if(!empty($mioEvent->kontaktereignisID)){
                                $Kontaktereignis['apiKey'] = $this->settingsService->getSettings();
                                $Kontaktereignis['kontaktereignisID'] = (int)$mioEvent->kontaktereignisID;
                                $Kontaktereignis['maileon'] = $result;
                                $resultNewOrder['Kontaktereignis'] = $this->synchronizationService->setTransactionEvent($Kontaktereignis);
                            }
                            break;
                         case 'Marketing Program':
                            if(!empty($mioEvent->marketingAutomation)){
                                $marketingAutomation['apiKey'] = $this->settingsService->getSettings();
                                $marketingAutomation['programId'] = (int)$mioEvent->marketingAutomation;
                                $marketingAutomation['email'] = $result['email'];
                                $resultNewOrder['marketingAutomation'] = $this->synchronizationService->setMarketingAutomation($marketingAutomation);
                            }
                            break;

                        default:
                            break;
                    }
                }
            }
        }
        return json_encode($resultNewOrder);
      }
    }



     /**
     * @param EventProceduresTriggered $event
     * @return mixed
     */
    public function waitingForPaymentAndActivation(EventProceduresTriggered $event)
    {
          // getEvent
        $mioEvents ='';
        $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[1.1] Warten auf Zahlung & Freischaltung');
        if(count($mioEventsArray) > 0){
           $mioEvents = $mioEventsArray;
        }else{
           $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[1.1] Waiting for payment & activation');
           $mioEvents = $mioEventsArray;
        }
        if(!empty($mioEvents) && count($mioEvents) > 0){
        /** @var Order $order */
        $order = $event->getOrder();
         $contactId = -1;
         $orderItems = $order->orderItems;
        foreach ($orderItems as $orderItem) {
            if($orderItem->typeId == 1){
                $id = $orderItem->id;
                $orderItemName = $orderItem->orderItemName;
                $quantity = $orderItem->quantity;
                $amounts = $orderItem->amounts;
                foreach ($amounts as $amount) {
                    $currency = $amount->currency;
                    $priceGross = $amount->priceGross;
                }
                $item[$id] = array(
                    'id' => $id,
                    'orderItemName' => $orderItemName,
                    'quantity' => $quantity,
                    'currency' => $currency,
                    'priceGross' => $priceGross
                );
            }
        }
        $result['item'] = $item;
        $amountsSummen = $order->amounts;
        foreach ($amountsSummen as $amountsSumme) {
            $result['netTotal'] = $amountsSumme->netTotal;
            $result['grossTotal'] = $amountsSumme->grossTotal;
        }
        /** @var OrderRelationReference[] $r */
        $relations = $order->relations;
        /** @var OrderRelationReference $a */
        foreach($relations as $relation)
        {
            if($relation->relation == 'receiver')
            {
                $contactId = $relation->referenceId;
                $contact = $this->accountService->findContactById($contactId);
                $result['firstName'] = $contact['firstName'];
                $result['lastName'] = $contact['lastName'];
                $result['email'] = $contact['email'];
                $result['gender'] = $contact['gender'];
                $result['contactId'] = $contactId;
                #$result['birthday'] = $contact['birthdayAt'];
                $result['orderId'] = $relation->orderId;
            }
            if(isset($result) && !empty($result)){
                foreach ($mioEvents as $mioEvent) {
                    $integration = $mioEvent->integration;
                    switch ($integration) {
                        case 'Kontaktereignis':
                            if(!empty($mioEvent->kontaktereignisID)){
                                $Kontaktereignis['apiKey'] = $this->settingsService->getSettings();
                                $Kontaktereignis['kontaktereignisID'] = (int)$mioEvent->kontaktereignisID;
                                $Kontaktereignis['maileon'] = $result;
                                $resultNewOrder['Kontaktereignis'] = $this->synchronizationService->setTransactionEvent($Kontaktereignis);
                            }
                            break;
                         case 'Marketing Program':
                            if(!empty($mioEvent->marketingAutomation)){
                                $marketingAutomation['apiKey'] = $this->settingsService->getSettings();
                                $marketingAutomation['programId'] = (int)$mioEvent->marketingAutomation;
                                $marketingAutomation['email'] = $result['email'];
                                $resultNewOrder['marketingAutomation'] = $this->synchronizationService->setMarketingAutomation($marketingAutomation);
                            }
                            break;

                        default:
                            break;
                    }
                }
            }
        }
        return json_encode($resultNewOrder);
        }
    }


   /**
     * @param EventProceduresTriggered $event
     * @return mixed
     */
    public function activatedWaitingForPayment(EventProceduresTriggered $event)
    {
           // getEvent
        $mioEvents ='';
        $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[1.2] Freigeschaltet, warten auf Zahlung');
        if(count($mioEventsArray) > 0){
           $mioEvents = $mioEventsArray;
        }else{
           $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[1.2] Activated, waiting for payment');
           $mioEvents = $mioEventsArray;
        }
        if(!empty($mioEvents) && count($mioEvents) > 0){
        /** @var Order $order */
        $order = $event->getOrder();
         $contactId = -1;
         $orderItems = $order->orderItems;
        foreach ($orderItems as $orderItem) {
            if($orderItem->typeId == 1){
                $id = $orderItem->id;
                $orderItemName = $orderItem->orderItemName;
                $quantity = $orderItem->quantity;
                $amounts = $orderItem->amounts;
                foreach ($amounts as $amount) {
                    $currency = $amount->currency;
                    $priceGross = $amount->priceGross;
                }
                $item[$id] = array(
                    'id' => $id,
                    'orderItemName' => $orderItemName,
                    'quantity' => $quantity,
                    'currency' => $currency,
                    'priceGross' => $priceGross
                );
            }
        }
        $result['item'] = $item;
        $amountsSummen = $order->amounts;
        foreach ($amountsSummen as $amountsSumme) {
            $result['netTotal'] = $amountsSumme->netTotal;
            $result['grossTotal'] = $amountsSumme->grossTotal;
        }
        /** @var OrderRelationReference[] $r */
        $relations = $order->relations;
        /** @var OrderRelationReference $a */
        foreach($relations as $relation)
        {
            if($relation->relation == 'receiver')
            {
                $contactId = $relation->referenceId;
                $contact = $this->accountService->findContactById($contactId);
                $result['firstName'] = $contact['firstName'];
                $result['lastName'] = $contact['lastName'];
                $result['email'] = $contact['email'];
                $result['gender'] = $contact['gender'];
                $result['contactId'] = $contactId;
                #$result['birthday'] = $contact['birthdayAt'];
                $result['orderId'] = $relation->orderId;
            }
            if(isset($result) && !empty($result)){
                foreach ($mioEvents as $mioEvent) {
                    $integration = $mioEvent->integration;
                    switch ($integration) {
                        case 'Kontaktereignis':
                            if(!empty($mioEvent->kontaktereignisID)){
                                $Kontaktereignis['apiKey'] = $this->settingsService->getSettings();
                                $Kontaktereignis['kontaktereignisID'] = (int)$mioEvent->kontaktereignisID;
                                $Kontaktereignis['maileon'] = $result;
                                $resultNewOrder['Kontaktereignis'] = $this->synchronizationService->setTransactionEvent($Kontaktereignis);
                            }
                            break;
                         case 'Marketing Program':
                            if(!empty($mioEvent->marketingAutomation)){
                                $marketingAutomation['apiKey'] = $this->settingsService->getSettings();
                                $marketingAutomation['programId'] = (int)$mioEvent->marketingAutomation;
                                $marketingAutomation['email'] = $result['email'];
                                $resultNewOrder['marketingAutomation'] = $this->synchronizationService->setMarketingAutomation($marketingAutomation);
                            }
                            break;

                        default:
                            break;
                    }
                }
            }
        }
        return json_encode($resultNewOrder);
        }
    }


       /**
     * @param EventProceduresTriggered $event
     * @return mixed
     */
    public function waitingForActivation(EventProceduresTriggered $event)
    {
           // getEvent
        $mioEvents ='';
        $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[2] Warten auf Freischaltung');
        if(count($mioEventsArray) > 0){
           $mioEvents = $mioEventsArray;
        }else{
           $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[2] Waiting for activation');
           $mioEvents = $mioEventsArray;
        }
        if(!empty($mioEvents) && count($mioEvents) > 0){
        /** @var Order $order */
        $order = $event->getOrder();
         $contactId = -1;
         $orderItems = $order->orderItems;
        foreach ($orderItems as $orderItem) {
            if($orderItem->typeId == 1){
                $id = $orderItem->id;
                $orderItemName = $orderItem->orderItemName;
                $quantity = $orderItem->quantity;
                $amounts = $orderItem->amounts;
                foreach ($amounts as $amount) {
                    $currency = $amount->currency;
                    $priceGross = $amount->priceGross;
                }
                $item[$id] = array(
                    'id' => $id,
                    'orderItemName' => $orderItemName,
                    'quantity' => $quantity,
                    'currency' => $currency,
                    'priceGross' => $priceGross
                );
            }
        }
        $result['item'] = $item;
        $amountsSummen = $order->amounts;
        foreach ($amountsSummen as $amountsSumme) {
            $result['netTotal'] = $amountsSumme->netTotal;
            $result['grossTotal'] = $amountsSumme->grossTotal;
        }
        /** @var OrderRelationReference[] $r */
        $relations = $order->relations;
        /** @var OrderRelationReference $a */
        foreach($relations as $relation)
        {
            if($relation->relation == 'receiver')
            {
                $contactId = $relation->referenceId;
                $contact = $this->accountService->findContactById($contactId);
                $result['firstName'] = $contact['firstName'];
                $result['lastName'] = $contact['lastName'];
                $result['email'] = $contact['email'];
                $result['gender'] = $contact['gender'];
                $result['contactId'] = $contactId;
                #$result['birthday'] = $contact['birthdayAt'];
                $result['orderId'] = $relation->orderId;
            }
            if(isset($result) && !empty($result)){
                foreach ($mioEvents as $mioEvent) {
                    $integration = $mioEvent->integration;
                    switch ($integration) {
                        case 'Kontaktereignis':
                            if(!empty($mioEvent->kontaktereignisID)){
                                $Kontaktereignis['apiKey'] = $this->settingsService->getSettings();
                                $Kontaktereignis['kontaktereignisID'] = (int)$mioEvent->kontaktereignisID;
                                $Kontaktereignis['maileon'] = $result;
                                $resultNewOrder['Kontaktereignis'] = $this->synchronizationService->setTransactionEvent($Kontaktereignis);
                            }
                            break;
                         case 'Marketing Program':
                            if(!empty($mioEvent->marketingAutomation)){
                                $marketingAutomation['apiKey'] = $this->settingsService->getSettings();
                                $marketingAutomation['programId'] = (int)$mioEvent->marketingAutomation;
                                $marketingAutomation['email'] = $result['email'];
                                $resultNewOrder['marketingAutomation'] = $this->synchronizationService->setMarketingAutomation($marketingAutomation);
                            }
                            break;

                        default:
                            break;
                    }
                }
            }
        }
        return json_encode($resultNewOrder);
        }
    }


   /**
     * @param EventProceduresTriggered $event
     * @return mixed
     */
    public function waitingForPayment(EventProceduresTriggered $event)
    {
           // getEvent
        $mioEvents ='';
        $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[3] Warten auf Zahlung');
        if(count($mioEventsArray) > 0){
           $mioEvents = $mioEventsArray;
        }else{
           $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[3] Waiting for payment');
           $mioEvents = $mioEventsArray;
        }
        if(!empty($mioEvents) && count($mioEvents) > 0){
        /** @var Order $order */
        $order = $event->getOrder();
         $contactId = -1;
         $orderItems = $order->orderItems;
        foreach ($orderItems as $orderItem) {
            if($orderItem->typeId == 1){
                $id = $orderItem->id;
                $orderItemName = $orderItem->orderItemName;
                $quantity = $orderItem->quantity;
                $amounts = $orderItem->amounts;
                foreach ($amounts as $amount) {
                    $currency = $amount->currency;
                    $priceGross = $amount->priceGross;
                }
                $item[$id] = array(
                    'id' => $id,
                    'orderItemName' => $orderItemName,
                    'quantity' => $quantity,
                    'currency' => $currency,
                    'priceGross' => $priceGross
                );
            }
        }
        $result['item'] = $item;
        $amountsSummen = $order->amounts;
        foreach ($amountsSummen as $amountsSumme) {
            $result['netTotal'] = $amountsSumme->netTotal;
            $result['grossTotal'] = $amountsSumme->grossTotal;
        }
        /** @var OrderRelationReference[] $r */
        $relations = $order->relations;
        /** @var OrderRelationReference $a */
        foreach($relations as $relation)
        {
            if($relation->relation == 'receiver')
            {
                $contactId = $relation->referenceId;
                $contact = $this->accountService->findContactById($contactId);
                $result['firstName'] = $contact['firstName'];
                $result['lastName'] = $contact['lastName'];
                $result['email'] = $contact['email'];
                $result['gender'] = $contact['gender'];
                $result['contactId'] = $contactId;
                #$result['birthday'] = $contact['birthdayAt'];
                $result['orderId'] = $relation->orderId;
            }
            if(isset($result) && !empty($result)){
                foreach ($mioEvents as $mioEvent) {
                    $integration = $mioEvent->integration;
                    switch ($integration) {
                        case 'Kontaktereignis':
                            if(!empty($mioEvent->kontaktereignisID)){
                                $Kontaktereignis['apiKey'] = $this->settingsService->getSettings();
                                $Kontaktereignis['kontaktereignisID'] = (int)$mioEvent->kontaktereignisID;
                                $Kontaktereignis['maileon'] = $result;
                                $resultNewOrder['Kontaktereignis'] = $this->synchronizationService->setTransactionEvent($Kontaktereignis);

                            }
                            break;
                         case 'Marketing Program':
                            if(!empty($mioEvent->marketingAutomation)){
                                $marketingAutomation['apiKey'] = $this->settingsService->getSettings();
                                $marketingAutomation['programId'] = (int)$mioEvent->marketingAutomation;
                                $marketingAutomation['email'] = $result['email'];
                                $resultNewOrder['marketingAutomation'] = $this->synchronizationService->setMarketingAutomation($marketingAutomation);
                            }
                            break;

                        default:
                            break;
                    }
                }
            }
        }
        return json_encode($resultNewOrder);
        }
    }


       /**
     * @param EventProceduresTriggered $event
     * @return mixed
     */
    public function startPayPalPaymentprocess(EventProceduresTriggered $event)
    {
           // getEvent
        $mioEvents ='';
        $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[3.1] Start PayPal-Zahlungsprozess');
        if(count($mioEventsArray) > 0){
           $mioEvents = $mioEventsArray;
        }else{
           $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[3.1] Start PayPal-paymentprocess');
           $mioEvents = $mioEventsArray;
        }
        if(!empty($mioEvents) && count($mioEvents) > 0){
        /** @var Order $order */
        $order = $event->getOrder();
         $contactId = -1;
         $orderItems = $order->orderItems;
        foreach ($orderItems as $orderItem) {
            if($orderItem->typeId == 1){
                $id = $orderItem->id;
                $orderItemName = $orderItem->orderItemName;
                $quantity = $orderItem->quantity;
                $amounts = $orderItem->amounts;
                foreach ($amounts as $amount) {
                    $currency = $amount->currency;
                    $priceGross = $amount->priceGross;
                }
                $item[$id] = array(
                    'id' => $id,
                    'orderItemName' => $orderItemName,
                    'quantity' => $quantity,
                    'currency' => $currency,
                    'priceGross' => $priceGross
                );
            }
        }
        $result['item'] = $item;
        $amountsSummen = $order->amounts;
        foreach ($amountsSummen as $amountsSumme) {
            $result['netTotal'] = $amountsSumme->netTotal;
            $result['grossTotal'] = $amountsSumme->grossTotal;
        }
        /** @var OrderRelationReference[] $r */
        $relations = $order->relations;
        /** @var OrderRelationReference $a */
        foreach($relations as $relation)
        {
            if($relation->relation == 'receiver')
            {
                $contactId = $relation->referenceId;
                $contact = $this->accountService->findContactById($contactId);
                $result['firstName'] = $contact['firstName'];
                $result['lastName'] = $contact['lastName'];
                $result['email'] = $contact['email'];
                $result['gender'] = $contact['gender'];
                $result['contactId'] = $contactId;
                #$result['birthday'] = $contact['birthdayAt'];
                $result['orderId'] = $relation->orderId;
            }
            if(isset($result) && !empty($result)){
                foreach ($mioEvents as $mioEvent) {
                    $integration = $mioEvent->integration;
                    switch ($integration) {
                        case 'Kontaktereignis':
                            if(!empty($mioEvent->kontaktereignisID)){
                                $Kontaktereignis['apiKey'] = $this->settingsService->getSettings();
                                $Kontaktereignis['kontaktereignisID'] = (int)$mioEvent->kontaktereignisID;
                                $Kontaktereignis['maileon'] = $result;
                                $resultNewOrder['Kontaktereignis'] = $this->synchronizationService->setTransactionEvent($Kontaktereignis);
                            }
                            break;
                         case 'Marketing Program':
                            if(!empty($mioEvent->marketingAutomation)){
                                $marketingAutomation['apiKey'] = $this->settingsService->getSettings();
                                $marketingAutomation['programId'] = (int)$mioEvent->marketingAutomation;
                                $marketingAutomation['email'] = $result['email'];
                                $resultNewOrder['marketingAutomation'] = $this->synchronizationService->setMarketingAutomation($marketingAutomation);
                            }
                            break;

                        default:
                            break;
                    }
                }
            }
        }
        return json_encode($resultNewOrder);
        }
    }


   /**
     * @param EventProceduresTriggered $event
     * @return mixed
     */
    public function inWaitingPosition(EventProceduresTriggered $event)
    {
           // getEvent
        $mioEvents ='';
        $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[3.2] In Warteposition');
       if(count($mioEventsArray) > 0){
           $mioEvents = $mioEventsArray;
        }else{
           $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[3.2] In waiting position');
           $mioEvents = $mioEventsArray;
        }
        if(!empty($mioEvents) && count($mioEvents) > 0){
        /** @var Order $order */
        $order = $event->getOrder();
         $contactId = -1;
         $orderItems = $order->orderItems;
        foreach ($orderItems as $orderItem) {
            if($orderItem->typeId == 1){
                $id = $orderItem->id;
                $orderItemName = $orderItem->orderItemName;
                $quantity = $orderItem->quantity;
                $amounts = $orderItem->amounts;
                foreach ($amounts as $amount) {
                    $currency = $amount->currency;
                    $priceGross = $amount->priceGross;
                }
                $item[$id] = array(
                    'id' => $id,
                    'orderItemName' => $orderItemName,
                    'quantity' => $quantity,
                    'currency' => $currency,
                    'priceGross' => $priceGross
                );
            }
        }
        $result['item'] = $item;
        $amountsSummen = $order->amounts;
        foreach ($amountsSummen as $amountsSumme) {
            $result['netTotal'] = $amountsSumme->netTotal;
            $result['grossTotal'] = $amountsSumme->grossTotal;
        }
        /** @var OrderRelationReference[] $r */
        $relations = $order->relations;
        /** @var OrderRelationReference $a */
        foreach($relations as $relation)
        {
            if($relation->relation == 'receiver')
            {
                $contactId = $relation->referenceId;
                $contact = $this->accountService->findContactById($contactId);
                $result['firstName'] = $contact['firstName'];
                $result['lastName'] = $contact['lastName'];
                $result['email'] = $contact['email'];
                $result['gender'] = $contact['gender'];
                $result['contactId'] = $contactId;
                #$result['birthday'] = $contact['birthdayAt'];
                $result['orderId'] = $relation->orderId;
            }
            if(isset($result) && !empty($result)){
                foreach ($mioEvents as $mioEvent) {
                    $integration = $mioEvent->integration;
                    switch ($integration) {
                        case 'Kontaktereignis':
                            if(!empty($mioEvent->kontaktereignisID)){
                                $Kontaktereignis['apiKey'] = $this->settingsService->getSettings();
                                $Kontaktereignis['kontaktereignisID'] = (int)$mioEvent->kontaktereignisID;
                                $Kontaktereignis['maileon'] = $result;
                                $resultNewOrder['Kontaktereignis'] = $this->synchronizationService->setTransactionEvent($Kontaktereignis);
                            }
                            break;
                         case 'Marketing Program':
                            if(!empty($mioEvent->marketingAutomation)){
                                $marketingAutomation['apiKey'] = $this->settingsService->getSettings();
                                $marketingAutomation['programId'] = (int)$mioEvent->marketingAutomation;
                                $marketingAutomation['email'] = $result['email'];
                                $resultNewOrder['marketingAutomation'] = $this->synchronizationService->setMarketingAutomation($marketingAutomation);
                            }
                            break;

                        default:
                            break;
                    }
                }
            }
        }
        return json_encode($resultNewOrder);
        }
    }


       /**
     * @param EventProceduresTriggered $event
     * @return mixed
     */
    public function readyForShipmentWaitingForPayment(EventProceduresTriggered $event)
    {
           // getEvent
        $mioEvents ='';
        $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[3.3] Versandfertig; warten auf Zahlung');
        if(count($mioEventsArray) > 0){
           $mioEvents = $mioEventsArray;
        }else{
           $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[3.3] Ready for shipment; waiting for payment');
           $mioEvents = $mioEventsArray;
        }
        if(!empty($mioEvents) && count($mioEvents) > 0){
        /** @var Order $order */
        $order = $event->getOrder();
         $contactId = -1;
         $orderItems = $order->orderItems;
        foreach ($orderItems as $orderItem) {
            if($orderItem->typeId == 1){
                $id = $orderItem->id;
                $orderItemName = $orderItem->orderItemName;
                $quantity = $orderItem->quantity;
                $amounts = $orderItem->amounts;
                foreach ($amounts as $amount) {
                    $currency = $amount->currency;
                    $priceGross = $amount->priceGross;
                }
                $item[$id] = array(
                    'id' => $id,
                    'orderItemName' => $orderItemName,
                    'quantity' => $quantity,
                    'currency' => $currency,
                    'priceGross' => $priceGross
                );
            }
        }
        $result['item'] = $item;
        $amountsSummen = $order->amounts;
        foreach ($amountsSummen as $amountsSumme) {
            $result['netTotal'] = $amountsSumme->netTotal;
            $result['grossTotal'] = $amountsSumme->grossTotal;
        }
        /** @var OrderRelationReference[] $r */
        $relations = $order->relations;
        /** @var OrderRelationReference $a */
        foreach($relations as $relation)
        {
            if($relation->relation == 'receiver')
            {
                $contactId = $relation->referenceId;
                $contact = $this->accountService->findContactById($contactId);
                $result['firstName'] = $contact['firstName'];
                $result['lastName'] = $contact['lastName'];
                $result['email'] = $contact['email'];
                $result['gender'] = $contact['gender'];
                $result['contactId'] = $contactId;
                #$result['birthday'] = $contact['birthdayAt'];
                $result['orderId'] = $relation->orderId;
            }
            if(isset($result) && !empty($result)){
                foreach ($mioEvents as $mioEvent) {
                    $integration = $mioEvent->integration;
                    switch ($integration) {
                        case 'Kontaktereignis':
                            if(!empty($mioEvent->kontaktereignisID)){
                                $Kontaktereignis['apiKey'] = $this->settingsService->getSettings();
                                $Kontaktereignis['kontaktereignisID'] = (int)$mioEvent->kontaktereignisID;
                                $Kontaktereignis['maileon'] = $result;
                                $resultNewOrder['Kontaktereignis'] = $this->synchronizationService->setTransactionEvent($Kontaktereignis);
                            }
                            break;
                         case 'Marketing Program':
                            if(!empty($mioEvent->marketingAutomation)){
                                $marketingAutomation['apiKey'] = $this->settingsService->getSettings();
                                $marketingAutomation['programId'] = (int)$mioEvent->marketingAutomation;
                                $marketingAutomation['email'] = $result['email'];
                                $resultNewOrder['marketingAutomation'] = $this->synchronizationService->setMarketingAutomation($marketingAutomation);
                            }
                            break;

                        default:
                            break;
                    }
                }
            }
        }
        return json_encode($resultNewOrder);
        }
    }


   /**
     * @param EventProceduresTriggered $event
     * @return mixed
     */
    public function dunningLetterSent(EventProceduresTriggered $event)
    {
          // getEvent
        $mioEvents ='';
        $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[3.4] Mahnung versendet');
        if(count($mioEventsArray) > 0){
           $mioEvents = $mioEventsArray;
        }else{
           $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[3.4] Dunning letter sent');
           $mioEvents = $mioEventsArray;
        }
        if(!empty($mioEvents) && count($mioEvents) > 0){
        /** @var Order $order */
        $order = $event->getOrder();
         $contactId = -1;
         $orderItems = $order->orderItems;
        foreach ($orderItems as $orderItem) {
            if($orderItem->typeId == 1){
                $id = $orderItem->id;
                $orderItemName = $orderItem->orderItemName;
                $quantity = $orderItem->quantity;
                $amounts = $orderItem->amounts;
                foreach ($amounts as $amount) {
                    $currency = $amount->currency;
                    $priceGross = $amount->priceGross;
                }
                $item[$id] = array(
                    'id' => $id,
                    'orderItemName' => $orderItemName,
                    'quantity' => $quantity,
                    'currency' => $currency,
                    'priceGross' => $priceGross
                );
            }
        }
        $result['item'] = $item;
        $amountsSummen = $order->amounts;
        foreach ($amountsSummen as $amountsSumme) {
            $result['netTotal'] = $amountsSumme->netTotal;
            $result['grossTotal'] = $amountsSumme->grossTotal;
        }
        /** @var OrderRelationReference[] $r */
        $relations = $order->relations;
        /** @var OrderRelationReference $a */
        foreach($relations as $relation)
        {
            if($relation->relation == 'receiver')
            {
                $contactId = $relation->referenceId;
                $contact = $this->accountService->findContactById($contactId);
                $result['firstName'] = $contact['firstName'];
                $result['lastName'] = $contact['lastName'];
                $result['email'] = $contact['email'];
                $result['gender'] = $contact['gender'];
                $result['contactId'] = $contactId;
                #$result['birthday'] = $contact['birthdayAt'];
                $result['orderId'] = $relation->orderId;
            }
            if(isset($result) && !empty($result)){
                foreach ($mioEvents as $mioEvent) {
                    $integration = $mioEvent->integration;
                    switch ($integration) {
                        case 'Kontaktereignis':
                            if(!empty($mioEvent->kontaktereignisID)){
                                $Kontaktereignis['apiKey'] = $this->settingsService->getSettings();
                                $Kontaktereignis['kontaktereignisID'] = (int)$mioEvent->kontaktereignisID;
                                $Kontaktereignis['maileon'] = $result;
                                $resultNewOrder['Kontaktereignis'] = $this->synchronizationService->setTransactionEvent($Kontaktereignis);
                            }
                            break;
                         case 'Marketing Program':
                            if(!empty($mioEvent->marketingAutomation)){
                                $marketingAutomation['apiKey'] = $this->settingsService->getSettings();
                                $marketingAutomation['programId'] = (int)$mioEvent->marketingAutomation;
                                $marketingAutomation['email'] = $result['email'];
                                $resultNewOrder['marketingAutomation'] = $this->synchronizationService->setMarketingAutomation($marketingAutomation);
                            }
                            break;

                        default:
                            break;
                    }
                }
            }
        }
        return json_encode($resultNewOrder);
        }
    }


       /**
     * @param EventProceduresTriggered $event
     * @return mixed
     */
    public function preparingForShipment(EventProceduresTriggered $event)
    {
           // getEvent
        $mioEvents ='';
        $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[4] In Versandvorbereitung');
        if(count($mioEventsArray) > 0){
           $mioEvents = $mioEventsArray;
        }else{
           $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[4] Preparing for Shipment');
           $mioEvents = $mioEventsArray;
        }
        if(!empty($mioEvents) && count($mioEvents) > 0){
        /** @var Order $order */
        $order = $event->getOrder();
         $contactId = -1;
         $orderItems = $order->orderItems;
        foreach ($orderItems as $orderItem) {
            if($orderItem->typeId == 1){
                $id = $orderItem->id;
                $orderItemName = $orderItem->orderItemName;
                $quantity = $orderItem->quantity;
                $amounts = $orderItem->amounts;
                foreach ($amounts as $amount) {
                    $currency = $amount->currency;
                    $priceGross = $amount->priceGross;
                }
                $item[$id] = array(
                    'id' => $id,
                    'orderItemName' => $orderItemName,
                    'quantity' => $quantity,
                    'currency' => $currency,
                    'priceGross' => $priceGross
                );
            }
        }
        $result['item'] = $item;
        $amountsSummen = $order->amounts;
        foreach ($amountsSummen as $amountsSumme) {
            $result['netTotal'] = $amountsSumme->netTotal;
            $result['grossTotal'] = $amountsSumme->grossTotal;
        }
        /** @var OrderRelationReference[] $r */
        $relations = $order->relations;
        /** @var OrderRelationReference $a */
        foreach($relations as $relation)
        {
            if($relation->relation == 'receiver')
            {
                $contactId = $relation->referenceId;
                $contact = $this->accountService->findContactById($contactId);
                $result['firstName'] = $contact['firstName'];
                $result['lastName'] = $contact['lastName'];
                $result['email'] = $contact['email'];
                $result['gender'] = $contact['gender'];
                $result['contactId'] = $contactId;
                #$result['birthday'] = $contact['birthdayAt'];
                $result['orderId'] = $relation->orderId;
            }
            if(isset($result) && !empty($result)){
                foreach ($mioEvents as $mioEvent) {
                    $integration = $mioEvent->integration;
                    switch ($integration) {
                        case 'Kontaktereignis':
                            if(!empty($mioEvent->kontaktereignisID)){
                                $Kontaktereignis['apiKey'] = $this->settingsService->getSettings();
                                $Kontaktereignis['kontaktereignisID'] = (int)$mioEvent->kontaktereignisID;
                                $Kontaktereignis['maileon'] = $result;
                                $resultNewOrder['Kontaktereignis'] = $this->synchronizationService->setTransactionEvent($Kontaktereignis);
                            }
                            break;
                         case 'Marketing Program':
                            if(!empty($mioEvent->marketingAutomation)){
                                $marketingAutomation['apiKey'] = $this->settingsService->getSettings();
                                $marketingAutomation['programId'] = (int)$mioEvent->marketingAutomation;
                                $marketingAutomation['email'] = $result['email'];
                                $resultNewOrder['marketingAutomation'] = $this->synchronizationService->setMarketingAutomation($marketingAutomation);
                            }
                            break;

                        default:
                            break;
                    }
                }
            }
        }
        return json_encode($resultNewOrder);
        }
    }


   /**
     * @param EventProceduresTriggered $event
     * @return mixed
     */
    public function shipmentProcessStarted(EventProceduresTriggered $event)
    {
           // getEvent
        $mioEvents ='';
        $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[5] Freigabe Versand');
        if(count($mioEventsArray) > 0){
           $mioEvents = $mioEventsArray;
        }else{
           $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[5] Shipment process started');
           $mioEvents = $mioEventsArray;
        }
        if(!empty($mioEvents) && count($mioEvents) > 0){
        /** @var Order $order */
        $order = $event->getOrder();
         $contactId = -1;
         $orderItems = $order->orderItems;
        foreach ($orderItems as $orderItem) {
            if($orderItem->typeId == 1){
                $id = $orderItem->id;
                $orderItemName = $orderItem->orderItemName;
                $quantity = $orderItem->quantity;
                $amounts = $orderItem->amounts;
                foreach ($amounts as $amount) {
                    $currency = $amount->currency;
                    $priceGross = $amount->priceGross;
                }
                $item[$id] = array(
                    'id' => $id,
                    'orderItemName' => $orderItemName,
                    'quantity' => $quantity,
                    'currency' => $currency,
                    'priceGross' => $priceGross
                );
            }
        }
        $result['item'] = $item;
        $amountsSummen = $order->amounts;
        foreach ($amountsSummen as $amountsSumme) {
            $result['netTotal'] = $amountsSumme->netTotal;
            $result['grossTotal'] = $amountsSumme->grossTotal;
        }
        /** @var OrderRelationReference[] $r */
        $relations = $order->relations;
        /** @var OrderRelationReference $a */
        foreach($relations as $relation)
        {
            if($relation->relation == 'receiver')
            {
                $contactId = $relation->referenceId;
                $contact = $this->accountService->findContactById($contactId);
                $result['firstName'] = $contact['firstName'];
                $result['lastName'] = $contact['lastName'];
                $result['email'] = $contact['email'];
                $result['gender'] = $contact['gender'];
                $result['contactId'] = $contactId;
                #$result['birthday'] = $contact['birthdayAt'];
                $result['orderId'] = $relation->orderId;
            }
            if(isset($result) && !empty($result)){
                foreach ($mioEvents as $mioEvent) {
                    $integration = $mioEvent->integration;
                    switch ($integration) {
                        case 'Kontaktereignis':
                            if(!empty($mioEvent->kontaktereignisID)){
                                $Kontaktereignis['apiKey'] = $this->settingsService->getSettings();
                                $Kontaktereignis['kontaktereignisID'] = (int)$mioEvent->kontaktereignisID;
                                $Kontaktereignis['maileon'] = $result;
                                $resultNewOrder['Kontaktereignis'] = $this->synchronizationService->setTransactionEvent($Kontaktereignis);
                            }
                            break;
                         case 'Marketing Program':
                            if(!empty($mioEvent->marketingAutomation)){
                                $marketingAutomation['apiKey'] = $this->settingsService->getSettings();
                                $marketingAutomation['programId'] = (int)$mioEvent->marketingAutomation;
                                $marketingAutomation['email'] = $result['email'];
                                $resultNewOrder['marketingAutomation'] = $this->synchronizationService->setMarketingAutomation($marketingAutomation);
                            }
                            break;

                        default:
                            break;
                    }
                }
            }
        }
        return json_encode($resultNewOrder);
        }
    }


       /**
     * @param EventProceduresTriggered $event
     * @return mixed
     */
    public function externalFulifillment(EventProceduresTriggered $event)
    {
           // getEvent
        $mioEvents ='';
        $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[5.1] Abwicklung extern');
        if(count($mioEventsArray) > 0){
           $mioEvents = $mioEventsArray;
        }else{
           $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[5.1] External fulifillment');
           $mioEvents = $mioEventsArray;
        }
        if(!empty($mioEvents) && count($mioEvents) > 0){
        /** @var Order $order */
        $order = $event->getOrder();
         $contactId = -1;
         $orderItems = $order->orderItems;
        foreach ($orderItems as $orderItem) {
            if($orderItem->typeId == 1){
                $id = $orderItem->id;
                $orderItemName = $orderItem->orderItemName;
                $quantity = $orderItem->quantity;
                $amounts = $orderItem->amounts;
                foreach ($amounts as $amount) {
                    $currency = $amount->currency;
                    $priceGross = $amount->priceGross;
                }
                $item[$id] = array(
                    'id' => $id,
                    'orderItemName' => $orderItemName,
                    'quantity' => $quantity,
                    'currency' => $currency,
                    'priceGross' => $priceGross
                );
            }
        }
        $result['item'] = $item;
        $amountsSummen = $order->amounts;
        foreach ($amountsSummen as $amountsSumme) {
            $result['netTotal'] = $amountsSumme->netTotal;
            $result['grossTotal'] = $amountsSumme->grossTotal;
        }
        /** @var OrderRelationReference[] $r */
        $relations = $order->relations;
        /** @var OrderRelationReference $a */
        foreach($relations as $relation)
        {
            if($relation->relation == 'receiver')
            {
                $contactId = $relation->referenceId;
                $contact = $this->accountService->findContactById($contactId);
                $result['firstName'] = $contact['firstName'];
                $result['lastName'] = $contact['lastName'];
                $result['email'] = $contact['email'];
                $result['gender'] = $contact['gender'];
                $result['contactId'] = $contactId;
                #$result['birthday'] = $contact['birthdayAt'];
                $result['orderId'] = $relation->orderId;
            }
            if(isset($result) && !empty($result)){
                foreach ($mioEvents as $mioEvent) {
                    $integration = $mioEvent->integration;
                    switch ($integration) {
                        case 'Kontaktereignis':
                            if(!empty($mioEvent->kontaktereignisID)){
                                $Kontaktereignis['apiKey'] = $this->settingsService->getSettings();
                                $Kontaktereignis['kontaktereignisID'] = (int)$mioEvent->kontaktereignisID;
                                $Kontaktereignis['maileon'] = $result;
                                $resultNewOrder['Kontaktereignis'] = $this->synchronizationService->setTransactionEvent($Kontaktereignis);
                            }
                            break;
                         case 'Marketing Program':
                            if(!empty($mioEvent->marketingAutomation)){
                                $marketingAutomation['apiKey'] = $this->settingsService->getSettings();
                                $marketingAutomation['programId'] = (int)$mioEvent->marketingAutomation;
                                $marketingAutomation['email'] = $result['email'];
                                $resultNewOrder['marketingAutomation'] = $this->synchronizationService->setMarketingAutomation($marketingAutomation);
                            }
                            break;

                        default:
                            break;
                    }
                }
            }
        }
        return json_encode($resultNewOrder);
        }
    }


   /**
     * @param EventProceduresTriggered $event
     * @return mixed
     */
    public function shippingInProcess(EventProceduresTriggered $event)
    {
          // getEvent
        $mioEvents ='';
        $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[6] Gerade im Versand');
        if(count($mioEventsArray) > 0){
           $mioEvents = $mioEventsArray;
        }else{
           $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[6] Shipping in process');
           $mioEvents = $mioEventsArray;
        }
        if(!empty($mioEvents) && count($mioEvents) > 0){
        /** @var Order $order */
        $order = $event->getOrder();
         $contactId = -1;
         $orderItems = $order->orderItems;
        foreach ($orderItems as $orderItem) {
            if($orderItem->typeId == 1){
                $id = $orderItem->id;
                $orderItemName = $orderItem->orderItemName;
                $quantity = $orderItem->quantity;
                $amounts = $orderItem->amounts;
                foreach ($amounts as $amount) {
                    $currency = $amount->currency;
                    $priceGross = $amount->priceGross;
                }
                $item[$id] = array(
                    'id' => $id,
                    'orderItemName' => $orderItemName,
                    'quantity' => $quantity,
                    'currency' => $currency,
                    'priceGross' => $priceGross
                );
            }
        }
        $result['item'] = $item;
        $amountsSummen = $order->amounts;
        foreach ($amountsSummen as $amountsSumme) {
            $result['netTotal'] = $amountsSumme->netTotal;
            $result['grossTotal'] = $amountsSumme->grossTotal;
        }
        /** @var OrderRelationReference[] $r */
        $relations = $order->relations;
        /** @var OrderRelationReference $a */
        foreach($relations as $relation)
        {
            if($relation->relation == 'receiver')
            {
                $contactId = $relation->referenceId;
                $contact = $this->accountService->findContactById($contactId);
                $result['firstName'] = $contact['firstName'];
                $result['lastName'] = $contact['lastName'];
                $result['email'] = $contact['email'];
                $result['gender'] = $contact['gender'];
                $result['contactId'] = $contactId;
                #$result['birthday'] = $contact['birthdayAt'];
                $result['orderId'] = $relation->orderId;
            }
            if(isset($result) && !empty($result)){
                foreach ($mioEvents as $mioEvent) {
                    $integration = $mioEvent->integration;
                    switch ($integration) {
                        case 'Kontaktereignis':
                            if(!empty($mioEvent->kontaktereignisID)){
                                $Kontaktereignis['apiKey'] = $this->settingsService->getSettings();
                                $Kontaktereignis['kontaktereignisID'] = (int)$mioEvent->kontaktereignisID;
                                $Kontaktereignis['maileon'] = $result;
                                $resultNewOrder['Kontaktereignis'] = $this->synchronizationService->setTransactionEvent($Kontaktereignis);
                            }
                            break;
                         case 'Marketing Program':
                            if(!empty($mioEvent->marketingAutomation)){
                                $marketingAutomation['apiKey'] = $this->settingsService->getSettings();
                                $marketingAutomation['programId'] = (int)$mioEvent->marketingAutomation;
                                $marketingAutomation['email'] = $result['email'];
                                $resultNewOrder['marketingAutomation'] = $this->synchronizationService->setMarketingAutomation($marketingAutomation);
                            }
                            break;

                        default:
                            break;
                    }
                }
            }
        }
        return json_encode($resultNewOrder);
        }
    }


       /**
     * @param EventProceduresTriggered $event
     * @return mixed
     */
    public function shipped(EventProceduresTriggered $event)
    {
           // getEvent
        $mioEvents ='';
        $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[7] Warenausgang gebucht');
        if(count($mioEventsArray) > 0){
           $mioEvents = $mioEventsArray;
        }else{
           $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[7] Shipped');
           $mioEvents = $mioEventsArray;
        }
        if(!empty($mioEvents) && count($mioEvents) > 0){
        /** @var Order $order */
        $order = $event->getOrder();
         $contactId = -1;
         $orderItems = $order->orderItems;
        foreach ($orderItems as $orderItem) {
            if($orderItem->typeId == 1){
                $id = $orderItem->id;
                $orderItemName = $orderItem->orderItemName;
                $quantity = $orderItem->quantity;
                $amounts = $orderItem->amounts;
                foreach ($amounts as $amount) {
                    $currency = $amount->currency;
                    $priceGross = $amount->priceGross;
                }
                $item[$id] = array(
                    'id' => $id,
                    'orderItemName' => $orderItemName,
                    'quantity' => $quantity,
                    'currency' => $currency,
                    'priceGross' => $priceGross
                );
            }
        }
        $result['item'] = $item;
        $amountsSummen = $order->amounts;
        foreach ($amountsSummen as $amountsSumme) {
            $result['netTotal'] = $amountsSumme->netTotal;
            $result['grossTotal'] = $amountsSumme->grossTotal;
        }
        /** @var OrderRelationReference[] $r */
        $relations = $order->relations;
        /** @var OrderRelationReference $a */
        foreach($relations as $relation)
        {
            if($relation->relation == 'receiver')
            {
                $contactId = $relation->referenceId;
                $contact = $this->accountService->findContactById($contactId);
                $result['firstName'] = $contact['firstName'];
                $result['lastName'] = $contact['lastName'];
                $result['email'] = $contact['email'];
                $result['gender'] = $contact['gender'];
                $result['contactId'] = $contactId;
                #$result['birthday'] = $contact['birthdayAt'];
                $result['orderId'] = $relation->orderId;
            }
            if(isset($result) && !empty($result)){
                foreach ($mioEvents as $mioEvent) {
                    $integration = $mioEvent->integration;
                    switch ($integration) {
                        case 'Kontaktereignis':
                            if(!empty($mioEvent->kontaktereignisID)){
                                $Kontaktereignis['apiKey'] = $this->settingsService->getSettings();
                                $Kontaktereignis['kontaktereignisID'] = (int)$mioEvent->kontaktereignisID;
                                $Kontaktereignis['maileon'] = $result;
                                $resultNewOrder['Kontaktereignis'] = $this->synchronizationService->setTransactionEvent($Kontaktereignis);
                            }
                            break;
                         case 'Marketing Program':
                            if(!empty($mioEvent->marketingAutomation)){
                                $marketingAutomation['apiKey'] = $this->settingsService->getSettings();
                                $marketingAutomation['programId'] = (int)$mioEvent->marketingAutomation;
                                $marketingAutomation['email'] = $result['email'];
                                $resultNewOrder['marketingAutomation'] = $this->synchronizationService->setMarketingAutomation($marketingAutomation);
                            }
                            break;

                        default:
                            break;
                    }
                }
            }
        }
        return json_encode($resultNewOrder);
        }
    }


   /**
     * @param EventProceduresTriggered $event
     * @return mixed
     */
    public function orderExported(EventProceduresTriggered $event)
    {
           // getEvent
        $mioEvents ='';
        $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[7.1] Auftrag exportiert');
        if(count($mioEventsArray) > 0){
           $mioEvents = $mioEventsArray;
        }else{
           $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[7.1] Order exported');
           $mioEvents = $mioEventsArray;
        }
        if(!empty($mioEvents) && count($mioEvents) > 0){
        /** @var Order $order */
        $order = $event->getOrder();
         $contactId = -1;
         $orderItems = $order->orderItems;
        foreach ($orderItems as $orderItem) {
            if($orderItem->typeId == 1){
                $id = $orderItem->id;
                $orderItemName = $orderItem->orderItemName;
                $quantity = $orderItem->quantity;
                $amounts = $orderItem->amounts;
                foreach ($amounts as $amount) {
                    $currency = $amount->currency;
                    $priceGross = $amount->priceGross;
                }
                $item[$id] = array(
                    'id' => $id,
                    'orderItemName' => $orderItemName,
                    'quantity' => $quantity,
                    'currency' => $currency,
                    'priceGross' => $priceGross
                );
            }
        }
        $result['item'] = $item;
        $amountsSummen = $order->amounts;
        foreach ($amountsSummen as $amountsSumme) {
            $result['netTotal'] = $amountsSumme->netTotal;
            $result['grossTotal'] = $amountsSumme->grossTotal;
        }
        /** @var OrderRelationReference[] $r */
        $relations = $order->relations;
        /** @var OrderRelationReference $a */
        foreach($relations as $relation)
        {
            if($relation->relation == 'receiver')
            {
                $contactId = $relation->referenceId;
                $contact = $this->accountService->findContactById($contactId);
                $result['firstName'] = $contact['firstName'];
                $result['lastName'] = $contact['lastName'];
                $result['email'] = $contact['email'];
                $result['gender'] = $contact['gender'];
                $result['contactId'] = $contactId;
                #$result['birthday'] = $contact['birthdayAt'];
                $result['orderId'] = $relation->orderId;
            }
            if(isset($result) && !empty($result)){
                foreach ($mioEvents as $mioEvent) {
                    $integration = $mioEvent->integration;
                    switch ($integration) {
                        case 'Kontaktereignis':
                            if(!empty($mioEvent->kontaktereignisID)){
                                $Kontaktereignis['apiKey'] = $this->settingsService->getSettings();
                                $Kontaktereignis['kontaktereignisID'] = (int)$mioEvent->kontaktereignisID;
                                $Kontaktereignis['maileon'] = $result;
                                $resultNewOrder['Kontaktereignis'] = $this->synchronizationService->setTransactionEvent($Kontaktereignis);
                            }
                            break;
                         case 'Marketing Program':
                            if(!empty($mioEvent->marketingAutomation)){
                                $marketingAutomation['apiKey'] = $this->settingsService->getSettings();
                                $marketingAutomation['programId'] = (int)$mioEvent->marketingAutomation;
                                $marketingAutomation['email'] = $result['email'];
                                $resultNewOrder['marketingAutomation'] = $this->synchronizationService->setMarketingAutomation($marketingAutomation);
                            }
                            break;

                        default:
                            break;
                    }
                }
            }
        }
        return json_encode($resultNewOrder);
        }
    }


       /**
     * @param EventProceduresTriggered $event
     * @return mixed
     */
    public function cancelled(EventProceduresTriggered $event)
    {
           // getEvent
        $mioEvents ='';
        $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[8] Storniert');
        if(count($mioEventsArray) > 0){
           $mioEvents = $mioEventsArray;
        }else{
           $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[8] Cancelled');
           $mioEvents = $mioEventsArray;
        }
        if(!empty($mioEvents) && count($mioEvents) > 0){
        /** @var Order $order */
        $order = $event->getOrder();
         $contactId = -1;
         $orderItems = $order->orderItems;
        foreach ($orderItems as $orderItem) {
            if($orderItem->typeId == 1){
                $id = $orderItem->id;
                $orderItemName = $orderItem->orderItemName;
                $quantity = $orderItem->quantity;
                $amounts = $orderItem->amounts;
                foreach ($amounts as $amount) {
                    $currency = $amount->currency;
                    $priceGross = $amount->priceGross;
                }
                $item[$id] = array(
                    'id' => $id,
                    'orderItemName' => $orderItemName,
                    'quantity' => $quantity,
                    'currency' => $currency,
                    'priceGross' => $priceGross
                );
            }
        }
        $result['item'] = $item;
        $amountsSummen = $order->amounts;
        foreach ($amountsSummen as $amountsSumme) {
            $result['netTotal'] = $amountsSumme->netTotal;
            $result['grossTotal'] = $amountsSumme->grossTotal;
        }
        /** @var OrderRelationReference[] $r */
        $relations = $order->relations;
        /** @var OrderRelationReference $a */
        foreach($relations as $relation)
        {
            if($relation->relation == 'receiver')
            {
                $contactId = $relation->referenceId;
                $contact = $this->accountService->findContactById($contactId);
                $result['firstName'] = $contact['firstName'];
                $result['lastName'] = $contact['lastName'];
                $result['email'] = $contact['email'];
                $result['gender'] = $contact['gender'];
                $result['contactId'] = $contactId;
                #$result['birthday'] = $contact['birthdayAt'];
                $result['orderId'] = $relation->orderId;
            }
            if(isset($result) && !empty($result)){
                foreach ($mioEvents as $mioEvent) {
                    $integration = $mioEvent->integration;
                    switch ($integration) {
                        case 'Kontaktereignis':
                            if(!empty($mioEvent->kontaktereignisID)){
                                $Kontaktereignis['apiKey'] = $this->settingsService->getSettings();
                                $Kontaktereignis['kontaktereignisID'] = (int)$mioEvent->kontaktereignisID;
                                $Kontaktereignis['maileon'] = $result;
                                $resultNewOrder['Kontaktereignis'] = $this->synchronizationService->setTransactionEvent($Kontaktereignis);
                            }
                            break;
                         case 'Marketing Program':
                            if(!empty($mioEvent->marketingAutomation)){
                                $marketingAutomation['apiKey'] = $this->settingsService->getSettings();
                                $marketingAutomation['programId'] = (int)$mioEvent->marketingAutomation;
                                $marketingAutomation['email'] = $result['email'];
                                $resultNewOrder['marketingAutomation'] = $this->synchronizationService->setMarketingAutomation($marketingAutomation);
                            }
                            break;

                        default:
                            break;
                    }
                }
            }
        }
        return json_encode($resultNewOrder);
        }
    }


   /**
     * @param EventProceduresTriggered $event
     * @return mixed
     */
    public function cancelledByCustomer(EventProceduresTriggered $event)
    {
           // getEvent
        $mioEvents ='';
        $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[8.1] Storniert durch Kunden');
        if(count($mioEventsArray) > 0){
           $mioEvents = $mioEventsArray;
        }else{
           $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[8.1] Cancelled by customer');
           $mioEvents = $mioEventsArray;
        }
        if(!empty($mioEvents) && count($mioEvents) > 0){
        /** @var Order $order */
        $order = $event->getOrder();
         $contactId = -1;
         $orderItems = $order->orderItems;
        foreach ($orderItems as $orderItem) {
            if($orderItem->typeId == 1){
                $id = $orderItem->id;
                $orderItemName = $orderItem->orderItemName;
                $quantity = $orderItem->quantity;
                $amounts = $orderItem->amounts;
                foreach ($amounts as $amount) {
                    $currency = $amount->currency;
                    $priceGross = $amount->priceGross;
                }
                $item[$id] = array(
                    'id' => $id,
                    'orderItemName' => $orderItemName,
                    'quantity' => $quantity,
                    'currency' => $currency,
                    'priceGross' => $priceGross
                );
            }
        }
        $result['item'] = $item;
        $amountsSummen = $order->amounts;
        foreach ($amountsSummen as $amountsSumme) {
            $result['netTotal'] = $amountsSumme->netTotal;
            $result['grossTotal'] = $amountsSumme->grossTotal;
        }
        /** @var OrderRelationReference[] $r */
        $relations = $order->relations;
        /** @var OrderRelationReference $a */
        foreach($relations as $relation)
        {
            if($relation->relation == 'receiver')
            {
                $contactId = $relation->referenceId;
                $contact = $this->accountService->findContactById($contactId);
                $result['firstName'] = $contact['firstName'];
                $result['lastName'] = $contact['lastName'];
                $result['email'] = $contact['email'];
                $result['gender'] = $contact['gender'];
                $result['contactId'] = $contactId;
                #$result['birthday'] = $contact['birthdayAt'];
                $result['orderId'] = $relation->orderId;
            }
            if(isset($result) && !empty($result)){
                foreach ($mioEvents as $mioEvent) {
                    $integration = $mioEvent->integration;
                    switch ($integration) {
                        case 'Kontaktereignis':
                            if(!empty($mioEvent->kontaktereignisID)){
                                $Kontaktereignis['apiKey'] = $this->settingsService->getSettings();
                                $Kontaktereignis['kontaktereignisID'] = (int)$mioEvent->kontaktereignisID;
                                $Kontaktereignis['maileon'] = $result;
                                $resultNewOrder['Kontaktereignis'] = $this->synchronizationService->setTransactionEvent($Kontaktereignis);
                            }
                            break;
                         case 'Marketing Program':
                            if(!empty($mioEvent->marketingAutomation)){
                                $marketingAutomation['apiKey'] = $this->settingsService->getSettings();
                                $marketingAutomation['programId'] = (int)$mioEvent->marketingAutomation;
                                $marketingAutomation['email'] = $result['email'];
                                $resultNewOrder['marketingAutomation'] = $this->synchronizationService->setMarketingAutomation($marketingAutomation);
                            }
                            break;

                        default:
                            break;
                    }
                }
            }
        }
        return json_encode($resultNewOrder);
        }
    }


       /**
     * @param EventProceduresTriggered $event
     * @return mixed
     */
    public function orderReturn(EventProceduresTriggered $event)
    {
           // getEvent
        $mioEvents ='';
        $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[9] Retoure');
        if(count($mioEventsArray) > 0){
           $mioEvents = $mioEventsArray;
        }else{
           $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[9] Return');
           $mioEvents = $mioEventsArray;
        }
        if(!empty($mioEvents) && count($mioEvents) > 0){
        /** @var Order $order */
        $order = $event->getOrder();
         $contactId = -1;
         $orderItems = $order->orderItems;
        foreach ($orderItems as $orderItem) {
            if($orderItem->typeId == 1){
                $id = $orderItem->id;
                $orderItemName = $orderItem->orderItemName;
                $quantity = $orderItem->quantity;
                $amounts = $orderItem->amounts;
                foreach ($amounts as $amount) {
                    $currency = $amount->currency;
                    $priceGross = $amount->priceGross;
                }
                $item[$id] = array(
                    'id' => $id,
                    'orderItemName' => $orderItemName,
                    'quantity' => $quantity,
                    'currency' => $currency,
                    'priceGross' => $priceGross
                );
            }
        }
        $result['item'] = $item;
        $amountsSummen = $order->amounts;
        foreach ($amountsSummen as $amountsSumme) {
            $result['netTotal'] = $amountsSumme->netTotal;
            $result['grossTotal'] = $amountsSumme->grossTotal;
        }
        /** @var OrderRelationReference[] $r */
        $relations = $order->relations;
        /** @var OrderRelationReference $a */
        foreach($relations as $relation)
        {
            if($relation->relation == 'receiver')
            {
                $contactId = $relation->referenceId;
                $contact = $this->accountService->findContactById($contactId);
                $result['firstName'] = $contact['firstName'];
                $result['lastName'] = $contact['lastName'];
                $result['email'] = $contact['email'];
                $result['gender'] = $contact['gender'];
                $result['contactId'] = $contactId;
                #$result['birthday'] = $contact['birthdayAt'];
                $result['orderId'] = $relation->orderId;
            }
            if(isset($result) && !empty($result)){
                foreach ($mioEvents as $mioEvent) {
                    $integration = $mioEvent->integration;
                    switch ($integration) {
                        case 'Kontaktereignis':
                            if(!empty($mioEvent->kontaktereignisID)){
                                $Kontaktereignis['apiKey'] = $this->settingsService->getSettings();
                                $Kontaktereignis['kontaktereignisID'] = (int)$mioEvent->kontaktereignisID;
                                $Kontaktereignis['maileon'] = $result;
                                $resultNewOrder['Kontaktereignis'] = $this->synchronizationService->setTransactionEvent($Kontaktereignis);
                            }
                            break;
                         case 'Marketing Program':
                            if(!empty($mioEvent->marketingAutomation)){
                                $marketingAutomation['apiKey'] = $this->settingsService->getSettings();
                                $marketingAutomation['programId'] = (int)$mioEvent->marketingAutomation;
                                $marketingAutomation['email'] = $result['email'];
                                $resultNewOrder['marketingAutomation'] = $this->synchronizationService->setMarketingAutomation($marketingAutomation);
                            }
                            break;

                        default:
                            break;
                    }
                }
            }
        }
        return json_encode($resultNewOrder);
        }
    }


   /**
     * @param EventProceduresTriggered $event
     * @return mixed
     */
    public function itemsAreUndergoingChecks(EventProceduresTriggered $event)
    {
           // getEvent
        $mioEvents ='';
        $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[9.1] Ware wird geprüft');
        if(count($mioEventsArray) > 0){
           $mioEvents = $mioEventsArray;
        }else{
           $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[9.1] Items are undergoing checks');
           $mioEvents = $mioEventsArray;
        }
        if(!empty($mioEvents) && count($mioEvents) > 0){
        /** @var Order $order */
        $order = $event->getOrder();
         $contactId = -1;
         $orderItems = $order->orderItems;
        foreach ($orderItems as $orderItem) {
            if($orderItem->typeId == 1){
                $id = $orderItem->id;
                $orderItemName = $orderItem->orderItemName;
                $quantity = $orderItem->quantity;
                $amounts = $orderItem->amounts;
                foreach ($amounts as $amount) {
                    $currency = $amount->currency;
                    $priceGross = $amount->priceGross;
                }
                $item[$id] = array(
                    'id' => $id,
                    'orderItemName' => $orderItemName,
                    'quantity' => $quantity,
                    'currency' => $currency,
                    'priceGross' => $priceGross
                );
            }
        }
        $result['item'] = $item;
        $amountsSummen = $order->amounts;
        foreach ($amountsSummen as $amountsSumme) {
            $result['netTotal'] = $amountsSumme->netTotal;
            $result['grossTotal'] = $amountsSumme->grossTotal;
        }
        /** @var OrderRelationReference[] $r */
        $relations = $order->relations;
        /** @var OrderRelationReference $a */
        foreach($relations as $relation)
        {
            if($relation->relation == 'receiver')
            {
                $contactId = $relation->referenceId;
                $contact = $this->accountService->findContactById($contactId);
                $result['firstName'] = $contact['firstName'];
                $result['lastName'] = $contact['lastName'];
                $result['email'] = $contact['email'];
                $result['gender'] = $contact['gender'];
                $result['contactId'] = $contactId;
                #$result['birthday'] = $contact['birthdayAt'];
                $result['orderId'] = $relation->orderId;
            }
            if(isset($result) && !empty($result)){
                foreach ($mioEvents as $mioEvent) {
                    $integration = $mioEvent->integration;
                    switch ($integration) {
                        case 'Kontaktereignis':
                            if(!empty($mioEvent->kontaktereignisID)){
                                $Kontaktereignis['apiKey'] = $this->settingsService->getSettings();
                                $Kontaktereignis['kontaktereignisID'] = (int)$mioEvent->kontaktereignisID;
                                $Kontaktereignis['maileon'] = $result;
                                $resultNewOrder['Kontaktereignis'] = $this->synchronizationService->setTransactionEvent($Kontaktereignis);
                            }
                            break;
                         case 'Marketing Program':
                            if(!empty($mioEvent->marketingAutomation)){
                                $marketingAutomation['apiKey'] = $this->settingsService->getSettings();
                                $marketingAutomation['programId'] = (int)$mioEvent->marketingAutomation;
                                $marketingAutomation['email'] = $result['email'];
                                $resultNewOrder['marketingAutomation'] = $this->synchronizationService->setMarketingAutomation($marketingAutomation);
                            }
                            break;

                        default:
                            break;
                    }
                }
            }
        }
        return json_encode($resultNewOrder);
        }
    }


       /**
     * @param EventProceduresTriggered $event
     * @return mixed
     */
    public function waitingForReturnFromWholeseller(EventProceduresTriggered $event)
    {
        // getEvent
        $mioEvents ='';
        $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[9.2] Warten auf Retoure von Großhändler');

        if(count($mioEventsArray) > 0){
           $mioEvents = $mioEventsArray;
        }else{
           $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[9.2] Waiting for return from wholeseller');
           $mioEvents = $mioEventsArray;
        }

        if(!empty($mioEvents) && count($mioEvents) > 0){
        /** @var Order $order */
        $order = $event->getOrder();
         $contactId = -1;
         $orderItems = $order->orderItems;
        foreach ($orderItems as $orderItem) {
            if($orderItem->typeId == 1){
                $id = $orderItem->id;
                $orderItemName = $orderItem->orderItemName;
                $quantity = $orderItem->quantity;
                $amounts = $orderItem->amounts;
                foreach ($amounts as $amount) {
                    $currency = $amount->currency;
                    $priceGross = $amount->priceGross;
                }
                $item[$id] = array(
                    'id' => $id,
                    'orderItemName' => $orderItemName,
                    'quantity' => $quantity,
                    'currency' => $currency,
                    'priceGross' => $priceGross
                );
            }
        }
        $result['item'] = $item;
        $amountsSummen = $order->amounts;
        foreach ($amountsSummen as $amountsSumme) {
            $result['netTotal'] = $amountsSumme->netTotal;
            $result['grossTotal'] = $amountsSumme->grossTotal;
        }
        /** @var OrderRelationReference[] $r */
        $relations = $order->relations;
        /** @var OrderRelationReference $a */
        foreach($relations as $relation)
        {
            if($relation->relation == 'receiver')
            {
                $contactId = $relation->referenceId;
                $contact = $this->accountService->findContactById($contactId);
                $result['firstName'] = $contact['firstName'];
                $result['lastName'] = $contact['lastName'];
                $result['email'] = $contact['email'];
                $result['gender'] = $contact['gender'];
                $result['contactId'] = $contactId;
                #$result['birthday'] = $contact['birthdayAt'];
                $result['orderId'] = $relation->orderId;
            }
            if(isset($result) && !empty($result)){
                foreach ($mioEvents as $mioEvent) {
                    $integration = $mioEvent->integration;
                    switch ($integration) {
                        case 'Kontaktereignis':
                            if(!empty($mioEvent->kontaktereignisID)){
                                $Kontaktereignis['apiKey'] = $this->settingsService->getSettings();
                                $Kontaktereignis['kontaktereignisID'] = (int)$mioEvent->kontaktereignisID;
                                $Kontaktereignis['maileon'] = $result;
                                $resultNewOrder['Kontaktereignis'] = $this->synchronizationService->setTransactionEvent($Kontaktereignis);
                            }
                            break;
                         case 'Marketing Program':
                            if(!empty($mioEvent->marketingAutomation)){
                                $marketingAutomation['apiKey'] = $this->settingsService->getSettings();
                                $marketingAutomation['programId'] = (int)$mioEvent->marketingAutomation;
                                $marketingAutomation['email'] = $result['email'];
                                $resultNewOrder['marketingAutomation'] = $this->synchronizationService->setMarketingAutomation($marketingAutomation);
                            }
                            break;

                        default:
                            break;
                    }
                }
            }
        }
        return json_encode($resultNewOrder);
        }
    }


   /**
     * @param EventProceduresTriggered $event
     * @return mixed
     */
    public function warrantyProcessStarted(EventProceduresTriggered $event)
    {
           // getEvent
        $mioEvents ='';
        $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[9.3] Gewährleistung eingeleitet');
        if(count($mioEventsArray) > 0){
           $mioEvents = $mioEventsArray;
        }else{
           $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[9.3] Warranty process started');
           $mioEvents = $mioEventsArray;
        }
        if(!empty($mioEvents) && count($mioEvents) > 0){
        /** @var Order $order */
        $order = $event->getOrder();
         $contactId = -1;
         $orderItems = $order->orderItems;
        foreach ($orderItems as $orderItem) {
            if($orderItem->typeId == 1){
                $id = $orderItem->id;
                $orderItemName = $orderItem->orderItemName;
                $quantity = $orderItem->quantity;
                $amounts = $orderItem->amounts;
                foreach ($amounts as $amount) {
                    $currency = $amount->currency;
                    $priceGross = $amount->priceGross;
                }
                $item[$id] = array(
                    'id' => $id,
                    'orderItemName' => $orderItemName,
                    'quantity' => $quantity,
                    'currency' => $currency,
                    'priceGross' => $priceGross
                );
            }
        }
        $result['item'] = $item;
        $amountsSummen = $order->amounts;
        foreach ($amountsSummen as $amountsSumme) {
            $result['netTotal'] = $amountsSumme->netTotal;
            $result['grossTotal'] = $amountsSumme->grossTotal;
        }
        /** @var OrderRelationReference[] $r */
        $relations = $order->relations;
        /** @var OrderRelationReference $a */
        foreach($relations as $relation)
        {
            if($relation->relation == 'receiver')
            {
                $contactId = $relation->referenceId;
                $contact = $this->accountService->findContactById($contactId);
                $result['firstName'] = $contact['firstName'];
                $result['lastName'] = $contact['lastName'];
                $result['email'] = $contact['email'];
                $result['gender'] = $contact['gender'];
                $result['contactId'] = $contactId;
                #$result['birthday'] = $contact['birthdayAt'];
                $result['orderId'] = $relation->orderId;
            }
            if(isset($result) && !empty($result)){
                foreach ($mioEvents as $mioEvent) {
                    $integration = $mioEvent->integration;
                    switch ($integration) {
                        case 'Kontaktereignis':
                            if(!empty($mioEvent->kontaktereignisID)){
                                $Kontaktereignis['apiKey'] = $this->settingsService->getSettings();
                                $Kontaktereignis['kontaktereignisID'] = (int)$mioEvent->kontaktereignisID;
                                $Kontaktereignis['maileon'] = $result;
                                $resultNewOrder['Kontaktereignis'] = $this->synchronizationService->setTransactionEvent($Kontaktereignis);
                            }
                            break;
                         case 'Marketing Program':
                            if(!empty($mioEvent->marketingAutomation)){
                                $marketingAutomation['apiKey'] = $this->settingsService->getSettings();
                                $marketingAutomation['programId'] = (int)$mioEvent->marketingAutomation;
                                $marketingAutomation['email'] = $result['email'];
                                $resultNewOrder['marketingAutomation'] = $this->synchronizationService->setMarketingAutomation($marketingAutomation);
                            }
                            break;

                        default:
                            break;
                    }
                }
            }
        }
        return json_encode($resultNewOrder);
        }
    }


       /**
     * @param EventProceduresTriggered $event
     * @return mixed
     */
    public function exchangeProcessStarted(EventProceduresTriggered $event)
    {
           // getEvent
        $mioEvents ='';
        $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[9.4] Umtausch eingeleitet');
        if(count($mioEventsArray) > 0){
           $mioEvents = $mioEventsArray;
        }else{
           $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[9.4] exchange process started');
           $mioEvents = $mioEventsArray;
        }
        if(!empty($mioEvents) && count($mioEvents) > 0){
        /** @var Order $order */
        $order = $event->getOrder();
         $contactId = -1;
         $orderItems = $order->orderItems;
        foreach ($orderItems as $orderItem) {
            if($orderItem->typeId == 1){
                $id = $orderItem->id;
                $orderItemName = $orderItem->orderItemName;
                $quantity = $orderItem->quantity;
                $amounts = $orderItem->amounts;
                foreach ($amounts as $amount) {
                    $currency = $amount->currency;
                    $priceGross = $amount->priceGross;
                }
                $item[$id] = array(
                    'id' => $id,
                    'orderItemName' => $orderItemName,
                    'quantity' => $quantity,
                    'currency' => $currency,
                    'priceGross' => $priceGross
                );
            }
        }
        $result['item'] = $item;
        $amountsSummen = $order->amounts;
        foreach ($amountsSummen as $amountsSumme) {
            $result['netTotal'] = $amountsSumme->netTotal;
            $result['grossTotal'] = $amountsSumme->grossTotal;
        }
        /** @var OrderRelationReference[] $r */
        $relations = $order->relations;
        /** @var OrderRelationReference $a */
        foreach($relations as $relation)
        {
            if($relation->relation == 'receiver')
            {
                $contactId = $relation->referenceId;
                $contact = $this->accountService->findContactById($contactId);
                $result['firstName'] = $contact['firstName'];
                $result['lastName'] = $contact['lastName'];
                $result['email'] = $contact['email'];
                $result['gender'] = $contact['gender'];
                $result['contactId'] = $contactId;
                #$result['birthday'] = $contact['birthdayAt'];
                $result['orderId'] = $relation->orderId;
            }
            if(isset($result) && !empty($result)){
                foreach ($mioEvents as $mioEvent) {
                    $integration = $mioEvent->integration;
                    switch ($integration) {
                        case 'Kontaktereignis':
                            if(!empty($mioEvent->kontaktereignisID)){
                                $Kontaktereignis['apiKey'] = $this->settingsService->getSettings();
                                $Kontaktereignis['kontaktereignisID'] = (int)$mioEvent->kontaktereignisID;
                                $Kontaktereignis['maileon'] = $result;
                                $resultNewOrder['Kontaktereignis'] = $this->synchronizationService->setTransactionEvent($Kontaktereignis);
                            }
                            break;
                         case 'Marketing Program':
                            if(!empty($mioEvent->marketingAutomation)){
                                $marketingAutomation['apiKey'] = $this->settingsService->getSettings();
                                $marketingAutomation['programId'] = (int)$mioEvent->marketingAutomation;
                                $marketingAutomation['email'] = $result['email'];
                                $resultNewOrder['marketingAutomation'] = $this->synchronizationService->setMarketingAutomation($marketingAutomation);
                            }
                            break;

                        default:
                            break;
                    }
                }
            }
        }
        return json_encode($resultNewOrder);
        }
    }


   /**
     * @param EventProceduresTriggered $event
     * @return mixed
     */
    public function creditNoteCreated(EventProceduresTriggered $event)
    {
           // getEvent
        $mioEvents ='';
        $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[9.5] Gutschrift angelegt');
        if(count($mioEventsArray) > 0){
           $mioEvents = $mioEventsArray;
        }else{
           $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[9.5] Credit note created');
           $mioEvents = $mioEventsArray;
        }
        if(!empty($mioEvents) && count($mioEvents) > 0){
        /** @var Order $order */
        $order = $event->getOrder();
         $contactId = -1;
         $orderItems = $order->orderItems;
        foreach ($orderItems as $orderItem) {
            if($orderItem->typeId == 1){
                $id = $orderItem->id;
                $orderItemName = $orderItem->orderItemName;
                $quantity = $orderItem->quantity;
                $amounts = $orderItem->amounts;
                foreach ($amounts as $amount) {
                    $currency = $amount->currency;
                    $priceGross = $amount->priceGross;
                }
                $item[$id] = array(
                    'id' => $id,
                    'orderItemName' => $orderItemName,
                    'quantity' => $quantity,
                    'currency' => $currency,
                    'priceGross' => $priceGross
                );
            }
        }
        $result['item'] = $item;
        $amountsSummen = $order->amounts;
        foreach ($amountsSummen as $amountsSumme) {
            $result['netTotal'] = $amountsSumme->netTotal;
            $result['grossTotal'] = $amountsSumme->grossTotal;
        }
        /** @var OrderRelationReference[] $r */
        $relations = $order->relations;
        /** @var OrderRelationReference $a */
        foreach($relations as $relation)
        {
            if($relation->relation == 'receiver')
            {
                $contactId = $relation->referenceId;
                $contact = $this->accountService->findContactById($contactId);
                $result['firstName'] = $contact['firstName'];
                $result['lastName'] = $contact['lastName'];
                $result['email'] = $contact['email'];
                $result['gender'] = $contact['gender'];
                $result['contactId'] = $contactId;
                #$result['birthday'] = $contact['birthdayAt'];
                $result['orderId'] = $relation->orderId;
            }
            if(isset($result) && !empty($result)){
                foreach ($mioEvents as $mioEvent) {
                    $integration = $mioEvent->integration;
                    switch ($integration) {
                        case 'Kontaktereignis':
                            if(!empty($mioEvent->kontaktereignisID)){
                                $Kontaktereignis['apiKey'] = $this->settingsService->getSettings();
                                $Kontaktereignis['kontaktereignisID'] = (int)$mioEvent->kontaktereignisID;
                                $Kontaktereignis['maileon'] = $result;
                                $resultNewOrder['Kontaktereignis'] = $this->synchronizationService->setTransactionEvent($Kontaktereignis);
                            }
                            break;
                         case 'Marketing Program':
                            if(!empty($mioEvent->marketingAutomation)){
                                $marketingAutomation['apiKey'] = $this->settingsService->getSettings();
                                $marketingAutomation['programId'] = (int)$mioEvent->marketingAutomation;
                                $marketingAutomation['email'] = $result['email'];
                                $resultNewOrder['marketingAutomation'] = $this->synchronizationService->setMarketingAutomation($marketingAutomation);
                            }
                            break;

                        default:
                            break;
                    }
                }
            }
        }
        return json_encode($resultNewOrder);
        }
    }


       /**
     * @param EventProceduresTriggered $event
     * @return mixed
     */
    public function warranty(EventProceduresTriggered $event)
    {
           // getEvent
        $mioEvents ='';
        $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[10] Gewährleistung');
        if(count($mioEventsArray) > 0){
           $mioEvents = $mioEventsArray;
        }else{
           $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[10] Warranty');
           $mioEvents = $mioEventsArray;
        }
        if(!empty($mioEvents) && count($mioEvents) > 0){
        /** @var Order $order */
        $order = $event->getOrder();
         $contactId = -1;
         $orderItems = $order->orderItems;
        foreach ($orderItems as $orderItem) {
            if($orderItem->typeId == 1){
                $id = $orderItem->id;
                $orderItemName = $orderItem->orderItemName;
                $quantity = $orderItem->quantity;
                $amounts = $orderItem->amounts;
                foreach ($amounts as $amount) {
                    $currency = $amount->currency;
                    $priceGross = $amount->priceGross;
                }
                $item[$id] = array(
                    'id' => $id,
                    'orderItemName' => $orderItemName,
                    'quantity' => $quantity,
                    'currency' => $currency,
                    'priceGross' => $priceGross
                );
            }
        }
        $result['item'] = $item;
        $amountsSummen = $order->amounts;
        foreach ($amountsSummen as $amountsSumme) {
            $result['netTotal'] = $amountsSumme->netTotal;
            $result['grossTotal'] = $amountsSumme->grossTotal;
        }
        /** @var OrderRelationReference[] $r */
        $relations = $order->relations;
        /** @var OrderRelationReference $a */
        foreach($relations as $relation)
        {
            if($relation->relation == 'receiver')
            {
                $contactId = $relation->referenceId;
                $contact = $this->accountService->findContactById($contactId);
                $result['firstName'] = $contact['firstName'];
                $result['lastName'] = $contact['lastName'];
                $result['email'] = $contact['email'];
                $result['gender'] = $contact['gender'];
                $result['contactId'] = $contactId;
                #$result['birthday'] = $contact['birthdayAt'];
                $result['orderId'] = $relation->orderId;
            }
            if(isset($result) && !empty($result)){
                foreach ($mioEvents as $mioEvent) {
                    $integration = $mioEvent->integration;
                    switch ($integration) {
                        case 'Kontaktereignis':
                            if(!empty($mioEvent->kontaktereignisID)){
                                $Kontaktereignis['apiKey'] = $this->settingsService->getSettings();
                                $Kontaktereignis['kontaktereignisID'] = (int)$mioEvent->kontaktereignisID;
                                $Kontaktereignis['maileon'] = $result;
                                $resultNewOrder['Kontaktereignis'] = $this->synchronizationService->setTransactionEvent($Kontaktereignis);
                            }
                            break;
                         case 'Marketing Program':
                            if(!empty($mioEvent->marketingAutomation)){
                                $marketingAutomation['apiKey'] = $this->settingsService->getSettings();
                                $marketingAutomation['programId'] = (int)$mioEvent->marketingAutomation;
                                $marketingAutomation['email'] = $result['email'];
                                $resultNewOrder['marketingAutomation'] = $this->synchronizationService->setMarketingAutomation($marketingAutomation);
                            }
                            break;

                        default:
                            break;
                    }
                }
            }
        }
        return json_encode($resultNewOrder);
        }
    }


   /**
     * @param EventProceduresTriggered $event
     * @return mixed
     */
    public function creditNote(EventProceduresTriggered $event)
    {
           // getEvent
        $mioEvents ='';
        $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[11] Gutschrift');
        if(count($mioEventsArray) > 0){
           $mioEvents = $mioEventsArray;
        }else{
           $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[11] Credit note');
           $mioEvents = $mioEventsArray;
        }
        if(!empty($mioEvents) && count($mioEvents) > 0){
        /** @var Order $order */
        $order = $event->getOrder();
         $contactId = -1;
         $orderItems = $order->orderItems;
        foreach ($orderItems as $orderItem) {
            if($orderItem->typeId == 1){
                $id = $orderItem->id;
                $orderItemName = $orderItem->orderItemName;
                $quantity = $orderItem->quantity;
                $amounts = $orderItem->amounts;
                foreach ($amounts as $amount) {
                    $currency = $amount->currency;
                    $priceGross = $amount->priceGross;
                }
                $item[$id] = array(
                    'id' => $id,
                    'orderItemName' => $orderItemName,
                    'quantity' => $quantity,
                    'currency' => $currency,
                    'priceGross' => $priceGross
                );
            }
        }
        $result['item'] = $item;
        $amountsSummen = $order->amounts;
        foreach ($amountsSummen as $amountsSumme) {
            $result['netTotal'] = $amountsSumme->netTotal;
            $result['grossTotal'] = $amountsSumme->grossTotal;
        }
        /** @var OrderRelationReference[] $r */
        $relations = $order->relations;
        /** @var OrderRelationReference $a */
        foreach($relations as $relation)
        {
            if($relation->relation == 'receiver')
            {
                $contactId = $relation->referenceId;
                $contact = $this->accountService->findContactById($contactId);
                $result['firstName'] = $contact['firstName'];
                $result['lastName'] = $contact['lastName'];
                $result['email'] = $contact['email'];
                $result['gender'] = $contact['gender'];
                $result['contactId'] = $contactId;
                #$result['birthday'] = $contact['birthdayAt'];
                $result['orderId'] = $relation->orderId;
            }
            if(isset($result) && !empty($result)){
                foreach ($mioEvents as $mioEvent) {
                    $integration = $mioEvent->integration;
                    switch ($integration) {
                        case 'Kontaktereignis':
                            if(!empty($mioEvent->kontaktereignisID)){
                                $Kontaktereignis['apiKey'] = $this->settingsService->getSettings();
                                $Kontaktereignis['kontaktereignisID'] = (int)$mioEvent->kontaktereignisID;
                                $Kontaktereignis['maileon'] = $result;
                                $resultNewOrder['Kontaktereignis'] = $this->synchronizationService->setTransactionEvent($Kontaktereignis);
                            }
                            break;
                         case 'Marketing Program':
                            if(!empty($mioEvent->marketingAutomation)){
                                $marketingAutomation['apiKey'] = $this->settingsService->getSettings();
                                $marketingAutomation['programId'] = (int)$mioEvent->marketingAutomation;
                                $marketingAutomation['email'] = $result['email'];
                                $resultNewOrder['marketingAutomation'] = $this->synchronizationService->setMarketingAutomation($marketingAutomation);
                            }
                            break;

                        default:
                            break;
                    }
                }
            }
        }
        return json_encode($resultNewOrder);
        }
    }

       /**
     * @param EventProceduresTriggered $event
     * @return mixed
     */
    public function refundDone(EventProceduresTriggered $event)
    {
           // getEvent
        $mioEvents ='';
        $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[11.1] Gutschrift ausgezahlt');
        if(count($mioEventsArray) > 0){
           $mioEvents = $mioEventsArray;
        }else{
           $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[11.1] Refund done');
           $mioEvents = $mioEventsArray;
        }
        if(!empty($mioEvents) && count($mioEvents) > 0){
        /** @var Order $order */
        $order = $event->getOrder();
         $contactId = -1;
         $orderItems = $order->orderItems;
        foreach ($orderItems as $orderItem) {
            if($orderItem->typeId == 1){
                $id = $orderItem->id;
                $orderItemName = $orderItem->orderItemName;
                $quantity = $orderItem->quantity;
                $amounts = $orderItem->amounts;
                foreach ($amounts as $amount) {
                    $currency = $amount->currency;
                    $priceGross = $amount->priceGross;
                }
                $item[$id] = array(
                    'id' => $id,
                    'orderItemName' => $orderItemName,
                    'quantity' => $quantity,
                    'currency' => $currency,
                    'priceGross' => $priceGross
                );
            }
        }
        $result['item'] = $item;
        $amountsSummen = $order->amounts;
        foreach ($amountsSummen as $amountsSumme) {
            $result['netTotal'] = $amountsSumme->netTotal;
            $result['grossTotal'] = $amountsSumme->grossTotal;
        }
        /** @var OrderRelationReference[] $r */
        $relations = $order->relations;
        /** @var OrderRelationReference $a */
        foreach($relations as $relation)
        {
            if($relation->relation == 'receiver')
            {
                $contactId = $relation->referenceId;
                $contact = $this->accountService->findContactById($contactId);
                $result['firstName'] = $contact['firstName'];
                $result['lastName'] = $contact['lastName'];
                $result['email'] = $contact['email'];
                $result['gender'] = $contact['gender'];
                $result['contactId'] = $contactId;
                #$result['birthday'] = $contact['birthdayAt'];
                $result['orderId'] = $relation->orderId;
            }
            if(isset($result) && !empty($result)){
                foreach ($mioEvents as $mioEvent) {
                    $integration = $mioEvent->integration;
                    switch ($integration) {
                        case 'Kontaktereignis':
                            if(!empty($mioEvent->kontaktereignisID)){
                                $Kontaktereignis['apiKey'] = $this->settingsService->getSettings();
                                $Kontaktereignis['kontaktereignisID'] = (int)$mioEvent->kontaktereignisID;
                                $Kontaktereignis['maileon'] = $result;
                                $resultNewOrder['Kontaktereignis'] = $this->synchronizationService->setTransactionEvent($Kontaktereignis);
                            }
                            break;
                         case 'Marketing Program':
                            if(!empty($mioEvent->marketingAutomation)){
                                $marketingAutomation['apiKey'] = $this->settingsService->getSettings();
                                $marketingAutomation['programId'] = (int)$mioEvent->marketingAutomation;
                                $marketingAutomation['email'] = $result['email'];
                                $resultNewOrder['marketingAutomation'] = $this->synchronizationService->setMarketingAutomation($marketingAutomation);
                            }
                            break;

                        default:
                            break;
                    }
                }
            }
        }
        return json_encode($resultNewOrder);
        }
    }


   /**
     * @param EventProceduresTriggered $event
     * @return mixed
     */
    public function repair(EventProceduresTriggered $event)
    {
           // getEvent
        $mioEvents ='';
        $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[12] Reparatur');
        if(count($mioEventsArray) > 0){
           $mioEvents = $mioEventsArray;
        }else{
           $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[12] Repair');
           $mioEvents = $mioEventsArray;
        }
        if(!empty($mioEvents) && count($mioEvents) > 0){
        /** @var Order $order */
        $order = $event->getOrder();
         $contactId = -1;
         $orderItems = $order->orderItems;
        foreach ($orderItems as $orderItem) {
            if($orderItem->typeId == 1){
                $id = $orderItem->id;
                $orderItemName = $orderItem->orderItemName;
                $quantity = $orderItem->quantity;
                $amounts = $orderItem->amounts;
                foreach ($amounts as $amount) {
                    $currency = $amount->currency;
                    $priceGross = $amount->priceGross;
                }
                $item[$id] = array(
                    'id' => $id,
                    'orderItemName' => $orderItemName,
                    'quantity' => $quantity,
                    'currency' => $currency,
                    'priceGross' => $priceGross
                );
            }
        }
        $result['item'] = $item;
        $amountsSummen = $order->amounts;
        foreach ($amountsSummen as $amountsSumme) {
            $result['netTotal'] = $amountsSumme->netTotal;
            $result['grossTotal'] = $amountsSumme->grossTotal;
        }
        /** @var OrderRelationReference[] $r */
        $relations = $order->relations;
        /** @var OrderRelationReference $a */
        foreach($relations as $relation)
        {
            if($relation->relation == 'receiver')
            {
                $contactId = $relation->referenceId;
                $contact = $this->accountService->findContactById($contactId);
                $result['firstName'] = $contact['firstName'];
                $result['lastName'] = $contact['lastName'];
                $result['email'] = $contact['email'];
                $result['gender'] = $contact['gender'];
                $result['contactId'] = $contactId;
                #$result['birthday'] = $contact['birthdayAt'];
                $result['orderId'] = $relation->orderId;
            }
            if(isset($result) && !empty($result)){
                foreach ($mioEvents as $mioEvent) {
                    $integration = $mioEvent->integration;
                    switch ($integration) {
                        case 'Kontaktereignis':
                            if(!empty($mioEvent->kontaktereignisID)){
                                $Kontaktereignis['apiKey'] = $this->settingsService->getSettings();
                                $Kontaktereignis['kontaktereignisID'] = (int)$mioEvent->kontaktereignisID;
                                $Kontaktereignis['maileon'] = $result;
                                $resultNewOrder['Kontaktereignis'] = $this->synchronizationService->setTransactionEvent($Kontaktereignis);
                            }
                            break;
                         case 'Marketing Program':
                            if(!empty($mioEvent->marketingAutomation)){
                                $marketingAutomation['apiKey'] = $this->settingsService->getSettings();
                                $marketingAutomation['programId'] = (int)$mioEvent->marketingAutomation;
                                $marketingAutomation['email'] = $result['email'];
                                $resultNewOrder['marketingAutomation'] = $this->synchronizationService->setMarketingAutomation($marketingAutomation);
                            }
                            break;

                        default:
                            break;
                    }
                }
            }
        }
        return json_encode($resultNewOrder);
        }
    }

           /**
     * @param EventProceduresTriggered $event
     * @return mixed
     */
    public function collectionOrder(EventProceduresTriggered $event)
    {
           // getEvent
        $mioEvents ='';
        $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[13] Sammelauftrag');
        if(count($mioEventsArray) > 0){
           $mioEvents = $mioEventsArray;
        }else{
           $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[13] Collection Order');
           $mioEvents = $mioEventsArray;
        }
        if(!empty($mioEvents) && count($mioEvents) > 0){
        /** @var Order $order */
        $order = $event->getOrder();
         $contactId = -1;
         $orderItems = $order->orderItems;
        foreach ($orderItems as $orderItem) {
            if($orderItem->typeId == 1){
                $id = $orderItem->id;
                $orderItemName = $orderItem->orderItemName;
                $quantity = $orderItem->quantity;
                $amounts = $orderItem->amounts;
                foreach ($amounts as $amount) {
                    $currency = $amount->currency;
                    $priceGross = $amount->priceGross;
                }
                $item[$id] = array(
                    'id' => $id,
                    'orderItemName' => $orderItemName,
                    'quantity' => $quantity,
                    'currency' => $currency,
                    'priceGross' => $priceGross
                );
            }
        }
        $result['item'] = $item;
        $amountsSummen = $order->amounts;
        foreach ($amountsSummen as $amountsSumme) {
            $result['netTotal'] = $amountsSumme->netTotal;
            $result['grossTotal'] = $amountsSumme->grossTotal;
        }
        /** @var OrderRelationReference[] $r */
        $relations = $order->relations;
        /** @var OrderRelationReference $a */
        foreach($relations as $relation)
        {
            if($relation->relation == 'receiver')
            {
                $contactId = $relation->referenceId;
                $contact = $this->accountService->findContactById($contactId);
                $result['firstName'] = $contact['firstName'];
                $result['lastName'] = $contact['lastName'];
                $result['email'] = $contact['email'];
                $result['gender'] = $contact['gender'];
                $result['contactId'] = $contactId;
                #$result['birthday'] = $contact['birthdayAt'];
                $result['orderId'] = $relation->orderId;
            }
            if(isset($result) && !empty($result)){
                foreach ($mioEvents as $mioEvent) {
                    $integration = $mioEvent->integration;
                    switch ($integration) {
                        case 'Kontaktereignis':
                            if(!empty($mioEvent->kontaktereignisID)){
                                $Kontaktereignis['apiKey'] = $this->settingsService->getSettings();
                                $Kontaktereignis['kontaktereignisID'] = (int)$mioEvent->kontaktereignisID;
                                $Kontaktereignis['maileon'] = $result;
                                $resultNewOrder['Kontaktereignis'] = $this->synchronizationService->setTransactionEvent($Kontaktereignis);
                            }
                            break;
                         case 'Marketing Program':
                            if(!empty($mioEvent->marketingAutomation)){
                                $marketingAutomation['apiKey'] = $this->settingsService->getSettings();
                                $marketingAutomation['programId'] = (int)$mioEvent->marketingAutomation;
                                $marketingAutomation['email'] = $result['email'];
                                $resultNewOrder['marketingAutomation'] = $this->synchronizationService->setMarketingAutomation($marketingAutomation);
                            }
                            break;

                        default:
                            break;
                    }
                }
            }
        }
        return json_encode($resultNewOrder);
        }
    }


   /**
     * @param EventProceduresTriggered $event
     * @return mixed
     */
    public function collectionOrderCreditNote(EventProceduresTriggered $event)
    {
           // getEvent
        $mioEvents ='';
        $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[14] Sammelgutschrift');
        if(count($mioEventsArray) > 0){
           $mioEvents = $mioEventsArray;
        }else{
           $mioEventsArray = $this->settingsService->getEventByTyp('MIO-[14] Collection Order credit note');
           $mioEvents = $mioEventsArray;
        }
        if(!empty($mioEvents) && count($mioEvents) > 0){
        /** @var Order $order */
        $order = $event->getOrder();
         $contactId = -1;
         $orderItems = $order->orderItems;
        foreach ($orderItems as $orderItem) {
            if($orderItem->typeId == 1){
                $id = $orderItem->id;
                $orderItemName = $orderItem->orderItemName;
                $quantity = $orderItem->quantity;
                $amounts = $orderItem->amounts;
                foreach ($amounts as $amount) {
                    $currency = $amount->currency;
                    $priceGross = $amount->priceGross;
                }
                $item[$id] = array(
                    'id' => $id,
                    'orderItemName' => $orderItemName,
                    'quantity' => $quantity,
                    'currency' => $currency,
                    'priceGross' => $priceGross
                );
            }
        }
        $result['item'] = $item;
        $amountsSummen = $order->amounts;
        foreach ($amountsSummen as $amountsSumme) {
            $result['netTotal'] = $amountsSumme->netTotal;
            $result['grossTotal'] = $amountsSumme->grossTotal;
        }
        /** @var OrderRelationReference[] $r */
        $relations = $order->relations;
        /** @var OrderRelationReference $a */
        foreach($relations as $relation)
        {
            if($relation->relation == 'receiver')
            {
                $contactId = $relation->referenceId;
                $contact = $this->accountService->findContactById($contactId);
                $result['firstName'] = $contact['firstName'];
                $result['lastName'] = $contact['lastName'];
                $result['email'] = $contact['email'];
                $result['gender'] = $contact['gender'];
                $result['contactId'] = $contactId;
                #$result['birthday'] = $contact['birthdayAt'];
                $result['orderId'] = $relation->orderId;
            }
            if(isset($result) && !empty($result)){
                foreach ($mioEvents as $mioEvent) {
                    $integration = $mioEvent->integration;
                    switch ($integration) {
                        case 'Kontaktereignis':
                            if(!empty($mioEvent->kontaktereignisID)){
                                $Kontaktereignis['apiKey'] = $this->settingsService->getSettings();
                                $Kontaktereignis['kontaktereignisID'] = (int)$mioEvent->kontaktereignisID;
                                $Kontaktereignis['maileon'] = $result;
                                $resultNewOrder['Kontaktereignis'] = $this->synchronizationService->setTransactionEvent($Kontaktereignis);
                            }
                            break;
                         case 'Marketing Program':
                            if(!empty($mioEvent->marketingAutomation)){
                                $marketingAutomation['apiKey'] = $this->settingsService->getSettings();
                                $marketingAutomation['programId'] = (int)$mioEvent->marketingAutomation;
                                $marketingAutomation['email'] = $result['email'];
                                $resultNewOrder['marketingAutomation'] = $this->synchronizationService->setMarketingAutomation($marketingAutomation);
                            }
                            break;

                        default:
                            break;
                    }
                }
            }
        }
        return json_encode($resultNewOrder);
        }
    }

}
