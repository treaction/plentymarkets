<?php
namespace MailInOne\Models\Database;

use Plenty\Modules\Plugin\DataBase\Contracts\Model;

/**
 * Class Synchronisation
 *
 * @property int $id
 * @property string $modus
 * @property int $quantity
 * @property string $createdAt
 */
class Synchronisation extends Model
{
    public $id = 0;
    public $modus = '';
    public $quantity = 50;
    public $createdAt = '';

    /**
     * @return string
     */
    public function getTableName():string
    {
        return 'MailInOne::synchronisation';
    }
}