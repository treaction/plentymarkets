<?php
namespace MailInOne\Models\Database;

use Plenty\Modules\Plugin\DataBase\Contracts\Model;

/**
 * Class LogSync
 *
 * @property int $id
 * @property string $date
 * @property int $countContact
 * @property int $event
 * @property int $marketingAutomation
 * @property int $doi
 */
class LogSync extends Model
{
    public $id = 0;
    public $date = '';
    public $countContact = 0;
    public $event = 0;
    public $marketingAutomation = 0;
    public $doi = 0;

    /**
     * @return string
     */
    public function getTableName():string
    {
        return 'MailInOne::logsync';
    }
}