<?php
namespace MailInOne\Models\Database;

use Plenty\Modules\Plugin\DataBase\Contracts\Model;

/**
 * Class ContactEvent
 *
 * @property int $id
 * @property string $event
 * @property string $ordner
 * @property string $integration
 * @property string $kontaktereignis
 * @property int $kontaktereignisID
 * @property string $marketingAutomation
 * @property string $doiKey
 * @property string $doiName
 * @property int $isDoi
 * @property string $createdAt
 */
class ContactEvent extends Model
{
    public $id = 0;
    public $event = '';
    public $ordner = '';
    public $integration = 0;
    public $kontaktereignis = '';
    public $kontaktereignisID = 0;
    public $marketingAutomation = '';
    public $doiKey = '';
    public $doiName = '';
    public $isDoi;
    public $createdAt = '';

    /**
     * @return string
     */
    public function getTableName():string
    {
        return 'MailInOne::contactevent';
    }
}