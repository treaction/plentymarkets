<?php
namespace MailInOne\Models\Database;

use Plenty\Modules\Plugin\DataBase\Contracts\Model;

/**
 * Class OrderEvent
 *
 * @property int $id
 * @property float $statusId
 * @property string $event
 * @property string $name
 * @property string $mioEvent
 * @property string $integration
 * @property string $kontaktereignis
 * @property int $kontaktereignisID
 * @property string $marketingAutomation
 * @property int $isActiv
 * @property string $createdAt
 */
class OrderEvent extends Model
{
    public $id = 0;
    public $statusId = 0;
    public $name = '';
    public $event = '';
    public $mioEvent = '';
    public $integration = 0;
    public $kontaktereignis = '';
    public $kontaktereignisID = 0;
    public $marketingAutomation = '';
    public $isActiv = 0;
    public $createdAt = '';

    /**
     * @return string
     */
    public function getTableName():string
    {
        return 'MailInOne::oderEvent';
    }
}