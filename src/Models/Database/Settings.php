<?php
namespace MailInOne\Models\Database;

use Plenty\Modules\Plugin\DataBase\Contracts\Model;

/**
 * Class Settings
 *
 * @property int $id
 * @property string $apiKey
 * @property string $createdAt
 */
class Settings extends Model
{
    public $id = 0;
    public $apiKey = '';
    public $createdAt = '';

    /**
     * @return string
     */
    public function getTableName():string
    {
        return 'MailInOne::settings';
    }
}