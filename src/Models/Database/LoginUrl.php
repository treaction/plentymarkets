<?php
namespace MailInOne\Models\Database;

use Plenty\Modules\Plugin\DataBase\Contracts\Model;

/**
 * Class LoginUrl
 *
 * @property int $id
 * @property string $loginurl
 * @property string $createdAt
 */
class LoginUrl extends Model
{
    public $id = 0;
    public $loginurl = '';
    public $createdAt = '';

    /**
     * @return string
     */
    public function getTableName():string
    {
        return 'MailInOne::loginurl';
    }
}