<?php
namespace MailInOne\Models\Database;

use Plenty\Modules\Plugin\DataBase\Contracts\Model;

/**
 * Class Cronjob
 *
 * @property int $id
 * @property int $cronjob
 * @property string $createdAt
 */
class Cronjob extends Model
{
    public $id = 0;
    public $cronjob = 5;
    public $createdAt = '';

    /**
     * @return string
     */
    public function getTableName():string
    {
        return 'MailInOne::cronjob';
    }
}