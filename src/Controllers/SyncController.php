<?php

namespace MailInOne\Controllers;

use Plenty\Plugin\Http\Request;
use Plenty\Plugin\Controller;
use MailInOne\Services\SynchronizationService;
use MailInOne\Services\AccountService; // plenty interface
use MailInOne\Services\Database\SettingsService;
use MailInOne\Helper\SyncHelper;
use Plenty\Plugin\Log\Loggable;

/**
 * Class SyncController
 * version    3.0.2
 * @package MailInOne\Controllers
 */
class SyncController extends Controller {

    use Loggable;

    /**
     * @internal MailInOne\Services\SynchronizationService Mail-In-One-Interface
     * @var MailInOne\Services\SynchronizationService
     */
    private $synchronizationService;

    /**
     * @internal MailInOne\Services\AccountService Plugin specific Plenty-Interface Collection. Methods wrap Plenty's standard laravel-models
     * @var MailInOne\Services\AccountService
     */
    private $accountService;

    /**
     * @internal MailInOne\Services\Database\SettingsService Mail-In-One Connections
     * @var MailInOne\Services\Database\SettingsService
     */
    private $settingsService;


    /**
     *
     * @var MailInOne\Helper\SyncHelper $sync_utils
     */
    private $sync_utils;


    /**
     * SyncController constructor.
     *
     * @param SettingsService $settingsService
     * @param SynchronizationService $synchronizationService
     * @param AccountService $accountService
     */
    public function __construct(
        SynchronizationService $synchronizationService,
        AccountService $accountService,
        SettingsService $settingsService
    ){
        // used in get* methods
        $this->synchronizationService = $synchronizationService;
        $this->accountService = $accountService;
        $this->settingsService = $settingsService;

        // used in method setSync
        $this->sync_utils = pluginApp(SyncHelper::class);
    }

    /**
     * not used within this class
     * not used in outer code
     * @return array
     */
    public function getCampaign() {
        $this->sync_utils->writeLogToMessageContainer('INFO',__METHOD__.'|'.__LINE__.'| entered METHOD');
        $apiKey['apiKey'] = $this->settingsService->getSettings();
        $result = $this->synchronizationService->getCampaign($apiKey);

        return json_encode($result);
    }

    /**
     * not used within this class
     * not used in outer code
     * @return array
     */
    public function getTargetGroup() {
        $this->sync_utils->writeLogToMessageContainer('INFO',__METHOD__.'|'.__LINE__.'| entered METHOD');
        $apiKey['apiKey'] = $this->settingsService->getSettings();
        $result = $this->synchronizationService->getTargetGroup($apiKey);
        return $result;
    }

    /**
     * not used within this class
     * not used in outer code
     * @return array
     */
    public function getContactFilter() {
        $this->sync_utils->writeLogToMessageContainer('INFO',__METHOD__.'|'.__LINE__.'| entered METHOD');
        $apiKey['apiKey'] = $this->settingsService->getSettings();
        $result = $this->synchronizationService->getContactFilter($apiKey);
        return $result;
    }

    /**
     * not used within this class
     * not used in outer code
     */
    public function getSyncInfo() {
        $this->sync_utils->writeLogToMessageContainer('INFO',__METHOD__.'|'.__LINE__.'| entered METHOD');
        $result['log'] = $this->settingsService->getSyncInfo();

        return $result;
    }

    /**
     * not used within this class
     * not referenced by outer code
     */
    public function setEvent(Request $request) {
        

        // START REFACTORED but TO BE DEBUGGED
        return $this->sync_utils->mioRunDataSyncWithTriggersForContactEventMaDoi();
        // END REFACTORED but TO BE DEBUGGED

        /* @comment: 3.0.1 algorithm */
        // $this->sync_utils->writeLogToMessageContainer('INFO',__METHOD__.'|'.__LINE__.'| entered METHOD');
 /*       
        $log_stats = [];
        $result = [];

        $foldres = $this->accountService->listAllFolders();

        foreach ($foldres as $folder) {
            $foldernames = $this->accountService->validationCharacter($folder['name']);
            $resultFolders = str_replace(' ', '_', $foldernames);

            $results['apiKey'] = $this->settingsService->getSettings();
            $results['folder'] = $resultFolders;
            $this->synchronizationService->deleteCustomField($results);
        }

        $foldersIds = array();
        $folders = $this->accountService->listAllFolders();

        foreach ($folders as $folder) {

            $foldersIds[$folder["id"]] = $folder["id"];
            $foldername = $this->accountService->validationCharacter($folder["name"]);
            $result['folderName'] = str_replace(' ', '_', $foldername);

            $contactResult['apiKey'] = $this->settingsService->getSettings();
            $contactResult['maileon'] = $result;
            $this->synchronizationService->createCustomField($contactResult);
        }

        // add Field plentyID
        $result['folderName'] = 'plentyID';
        $contactResult['apiKey'] = $this->settingsService->getSettings();
        $contactResult['maileon'] = $result;

        $this->synchronizationService->createCustomField($contactResult);
        $result['folderName'] = 'lastOrderAt';
        $contactResult['apiKey'] = $this->settingsService->getSettings();
        $contactResult['maileon'] = $result;
        $this->synchronizationService->createCustomField($contactResult);
        
        $lastSync = $this->settingsService->getLastSync();
        $result['lastSync'] = $lastSync;
        
        $contacts = $this->accountService->listAllRecipients();
        $countContact = 0;
        $countEvent = 0;
        $countMa = 0;
        $countDoi = 0;
        $newContactWithOutId = 0;
        $updateContactWithId = 0;
        $newContactWithId = 0;

        foreach ($contacts as $contact) {

            if (!empty($contact["contactId"]) && $contact["contactId"] != 0) { // CRM
                
                $contactId = $this->accountService->getContactIdByEmail($contact['email']);
                
                if (!is_null($contactId)) {
                    $contactPlenty = $this->accountService->findContactById($contactId);
                    $result['plentyID'] = $contactPlenty["plentyId"];
                    $result['lastOrderAt'] = $contactPlenty["lastOrderAt"];
                    $result['updatedAt'] = $contactPlenty["updatedAt"];
                    $result['createdAt'] = $contactPlenty["createdAt"];                    
                    $result['contactId'] = $contact["contactId"];
                    $result['contactPlenty'] = $contactPlenty;    
                }
                
                $log_stats['CONTACT'][] = implode( "; ", [__LINE__, $contact["email"], $contact["id"], $contact["contactId"] ]);
            } else { // NL
                $result['email'] = $contact["email"];
                $result['firstName'] = $contact["firstName"];
                $result['lastName'] = $contact["lastName"];
                $result['contactId'] = $contact["contactId"];
                $result['gender'] = $contact["gender"];
                $result['birthday'] = $contact["birthday"];
                $result['timestamp'] = $contact["timestamp"];
                $log_stats['CONTACT'][] = implode( "; ", [__LINE__, $contact["email"], $contact["id"], 'leer' ]);
            }
            
            

            if (empty($contact["contactId"])) { // NL

                $log_stats['NL'][] = implode( "; ", [__LINE__, $contact["timestamp"], $lastSync, $contact["email"] ]);
                
                if (strtotime($contact["timestamp"]) >= strtotime($lastSync)) {

                    // The timestamp when the recipient confirmed the newsletter subscription
                    // if (isset($contact["confirmedTimestamp"]) && ! empty($contact["confirmedTimestamp"])) {

                    if (array_key_exists($contact['folderId'], $foldersIds)) {
                        $folderContact = $this->accountService->listFolderById($contact['folderId']);
                        $folderArray = $folderContact->toArray();
                        $foldername = $this->accountService->validationCharacter($folderArray['name']);
                        $result['folderName'] = str_replace(' ', '_', $foldername);
                    } else {
                        $result['folderName'] = '';
                    }


                    switch ($contact["gender"]) {
                        case 'm':
                            $result['SALUTATION'] = 'Herr';
                            break;
                        case 'f':
                            $result['SALUTATION'] = 'Frau';
                            break;
                        default:
                            $result['SALUTATION'] = '';
                            break;
                    }

                    $contactResult['apiKey'] = $this->settingsService->getSettings();
                    $contactResult['maileon'] = $result;

                    $settingsEventArray = $this->settingsService->getEvent();
                    if (is_bool($settingsEventArray) && !$settingsEventArray) {
                        $this->settingsService->saveEventInit();
                        $settingsEventArray = $this->settingsService->getEvent();
                    }
                    foreach ($settingsEventArray as $settingsEvent) {

                        $id = $settingsEvent->id;
                        $event = $settingsEvent->event;
                        $ordner = $settingsEvent->ordner;
                        $integration = $settingsEvent->integration;
                        $kontaktereignis = $settingsEvent->kontaktereignis;
                        $kontaktereignisID = $settingsEvent->kontaktereignisID;
                        $marketingAutomation = $settingsEvent->marketingAutomation;
                        $doiKey = $settingsEvent->doiKey;
                        $isDoi = $settingsEvent->isDoi;

                        if (($event === 'Neuer Kontakt') && $ordner === $result['folderName']) {
                            //Sync
                            $contactResult['doi'] = $isDoi;
                            $syncContactsWithOutId[$newContactWithOutId] = $contactResult;
                            $countContact++;
                            $newContactWithOutId++;

                            //DOI
                            if (($isDoi) && (!empty($doiKey)) && (($integration === 'DOI senden'))) {
                                //DOI Mailing auslösen
                                $doi['apiKey'] = $this->settingsService->getSettings();
                                $doi['doiKey'] = $doiKey;
                                $doi['maileon'] = $result;
                                $resultt['doi'] = $this->synchronizationService->setDoi($doi);
                                $countDoi++;
                            }
                            //Kontaktereignis
                            if (($integration === 'Kontaktereignis') && (!empty($kontaktereignisID))) {
                                $ker['apiKey'] = $this->settingsService->getSettings();
                                $ker['kontaktereignisID'] = $kontaktereignisID;
                                $ker['maileon'] = $result;
                                $resultt['kr'] = $this->synchronizationService->setTransaction($ker);
                                $countEvent++;
                            }
                            //Marketing Program
                            if (($integration === 'Marketing Program') && (!empty($marketingAutomation))) {
                                $maResult['apiKey'] = $this->settingsService->getSettings();
                                $maResult['programId'] = $marketingAutomation;
                                $maResult['email'] = $contact["email"];
                                $resultt['maNL'] = $this->synchronizationService->setMarketingAutomation($maResult);
                                $countMa++;
                            }
                        }
                    }
                }
            } else { // CRM
                
                $log_stats['CRM'][] = implode( "; ", [__LINE__, $result['updatedAt'], $lastSync, $contact["email"] ]);
                
                $crm_is_updated_contact = $this->sync_utils->helperIsUpdatedContact( $result['createdAt'],  $result['updatedAt']);
                $crm_is_new_contact     = $this->sync_utils->helperIsNewContact($result['createdAt'],  $result['updatedAt']);
                
                
                if ( !empty($result['updatedAt']) && (strtotime($result["updatedAt"]) >= strtotime($lastSync))) {
                    // The timestamp when the recipient confirmed the newsletter subscription
                    // if (isset($contact["confirmedTimestamp"]) && ! empty($contact["confirmedTimestamp"])) {

                    if (array_key_exists($contact['folderId'], $foldersIds)) {
                        $folderContact = $this->accountService->listFolderById($contact['folderId']);
                        $folderArray = $folderContact->toArray();
                        $foldername = $this->accountService->validationCharacter($folderArray['name']);
                        $result['folderName'] = str_replace(' ', '_', $foldername);
                    } else {
                        $result['folderName'] = '';
                    }
                    switch ($contact["gender"]) {
                        case 'm':
                            $result['SALUTATION'] = 'Herr';
                            break;
                        case 'f':
                            $result['SALUTATION'] = 'Frau';
                            break;
                        default:
                            $result['SALUTATION'] = '';
                            break;
                    }
                    $contactResult['apiKey'] = $this->settingsService->getSettings();
                    $contactResult['maileon'] = $result;

                    //Settings
                    $settingsEventArray = $this->settingsService->getEvent();
                    if (is_bool($settingsEventArray) && !$settingsEventArray) {
                        $this->settingsService->saveEventInit();
                        $settingsEventArray = $this->settingsService->getEvent();
                    }
                    foreach ($settingsEventArray as $settingsEvent) {
                        $id = $settingsEvent->id;
                        $event = $settingsEvent->event;
                        $ordner = $settingsEvent->ordner;
                        $integration = $settingsEvent->integration;
                        $kontaktereignis = $settingsEvent->kontaktereignis;
                        $kontaktereignisID = $settingsEvent->kontaktereignisID;
                        $marketingAutomation = $settingsEvent->marketingAutomation;
                        $doiKey = $settingsEvent->doiKey;
                        $isDoi = $settingsEvent->isDoi;
                        
                        
                        //Update
                        if ($crm_is_updated_contact && ($event === 'aktualisierte Kontakt') && ($ordner === $result['folderName'])) {
                            
                            //Sync
                            $contactResult['doi'] = $isDoi;
                            $syncUpdateContactsWithId[$updateContactWithId] = $contactResult;
                            $countContact++;
                            $updateContactWithId++;

                            //DOI
                            if (($isDoi) && (!empty($doiKey)) && (($integration === 'DOI senden'))) {
                                //DOI Mailing auslösen
                                $doi['apiKey'] = $this->settingsService->getSettings();
                                $doi['doiKey'] = $doiKey;
                                $doi['maileon'] = $result;
                                $resultt['doi'] = $this->synchronizationService->setDoi($doi);
                                $countDoi++;
                            }
                            //Kontaktereignis
                            if (($integration === 'Kontaktereignis') && (!empty($kontaktereignisID))) {
                                $ker['apiKey'] = $this->settingsService->getSettings();
                                $ker['kontaktereignisID'] = $kontaktereignisID;
                                $ker['maileon'] = $result;
                                $resultt['kr'] = $this->synchronizationService->setTransaction($ker);
                                $countEvent++;
                            }
                            //Marketing Program
                            if (($integration === 'Marketing Program') && (!empty($marketingAutomation))) {
                                $maResult['apiKey'] = $this->settingsService->getSettings();
                                $maResult['programId'] = $marketingAutomation;
                                $maResult['email'] = $contact["email"];
                                $resultt['maCRMUpdate'] = $this->synchronizationService->setMarketingAutomation($maResult);
                                $countMa++;
                            }

                        } elseif ( $crm_is_new_contact && ($event === 'Neuer Kontakt') && $ordner === $result['folderName']) {
                            //Sync
                            $contactResult['doi'] = $isDoi;
                            $syncNewContactsWithId[$newContactWithId] = $contactResult;
                            $countContact++;
                            $newContactWithId++;

                            //DOI
                            if (($isDoi) && (!empty($doiKey)) && (($integration === 'DOI senden'))) {
                                //DOI Mailing auslösen
                                $doi['apiKey'] = $this->settingsService->getSettings();
                                $doi['doiKey'] = $doiKey;
                                $doi['maileon'] = $result;
                                $resultt['doi'] = $this->synchronizationService->setDoi($doi);
                                $countDoi++;
                            }
                            //Kontaktereignis
                            if (($integration === 'Kontaktereignis') && (!empty($kontaktereignisID))) {
                                $ker['apiKey'] = $this->settingsService->getSettings();
                                $ker['kontaktereignisID'] = $kontaktereignisID;
                                $ker['maileon'] = $result;
                                $resultt['kr'] = $this->synchronizationService->setTransaction($ker);
                                $countEvent++;
                            }
                            //Marketing Program
                            if (($integration === 'Marketing Program') && (!empty($marketingAutomation))) {
                                $maResult['apiKey'] = $this->settingsService->getSettings();
                                $maResult['programId'] = $marketingAutomation;
                                $maResult['email'] = $contact["email"];
                                $resultt['maCRMNew'] = $this->synchronizationService->setMarketingAutomation($maResult);
                                $countMa++;
                            }
                        }
                        $resultArray[$id] = $resultt;
                    }
                }
            }
        }
        
        
        $loop=0;
        $temp=[];
        foreach( $log_stats['CRM'] as $hd ) {
            if( $loop < 50 ) {
                $temp[] = $hd;
                $loop++;
            } else {
                $this->getLogger('SyncController_SetEventCRM')
                 ->info('MailInOne::plentymiosync.execLog', ['daten' =>  $temp ]);
                $loop = 0;
                $temp=[];
            }
        }
        $this->getLogger('SyncController_SetEventCRM')
                 ->info('MailInOne::plentymiosync.execLog', ['daten' =>  $temp ]);        
        
        
        $loop=0;
        $temp=[];
        foreach( $log_stats['NL'] as $hd ) {
            if( $loop < 50 ) {
                $temp[] = $hd;
                $loop++;
            } else {
                $this->getLogger('SyncController_SetEventNL')
                 ->info('MailInOne::plentymiosync.execLog', ['daten' =>  $temp ]);
                $loop = 0;
                $temp=[];
            }
        }          
        $this->getLogger('SyncController_SetEventNL')
                 ->info('MailInOne::plentymiosync.execLog', ['daten' =>  $temp ]);

        // $log_stats['CONTACT']
        $loop=0;
        $temp=[];
        foreach( $log_stats['CONTACT'] as $hd ) {
            if( $loop < 100 ) {
                $temp[] = $hd;
                $loop++;
            } else {
                $this->getLogger('SyncController_SetEventCONT')
                 ->info('MailInOne::plentymiosync.execLog', ['daten' =>  $temp ]);
                $loop = 0;
                $temp=[];
            }
        }
        $this->getLogger('SyncController_SetEventCONT')
             ->info('MailInOne::plentymiosync.execLog', ['daten' =>  $temp ]);
        
             
        $apiKey = $this->settingsService->getSettings();

        $syncAllContact['apiKey'] = $apiKey;
        $syncAllContact['syncContactsWithOutId'] = $syncContactsWithOutId;
        $syncAllContact['syncUpdateContactsWithId'] = $syncUpdateContactsWithId;
        $syncAllContact['syncNewContactsWithId'] = $syncNewContactsWithId;

        $resultArray['syncAllContact'] = $this->synchronizationService->syncContacts($syncAllContact);
        $this->settingsService->saveLogSync($countContact, $countDoi, $countEvent, $countMa);
        return json_encode($resultArray);
*/
    }

    /**
     * not used within this class
     * not referenced by outer code
     */
    public function getEvent() {
        $this->sync_utils->writeLogToMessageContainer('INFO',__METHOD__.'|'.__LINE__.'| entered METHOD');
        $result = array();

        $result['DB'] = $this->settingsService->getEvent();

        $apiKey['apiKey'] = $this->settingsService->getSettings();
        $result['conatctEvent'] = $this->synchronizationService->getConatctEvent($apiKey);
        $result['doiMailings'] = $this->synchronizationService->getDoiMailings($apiKey);
        $result['folders'] = $this->accountService->listAllFolders();

        return $result;
    }

    /**
     * not used within this class
     * not referenced by outer code
     */
    public function getAllStatusOrder() {
        $this->sync_utils->writeLogToMessageContainer('INFO',__METHOD__.'|'.__LINE__.'| entered METHOD');
        $result = $this->accountService->getAllStatusOrder();

        return json_encode($result);
    }

    /**
     * not used within this class
     * not referenced by outer code
     */
    public function getAllOrderEvent() {
        $this->sync_utils->writeLogToMessageContainer('INFO',__METHOD__.'|'.__LINE__.'| entered METHOD');
        $result['DB'] = $this->settingsService->getAllOrderEvent();

        $apiKey['apiKey'] = $this->settingsService->getSettings();
        $result['conatctEvent'] = $this->synchronizationService->getConatctEvent($apiKey);
        $result['statusOrder'] = $this->accountService->getAllStatusOrder();

        return json_encode($result);
    }


    /**
     * MAIN METHOD OF SyncController::class
     * @internal method is run on user interaction. Updates MIO custom fields by plenty folders and runs an inital data synchronisation plenty to MIO
     * @author jsr|treaction ag
     * @version since 3.0.2 (code of V3.0.1 replaced)
     * @param Request $request
     * @return type
     */
    public function setSync(Request $request) {


        $this->getLogger('SyncController_setSync_START')
                ->info('MailInOne::plentymiosync.execLog', [ 'Starttime' => date('Y-m-d H:i:s') ]);

        try {
            $this->sync_utils->mioDeleteCustomFieldsInMioByMioCustomFields();
            $this->sync_utils->mioCreateStandardCustomField('ID'); // former: "plenty_ID", now a prefix ("plenty_") is added to each custom field
            $this->sync_utils->mioCreateStandardCustomField('lastOrderAt'); // plenty_lastOrderAt
            $this->sync_utils->mioCreateCustomFieldsFromPlentyFoldernames();

            return $this->sync_utils->mioRunDataSynchronisationInitial();
        } catch (\Exception $exc) {
            $this->getLogger('SyncController_setSync')
                ->error('MailInOne::plentymiosync.execLog', [ 'tracelog' => $exc->getTraceAsString() ]);
        }
    }
}
