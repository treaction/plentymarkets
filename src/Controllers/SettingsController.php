<?php

namespace MailInOne\Controllers;

use MailInOne\Services\Database\SettingsService;
use Plenty\Plugin\Controller;
use Plenty\Plugin\Http\Request;

class SettingsController extends Controller {

    /**
     *
     * @var SettingsService
     */
    private $settingsService;

    /**
     * SettingsController constructor.
     *
     * @param SettingsService $settingsService
     */
    public function __construct(SettingsService $settingsService) {
        $this->settingsService = $settingsService;
    }

    /**
     *
     * @param Request $request
     * @return string
     */
    public function saveSettings(Request $request) {
        $settings = $request->get('settings');

        if ($settings) {
            if ($this->settingsService->saveSetting($settings)) {
                return $this->getSettings();
            }
        }
    }

    /**
     *
     * @param Request $request
     * @return string
     */
    public function deleteEvent(Request $request) {
        $eventId = $request->get('eventId');

        if ($eventId) {
            $res = $this->settingsService->deleteEvent($eventId);
            return $res;
        }
    }

    /**
     *
     * @param Request $request
     * @return string
     */
    public function saveEvent(Request $request) {
        $event = $request->get('event');

        if ($event) {
            if ($this->settingsService->saveEvent($event)) {
                return $event;
            }
        }
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function saveEventEdit(Request $request) {
        $event = $request->get('event');
        $id = $event['id'];
        if ($event) {
            $result = $this->settingsService->saveEventEdit($id, $event);
        }
        return json_encode($result);
    }

    /**
     *
     * @param Request $request
     * @return string
     */
    public function sendmails(Request $request) {
        $account = $request->get('account');
        if ($account) {
            $result = $this->settingsService->sendmails($account);
        }
        return json_encode($result);
    }

    public function sendLog($log_data) {
        $result = $this->settingsService->sendLog($log_data);
        return $result;
    }

    public function getSettings() {
        $result = $this->settingsService->getSettings();

        return json_encode($result);
    }

    public function getLoginUrl() {
        $result = $this->settingsService->getLoginUrl();

        return json_encode($result);
    }

    public function getCronjob() {
        $result = $this->settingsService->getCronjob();

        return json_encode($result);
    }

    public function resetSync() {
        $result = $this->settingsService->resetLogSync();
        return json_encode($result);
    }

    public function getSynchronisation() {
        $result['syncOption'] = $this->settingsService->getSynchronisation();

        return json_encode($result);
    }

    /**
     *
     * @param Request $request
     * @return string
     */
    public function deleteOrderEvent(Request $request) {
        $eventId = $request->get('orderEventId');

        if ($eventId) {
            $res = $this->settingsService->deleteOrderEvent($eventId);
            return $res;
        }
    }

    /**
     *
     * @param Request $request
     * @return string
     */
    public function saveOrderEvent(Request $request) {
        $orderEvent = $request->get('orderevent');

        if ($orderEvent) {
            if ($this->settingsService->saveOrderEvent($orderEvent)) {
                return $orderEvent;
            }
        }
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function saveOrderEventEdit(Request $request) {
        $orderEvent = $request->get('orderevent');
        $id = $orderEvent['id'];
        if ($orderEvent) {
            $result = $this->settingsService->saveOrderEventEdit($id, $orderEvent);
        }
        return json_encode($result);
    }

}
