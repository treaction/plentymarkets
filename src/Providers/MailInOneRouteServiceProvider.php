<?php
namespace MailInOne\Providers;

use Plenty\Plugin\RouteServiceProvider;
use Plenty\Plugin\Routing\ApiRouter;

/**
 * Class HMailInOneRouteServiceProvider
 *
 * @package MailInOne\Providers
 */
class MailInOneRouteServiceProvider extends RouteServiceProvider
{

    /**
     *
     * @param ApiRouter $apiRouter
     */
    public function map(ApiRouter $apiRouter)
    {
        $apiRouter->version([
            'v1'
        ], [
            'namespace' => 'MailInOne\Controllers',
            'middleware' => 'oauth'
        ], function ($apiRouter) {
            $apiRouter->post('mailInOne/settings/', 'SettingsController@saveSettings');
            $apiRouter->post('mailInOne/saveEvent/', 'SettingsController@saveEvent');
            $apiRouter->post('mailInOne/deleteEvent/', 'SettingsController@deleteEvent');
            $apiRouter->post('mailInOne/sendmails/', 'SettingsController@sendmails');
            $apiRouter->post('mailInOne/sendLog/', 'SettingsController@sendLog');
            $apiRouter->get('mailInOne/apikey/', 'SettingsController@getSettings');
            $apiRouter->get('mailInOne/getSynchronisation/', 'SettingsController@getSynchronisation');
            $apiRouter->get('mailInOne/getCampaign/', 'SyncController@getCampaign');
            $apiRouter->get('mailInOne/getSyncInfo/', 'SyncController@getSyncInfo');
            $apiRouter->post('mailInOne/setSync/', 'SyncController@setSync');
            $apiRouter->post('mailInOne/setEvent/', 'SyncController@setEvent');
            $apiRouter->get('mailInOne/getEvent/', 'SyncController@getEvent');
            // $apiRouter->get('mailInOne/getDoiPermissonByEmail/', 'SyncController@getDoiPermissonByEmail'); // not as route
            $apiRouter->get('mailInOne/getTargetGroup/', 'SyncController@getTargetGroup');
            $apiRouter->get('mailInOne/getContactFilter/', 'SyncController@getContactFilter');
            $apiRouter->get('mailInOne/loginurl/', 'SettingsController@getLoginUrl');
            $apiRouter->get('mailInOne/cronjob/', 'SettingsController@getCronjob');
            $apiRouter->post('mailInOne/resetSync/', 'SettingsController@resetSync');
            $apiRouter->put('mailInOne/saveEventEdit/', 'SettingsController@saveEventEdit');
            $apiRouter->get('mailInOne/getAllOrderEvent/', 'SyncController@getAllOrderEvent');
            $apiRouter->get('mailInOne/getAllStatusOrder/', 'SyncController@getAllStatusOrder');
            $apiRouter->post('mailInOne/saveOrderEvent/', 'SettingsController@saveOrderEvent');
            $apiRouter->post('mailInOne/deleteOrderEvent/', 'SettingsController@deleteOrderEvent');
            $apiRouter->put('mailInOne/saveOrderEventEdit/', 'SettingsController@saveOrderEventEdit');

        });
    }
}
