<?php
namespace MailInOne\Providers;

use Plenty\Plugin\ServiceProvider;
use Plenty\Modules\Cron\Services\CronContainer;
use MailInOne\Crons\ContactSyncCron;
use MailInOne\Crons\CreateCustomFieldCron;

use Plenty\Modules\EventProcedures\Services\EventProceduresService;
use Plenty\Modules\EventProcedures\Services\Entries\ProcedureEntry;


/**
 * Class MailInOneServiceProvider
 *
 * @package MailInOne\Providers
 */
class MailInOneServiceProvider extends ServiceProvider
{

    /**
     * Register the service provider.
     */
    public function register()
    {
        $this->getApplication()->register(MailInOneRouteServiceProvider::class);
        // $this->getApplication()->register(MessageContainerHelper::class);
        // $this->getApplication()->register(SyncHelper::class);
    }

    /**
      * @param CronContainer          $container
      * @param EventProceduresService $eventProceduresService
    */
    public function boot(CronContainer $container, EventProceduresService $eventProceduresService)
    {

        $container->add(CronContainer::EVERY_FIFTEEN_MINUTES, ContactSyncCron::class); // NEVER USE EVERY_FIVE_MINUTES, SINCE IT'S NOT IMPLEMENTED BY PLENTY
        // removed, since CustomFields are setup already by  ContactSyncCron
        // $container->add(CronContainer::EVERY_FIFTEEN_MINUTES, CreateCustomFieldCron::class);

                   // register event actions
		$eventProceduresService->registerProcedure('MailInOne', ProcedureEntry::EVENT_TYPE_ORDER, [
			'de' => 'MIO Neuer Auftrag',
			'en' => 'MIO New Order'
		], 'MailInOne\\Procedures\\OrderEventProcedure@newOrder');
                $eventProceduresService->registerProcedure('MailInOne', ProcedureEntry::EVENT_TYPE_ORDER, [
			'de' => 'MIO-[1] Unvollständige Daten',
			'en' => 'MIO-[1] Data missing'
		], \MailInOne\Procedures\OrderEventProcedure::class.'@dataMissing');

                $eventProceduresService->registerProcedure('MailInOne', ProcedureEntry::EVENT_TYPE_ORDER, [
			'de' => 'MIO-[1.1] Warten auf Zahlung & Freischaltung',
			'en' => 'MIO-[1.1] Waiting for payment & activation'
		], 'MailInOne\\Procedures\\OrderEventProcedure@waitingForPaymentAndActivation');
                $eventProceduresService->registerProcedure('MailInOne', ProcedureEntry::EVENT_TYPE_ORDER, [
			'de' => 'MIO-[1.2] Freigeschaltet, warten auf Zahlung',
			'en' => 'MIO-[1.2] Activated, waiting for payment'
		], 'MailInOne\\Procedures\\OrderEventProcedure@activatedWaitingForPayment');

                $eventProceduresService->registerProcedure('MailInOne', ProcedureEntry::EVENT_TYPE_ORDER, [
			'de' => 'MIO-[2] Warten auf Freischaltung',
			'en' => 'MIO-[2] Waiting for activation'
		], 'MailInOne\\Procedures\\OrderEventProcedure@waitingForActivation');
                $eventProceduresService->registerProcedure('MailInOne', ProcedureEntry::EVENT_TYPE_ORDER, [
			'de' => 'MIO-[3] Warten auf Zahlung',
			'en' => 'MIO-[3] Waiting for payment'
		], 'MailInOne\\Procedures\\OrderEventProcedure@waitingForPayment');

                $eventProceduresService->registerProcedure('MailInOne', ProcedureEntry::EVENT_TYPE_ORDER, [
			'de' => 'MIO-[3.1] Start PayPal-Zahlungsprozess',
			'en' => 'MIO-[3.1] Start PayPal-paymentprocess'
		], 'MailInOne\\Procedures\\OrderEventProcedure@startPayPalPaymentprocess');
                $eventProceduresService->registerProcedure('MailInOne', ProcedureEntry::EVENT_TYPE_ORDER, [
			'de' => 'MIO-[3.2] In Warteposition',
			'en' => 'MIO-[3.2] In waiting position'
		], 'MailInOne\\Procedures\\OrderEventProcedure@inWaitingPosition');

                $eventProceduresService->registerProcedure('MailInOne', ProcedureEntry::EVENT_TYPE_ORDER, [
			'de' => 'MIO-[3.3] Versandfertig; warten auf Zahlung',
			'en' => 'MIO-[3.3] Ready for shipment; waiting for payment'
		], 'MailInOne\\Procedures\\OrderEventProcedure@readyForShipmentWaitingForPayment');
                $eventProceduresService->registerProcedure('MailInOne', ProcedureEntry::EVENT_TYPE_ORDER, [
			'de' => 'MIO-[3.4] Mahnung versendet',
			'en' => 'MIO-[3.4] Dunning letter sent'
		], 'MailInOne\\Procedures\\OrderEventProcedure@dunningLetterSent');

                $eventProceduresService->registerProcedure('MailInOne', ProcedureEntry::EVENT_TYPE_ORDER, [
			'de' => 'MIO-[4] In Versandvorbereitung',
			'en' => 'MIO-[4] Preparing for Shipment'
		], 'MailInOne\\Procedures\\OrderEventProcedure@preparingForShipment');
                $eventProceduresService->registerProcedure('MailInOne', ProcedureEntry::EVENT_TYPE_ORDER, [
			'de' => 'MIO-[5] Freigabe Versand',
			'en' => 'MIO-[5] Shipment process started'
		], 'MailInOne\\Procedures\\OrderEventProcedure@shipmentProcessStarted');

                $eventProceduresService->registerProcedure('MailInOne', ProcedureEntry::EVENT_TYPE_ORDER, [
			'de' => 'MIO-[5.1] Abwicklung extern',
			'en' => 'MIO-[5.1] External fulifillment'
		], 'MailInOne\\Procedures\\OrderEventProcedure@externalFulifillment');
                $eventProceduresService->registerProcedure('MailInOne', ProcedureEntry::EVENT_TYPE_ORDER, [
			'de' => 'MIO-[6] Gerade im Versand',
			'en' => 'MIO-[6] Shipping in process'
		], 'MailInOne\\Procedures\\OrderEventProcedure@shippingInProcess');

                $eventProceduresService->registerProcedure('MailInOne', ProcedureEntry::EVENT_TYPE_ORDER, [
			'de' => 'MIO-[7] Warenausgang gebucht',
			'en' => 'MIO-[7] Shipped'
		], 'MailInOne\\Procedures\\OrderEventProcedure@shipped');
                $eventProceduresService->registerProcedure('MailInOne', ProcedureEntry::EVENT_TYPE_ORDER, [
			'de' => 'MIO-[7.1] Auftrag exportiert',
			'en' => 'MIO-[7.1] Order exported'
		], 'MailInOne\\Procedures\\OrderEventProcedure@orderExported');

                $eventProceduresService->registerProcedure('MailInOne', ProcedureEntry::EVENT_TYPE_ORDER, [
			'de' => 'MIO-[8] Storniert',
			'en' => 'MIO-[8] Cancelled'
		], 'MailInOne\\Procedures\\OrderEventProcedure@cancelled');
                $eventProceduresService->registerProcedure('MailInOne', ProcedureEntry::EVENT_TYPE_ORDER, [
			'de' => 'MIO-[8.1] Storniert durch Kunden',
			'en' => 'MIO-[8.1] Cancelled by customer'
		], 'MailInOne\\Procedures\\OrderEventProcedure@cancelledByCustomer');

                $eventProceduresService->registerProcedure('MailInOne', ProcedureEntry::EVENT_TYPE_ORDER, [
			'de' => 'MIO-[9] Retoure',
			'en' => 'MIO-[9] Return'
		], 'MailInOne\\Procedures\\OrderEventProcedure@orderReturn');
                $eventProceduresService->registerProcedure('MailInOne', ProcedureEntry::EVENT_TYPE_ORDER, [
			'de' => 'MIO-[9.1] Ware wird geprüft',
			'en' => 'MIO-[9.1] Items are undergoing checks'
		], 'MailInOne\\Procedures\\OrderEventProcedure@itemsAreUndergoingChecks');

                $eventProceduresService->registerProcedure('MailInOne', ProcedureEntry::EVENT_TYPE_ORDER, [
			'de' => 'MIO-[9.2] Warten auf Retoure von Großhändler',
			'en' => 'MIO-[9.2] Waiting for return from wholeseller'
		], 'MailInOne\\Procedures\\OrderEventProcedure@waitingForReturnFromWholeseller');
                $eventProceduresService->registerProcedure('MailInOne', ProcedureEntry::EVENT_TYPE_ORDER, [
			'de' => 'MIO-[9.3] Gewährleistung eingeleitet',
			'en' => 'MIO-[9.3] Warranty process started'
		], 'MailInOne\\Procedures\\OrderEventProcedure@warrantyProcessStarted');

                $eventProceduresService->registerProcedure('MailInOne', ProcedureEntry::EVENT_TYPE_ORDER, [
			'de' => 'MIO-[9.4] Umtausch eingeleitet',
			'en' => 'MIO-[9.4] exchange process started'
		], 'MailInOne\\Procedures\\OrderEventProcedure@exchangeProcessStarted');
                $eventProceduresService->registerProcedure('MailInOne', ProcedureEntry::EVENT_TYPE_ORDER, [
			'de' => 'MIO-[9.5] Gutschrift angelegt',
			'en' => 'MIO-[9.5] Credit note created'
		], 'MailInOne\\Procedures\\OrderEventProcedure@creditNoteCreated');

                $eventProceduresService->registerProcedure('MailInOne', ProcedureEntry::EVENT_TYPE_ORDER, [
			'de' => 'MIO-[10] Gewährleistung',
			'en' => 'MIO-[10] Warranty'
		], 'MailInOne\\Procedures\\OrderEventProcedure@warranty');
                $eventProceduresService->registerProcedure('MailInOne', ProcedureEntry::EVENT_TYPE_ORDER, [
			'de' => 'MIO-[11] Gutschrift',
			'en' => 'MIO-[11] Credit note'
		], 'MailInOne\\Procedures\\OrderEventProcedure@creditNote');

                $eventProceduresService->registerProcedure('MailInOne', ProcedureEntry::EVENT_TYPE_ORDER, [
			'de' => 'MIO-[11.1] Gutschrift ausgezahlt',
			'en' => 'MIO-[11.1] Refund done'
		], 'MailInOne\\Procedures\\OrderEventProcedure@refundDone');
                $eventProceduresService->registerProcedure('MailInOne', ProcedureEntry::EVENT_TYPE_ORDER, [
			'de' => 'MIO-[12] Reparatur',
			'en' => 'MIO-[12] Repair'
		], 'MailInOne\\Procedures\\OrderEventProcedure@repair');

                $eventProceduresService->registerProcedure('MailInOne', ProcedureEntry::EVENT_TYPE_ORDER, [
			'de' => 'MIO-[13] Sammelauftrag',
			'en' => 'MIO-[13] Collection Order'
		], 'MailInOne\\Procedures\\OrderEventProcedure@collectionOrder');
                $eventProceduresService->registerProcedure('MailInOne', ProcedureEntry::EVENT_TYPE_ORDER, [
			'de' => 'MIO-[14] Sammelgutschrift',
			'en' => 'MIO-[14] Collection Order credit note'
		], 'MailInOne\\Procedures\\OrderEventProcedure@collectionOrderCreditNote');
    }
}

