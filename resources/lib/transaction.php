<?php
$rootPath = __DIR__.\DIRECTORY_SEPARATOR;
\define('DIR_Includes', $rootPath.'includes'.\DIRECTORY_SEPARATOR);

require_once(DIR_Includes.'MaileonApiClient.php');

function isUnsubscribed($email, $config)
{
    $reportsService = new com_maileon_api_reports_ReportsService($config);
    $reportsService->setDebug(false);
    $response = $reportsService->getUnsubscribers(null, null, null, null, [$email]);

    return (count($response->getResult()) > 0)
        ? true  // unsubsribed
        :  false; // not unsubsribed => still subscribed
}


// check Contact
function existsContact($email, $config)
{
    $debug           = FALSE;
    $contactsService = new com_maileon_api_contacts_ContactsService($config);
    $contactsService->setDebug($debug);
    $response        = $contactsService->getContactByEmail($email);

    return $response->isSuccess();
}


// This method creates/updates a contact with the given id in Maileon
function createContact($email, $FIRSTNAME, $LASTNAME, $gender, $config)
{
    $debug           = FALSE;
    $contactsService = new com_maileon_api_contacts_ContactsService($config);
    $contactsService->setDebug($debug);

    $newContact                                                                              = new com_maileon_api_contacts_Contact();
    $newContact->email                                                                       = $email;
    $newContact->standard_fields[com_maileon_api_contacts_StandardContactField::$FIRSTNAME]  = $FIRSTNAME;
    $newContact->standard_fields[com_maileon_api_contacts_StandardContactField::$LASTNAME]   = $LASTNAME;
    $newContact->standard_fields[com_maileon_api_contacts_StandardContactField::$SALUTATION] = $gender;
    $newContact->permission                                                                  = com_maileon_api_contacts_Permission::$DOI_PLUS;

    $response = $contactsService->createContact($newContact, com_maileon_api_contacts_SynchronizationMode::$UPDATE, "", "", false);

    return $response;
}


try {
    $apiKey            = SdkRestApi::getParam('apiKey');
    $kontaktereignisID = SdkRestApi::getParam('kontaktereignisID');
    $contact           = SdkRestApi::getParam('maileon');

    // Set the global configuration for accessing the REST-API
    $config = array(
        "BASE_URI" => "https://api.maileon.com/1.0",
        "API_KEY" => $apiKey,
        "PROXY_HOST" => "",
        "PROXY_PORT" => "",
        "THROW_EXCEPTION" => true,
        "TIMEOUT" => 300,
        "DEBUG" => "false" // NEVER enable on production
    );

    /*
      if($contact["gender"] == 'male'){
      $gender = 'Herr';
      }else{
      $gender = 'Frau';
      }
     */

    switch ($contact["gender"]) {
        case 'd':
        case 'divers':
        case 'male':
        case 'm':
            $gender = 'Herr';
            break;
        case 'female':
        case 'f':
            $gender = 'Frau';
            break;
        default:
            $gender = 'Herr';
            break;
    }

    $is_unsubscribed = isUnsubscribed($contact['email'], $config);

    if(!$is_unsubscribed) {
        //Check Conatct
        $contactExisted = existsContact($contact['email'], $config);

        if (!$contactExisted) {
            createContact(
                $contact['email'],
                $contact["firstName"],
                $contact["lastName"],
                $gender,
                $config
            );
            sleep(2);
        }
        // Create the service
        $transactionsService                = new com_maileon_api_transactions_TransactionsService($config);
        $debug                              = false;
        $transactionsService->setDebug($debug);
        // @see SyncHelper::helperGetMaileonContactData()
        $transaction                        = new com_maileon_api_transactions_Transaction();
        $transaction->contact               = new com_maileon_api_transactions_ContactReference();
        $transaction->contact->email        = strtolower($contact['email']);
        $transaction->type                  = (int) $kontaktereignisID;
        $transaction->content['ContactId']  = (empty($contact["contactId"])) ? (int) $contact["id"] : (int) $contact["contactId"]; // newsletterId or CRM-contactId
        //$transaction->content['gender'] = $contact["gender"];
        $transaction->content['Firstname']  = $contact["firstName"];
        $transaction->content['Lastname']   = $contact["lastName"];
        // $transaction->content['Geburtstag'] = $contact["birthday"];
        $transaction->content['FolderName'] = $contact["folderName"];

        $transactions = array($transaction);
        $response     = $transactionsService->createTransactions($transactions, false, false);

        return $response->isSuccess();
    } else {
        return $contact['email'] . ' is unsubscribed';
    }

} catch (Exception $e) {
    return json_decode($e->getData());
}
