<?php
/* @internal returns array of mio contacts with email, mio_id and all corresponding custom fields
 * @author JSR|treaction ag
 * @api api.maileon.com/1.0
 * @function getAllMioContactsWithCustomFields
 */
$rootPath = __DIR__.\DIRECTORY_SEPARATOR;
\define('DIR_Includes', $rootPath.'includes'.\DIRECTORY_SEPARATOR);


require_once(DIR_Includes.'MaileonApiClient.php');


try {
    $apiKey        = SdkRestApi::getParam('apiKey');
    $custom_fields = SdkRestApi::getParam('custom_fields');
    // Set the global configuration for accessing the REST-API
    $config        = array(
        "BASE_URI" => "https://api.maileon.com/1.0",
        "API_KEY" => $apiKey,
        "PROXY_HOST" => "",
        "PROXY_PORT" => "",
        "THROW_EXCEPTION" => true,
        "TIMEOUT" => 300,
        "DEBUG" => "false" // NEVER enable on production
    );
  

    $contacts_service = new com_maileon_api_contacts_ContactsService($config);
    $contacts_service->setDebug(false);

    $xml                = $contacts_service->getContactsCount()->getBodyData();
    $number_of_contacts = new SimpleXMLElement($xml);
    $loops              = ((int) ((int) $number_of_contacts[0] / 1000.0) + 1);

    $ret_arr = [];

    for ($i = 0; $i < $loops; $i++) {
        $page     = (int) ($i + 1);
        $contacts = $contacts_service->getContacts($page, 100, ['FIRSTNAME', 'LASTNAME'],$custom_fields);

        return $contacts;
        //$contacts = new SimpleXMLElement( $contacts_service->getContacts($page,1000)->getBodyData());
        foreach ($contacts->getResult() as $c) {
            if (isset($c->email)) {
                $ret_arr[] = [
                    'email' => $c->email,
                    'mio_id' => $c->id,
                    'mio_custom_fields' => $c->custom_fields,
                ];
            }
        }
    }

    return $ret_arr;
} catch (Exception $e) {
    return json_decode($e->getData());
}
