<?php

$rootPath = __DIR__ . \DIRECTORY_SEPARATOR;
\define('DIR_Includes', $rootPath . 'includes' . \DIRECTORY_SEPARATOR);

require_once(DIR_Includes.'MaileonApiClient.php');


try{
    $apiKey = SdkRestApi::getParam('apiKey');
    $kontaktereignisID= SdkRestApi::getParam('kontaktereignisID');
    // Set the global configuration for accessing the REST-API
    $config = array(
        "BASE_URI" => "https://api.maileon.com/1.0",
        "API_KEY" => $apiKey,
        "PROXY_HOST" => "",
        "PROXY_PORT" => "",
        "THROW_EXCEPTION" => true,
        "TIMEOUT" => 300, 
        "DEBUG" => "false" // NEVER enable on production
    );

   // Create the service
	$transactionsService = new com_maileon_api_transactions_TransactionsService($config);
	$transactionsService->setDebug($debug);
	
        $response = $transactionsService->getTransactionType($kontaktereignisID);

       return $response->getResultXML() ;

}catch (Exception $e){
    return json_decode($e->getData());

}