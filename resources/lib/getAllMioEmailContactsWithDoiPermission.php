<?php
/* @internal returns array of mio contact permissons
 * @author JSR|treaction ag
 * @api api.maileon.com/1.0
 * @function getAllMioEmailContactsWithDoiPermission
 */
$rootPath = __DIR__ . \DIRECTORY_SEPARATOR;
\define('DIR_Includes', $rootPath . 'includes' . \DIRECTORY_SEPARATOR);

require_once(DIR_Includes.'MaileonApiClient.php');

try{
    $apiKey = SdkRestApi::getParam('apiKey');
    // Set the global configuration for accessing the REST-API
    $config = array(
        "BASE_URI" => "https://api.maileon.com/1.0",
        "API_KEY" => $apiKey,
        "PROXY_HOST" => "",
        "PROXY_PORT" => "",
        "THROW_EXCEPTION" => true,
        "TIMEOUT" => 300,
        "DEBUG" => "false" // NEVER enable on production
    );

    $contacts_service = new com_maileon_api_contacts_ContactsService($config);
    $contacts_service->setDebug(false);

    $xml = $contacts_service->getContactsCount()->getBodyData();
    $number_of_contacts = new SimpleXMLElement( $xml );
    $loops = ((int)((int)$number_of_contacts[0] / 1000.0)+1);

    $ret_arr = [];

    for($i=0; $i<$loops;$i++) {
        $page = (int)($i + 1);
        $contact_arr = new SimpleXMLElement( $contacts_service->getContacts($page,1000)->getBodyData());

        foreach($contact_arr as $c) {
            if(isset($c->email) && isset($c->permission)) {
                $ret_arr[] = [
                    'email' => $c->email,
                    'permission' => $c->permission
                ];
            }
        }
    }
        /**
         * returned is $ret_arr in following structure:
         * 0:Object
            email:Array[1]
            0:"anja.ahlewurst@web.de"
            permission:Array[1]
            0:"5"
            1:Object
            email:Array[1]
            0:"email@mail.de"
            permission:Array[1]
            0:"5"
         */
    return $ret_arr;
    
}catch (Exception $e){
    return json_decode($e->getData());
}