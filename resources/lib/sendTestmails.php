<?php


$rootPath = __DIR__ . \DIRECTORY_SEPARATOR;
\define('DIR_Mail', $rootPath . 'Mail' . \DIRECTORY_SEPARATOR);
\define('DIR_Swift', $rootPath . 'swiftmailer-5.4.1' . \DIRECTORY_SEPARATOR);
\define('DIR_Lib', DIR_Swift . 'lib' . \DIRECTORY_SEPARATOR);

require_once(DIR_Lib.'swift_required.php');
require_once (DIR_Mail.'SwiftMailer.php');

$daten = SdkRestApi::getParam('daten');

$subject = 'Neue Mandant';
$messageBody = $daten;

$serverName = 'maas.treaction.de';
$smtpHost = 'smtp.office365.com';
$smtpUser = 'dev-maassmtp@treaction.de';
$smtpPassword = 'fNc8H+3Rw@m&1x';
$smtpEncryption = 'tls';
$smtpPort = 587;
$fromEmail = 'dev-maassmtp@treaction.de';
$fromName = 'Maas Treaction';

$cmSenderPrefix = '@treaction.de';

$mailEngine = new \Packages\Core\Mail\SwiftMailer();

$mailEngine->setSmtpHost($smtpHost);
$mailEngine->setSmtpUsername($smtpUser);
$mailEngine->setSmtpPassword($smtpPassword);
$mailEngine->setSmtpPort($smtpPort);
$mailEngine->setEncryption($smtpEncryption);
$mailEngine->setServerName($serverName);
$mailEngine->setFromEmail($fromEmail);
$mailEngine->setFromName($fromName);
$mailEngine->init();

$Swift_Message = $mailEngine->createMessageObject();
/* @var $swiftMessage \Swift_Message */

$Swift_Message->setBody($messageBody)
    ->setSubject($subject)
;
#$Swift_Message->setTo(array('sami.jarmoud@treaction.de', 'campaign@treaction.de'));
$Swift_Message->setTo('sami.jarmoud@treaction.net');

$mailerSendResults = $mailEngine->sendMessage($Swift_Message);
return $mailerSendResults;