<?php
$rootPath = __DIR__.\DIRECTORY_SEPARATOR;
\define('DIR_Includes', $rootPath.'includes'.\DIRECTORY_SEPARATOR);

require_once(DIR_Includes.'MaileonApiClient.php');

// check Contact
function existsContact($email, $config)
{

    $contactsService = new com_maileon_api_contacts_ContactsService($config);
    $contactsService->setDebug(false);
    $response        = $contactsService->getContactByEmail($email);

    return $response->isSuccess();
}


// This method creates/updates a contact with the given id in Maileon
function createContact($email, $FIRSTNAME, $LASTNAME, $gender, $config)
{

    $contactsService = new com_maileon_api_contacts_ContactsService($config);
    $contactsService->setDebug(false);

    $newContact                                                                              = new com_maileon_api_contacts_Contact();
    $newContact->email                                                                       = $email;
    $newContact->standard_fields[com_maileon_api_contacts_StandardContactField::$FIRSTNAME]  = $FIRSTNAME;
    $newContact->standard_fields[com_maileon_api_contacts_StandardContactField::$LASTNAME]   = $LASTNAME;
    $newContact->standard_fields[com_maileon_api_contacts_StandardContactField::$SALUTATION] = $gender;
    $newContact->permission                                                                   = com_maileon_api_contacts_Permission::$DOI_PLUS;

    $response = $contactsService->createContact(
        $newContact,
        com_maileon_api_contacts_SynchronizationMode::$UPDATE,
        "",
        "",
        false
    );

    return $response;
}


try {
    $apiKey            = SdkRestApi::getParam('apiKey');
    $kontaktereignisID = SdkRestApi::getParam('kontaktereignisID');
    $contact           = SdkRestApi::getParam('maileon');
    $products          = array();

    // Set the global configuration for accessing the REST-API
    $config            = array(
        "BASE_URI" => "https://api.maileon.com/1.0",
        "API_KEY" => $apiKey,
        "PROXY_HOST" => "",
        "PROXY_PORT" => "",
        "THROW_EXCEPTION" => true,
        "TIMEOUT" => 300,
        "DEBUG" => "false" // NEVER enable on production
    );

    if (isset($contact["gender"])) {

        if ($contact["gender"] === 'male') {
            $gender = 'Herr';
        } else {
            $gender = 'Frau';
        }

    } else {
        $gender = ' ';
    }

    //Order
    foreach ($contact['item'] as $item) {
        $product['id']            = $item['id'];
        $product['orderItemName'] = $item['orderItemName'];
        $product['quantity']      = $item['quantity'];
        $product['currency']      = $item['currency'];
        $product['priceGross']    = $item['priceGross'];
        array_push($products, $product);
    }

    $netTotal['netTotal']     = $contact['netTotal'];
    $grossTotal['grossTotal'] = $contact['grossTotal'];
    array_push($products, $netTotal);
    array_push($products, $grossTotal);


    //Check Contact
    $contactExisted = existsContact($contact['email'], $config);
    if (!$contactExisted) {
        createContact($contact['email'], $contact["firstName"], $contact["lastName"], $gender, $config);
        sleep(2);
    }

    // Create the service
    $transactionsService = new com_maileon_api_transactions_TransactionsService($config);
    $debug               = false;
    $transactionsService->setDebug(false);

    $transaction          = new com_maileon_api_transactions_Transaction();
    $transaction->contact = new com_maileon_api_transactions_ContactReference();

    $transaction->contact->email       = $contact['email'];
    $transaction->type                 = (int) $kontaktereignisID;
    $transaction->content['gender']    = $gender;
    $transaction->content['firstName'] = $contact["firstName"];
    $transaction->content['lastName']  = $contact["lastName"];
    $transaction->content['contactId'] = $contact["contactId"];
    $transaction->content['orderId']   = $contact["orderId"];
    $transaction->content['Products']  = $products;
    $transactions                      = array($transaction);
    $response                          = $transactionsService->createTransactions($transactions, false, false);

    return $response->getResult();
} catch (Exception $e) {
    return json_decode($e->getData());
}