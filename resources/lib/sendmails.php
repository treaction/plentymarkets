<?php


$rootPath = __DIR__ . \DIRECTORY_SEPARATOR;
\define('DIR_Mail', $rootPath . 'Mail' . \DIRECTORY_SEPARATOR);
\define('DIR_Swift', $rootPath . 'swiftmailer-5.4.1' . \DIRECTORY_SEPARATOR);
\define('DIR_Lib', DIR_Swift . 'lib' . \DIRECTORY_SEPARATOR);

require_once(DIR_Lib.'swift_required.php');
require_once (DIR_Mail.'SwiftMailer.php');

$account = SdkRestApi::getParam('account');

$subject = 'Neue Mandant';
$messageBody = 'Bitte ein neue Mandant anlegen'
    .\chr(13) . \chr(13)
    . '========================================' . \chr(13)
    . 'Vorname: ' . $account["firstname"]. \chr(13)
    . 'Nachname: ' . $account["lastname"] . \chr(13)
    . 'Firma: ' . $account["company"] . \chr(13)
    . 'Telefonnummer: ' . $account["phone"] . \chr(13)
    . 'Straße: ' . $account["street"]. \chr(13)
    . 'PLZ: ' . $account["zip"]. \chr(13)
    . 'Ort: ' . $account["city"]. \chr(13)
    . 'Land: ' . $account["country"]. \chr(13)
    . 'Email: ' . $account["email"]. \chr(13)
    . '========================================' . \chr(13)
;

$serverName = 'maas.treaction.de';
$smtpHost = 'smtp.office365.com';
$smtpUser = 'dev-maassmtp@treaction.de';
$smtpPassword = 'fNc8H+3Rw@m&1x';
$smtpEncryption = 'tls';
$smtpPort = 587;
$fromEmail = 'dev-maassmtp@treaction.de';
$fromName = 'Maas Treaction';

$cmSenderPrefix = '@treaction.de';

$mailEngine = new \Packages\Core\Mail\SwiftMailer();

$mailEngine->setSmtpHost($smtpHost);
$mailEngine->setSmtpUsername($smtpUser);
$mailEngine->setSmtpPassword($smtpPassword);
$mailEngine->setSmtpPort($smtpPort);
$mailEngine->setEncryption($smtpEncryption);
$mailEngine->setServerName($serverName);
$mailEngine->setFromEmail($fromEmail);
$mailEngine->setFromName($fromName);
$mailEngine->init();

$Swift_Message = $mailEngine->createMessageObject();
/* @var $swiftMessage \Swift_Message */

$Swift_Message->setBody($messageBody)
    ->setSubject($subject)
;
$Swift_Message->setTo(array('sami.jarmoud@treaction.de', 'campaign@treaction.de'));
#$Swift_Message->setTo('sami.jarmoud@treaction.de');

$mailerSendResults = $mailEngine->sendMessage($Swift_Message);
return $mailerSendResults;