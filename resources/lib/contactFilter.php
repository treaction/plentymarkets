<?php

$rootPath = __DIR__ . \DIRECTORY_SEPARATOR;
\define('DIR_Includes', $rootPath . 'includes' . \DIRECTORY_SEPARATOR);

require_once(DIR_Includes.'MaileonApiClient.php');


try{
    $apiKey = SdkRestApi::getParam('apiKey');
    // Set the global configuration for accessing the REST-API
    $config = array(
        "BASE_URI" => "https://api.maileon.com/1.0",
        "API_KEY" => $apiKey,
        "PROXY_HOST" => "",
        "PROXY_PORT" => "",
        "THROW_EXCEPTION" => true,
        "TIMEOUT" => 300, 
        "DEBUG" => "false" // NEVER enable on production
    );

    $contactfiltersService = new com_maileon_api_contactfilters_ContactfiltersService($config);
    $debug = false;
    $contactfiltersService->setDebug($debug);

    $response = $contactfiltersService->getContactFilters(1,50);

    return $response->getResultXML() ;

}catch (Exception $e){
    return json_decode($e->getData());

}