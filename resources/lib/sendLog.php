<?php
/**
 * @internal Send Log Data on Errors|Warnings|Infos
 * @author jsr|treaction ag
 * @version 3.0.2
 */
$rootPath = __DIR__ . \DIRECTORY_SEPARATOR;
\define('DIR_Mail', $rootPath . 'Mail' . \DIRECTORY_SEPARATOR);
\define('DIR_Swift', $rootPath . 'swiftmailer-5.4.1' . \DIRECTORY_SEPARATOR);
\define('DIR_Lib', DIR_Swift . 'lib' . \DIRECTORY_SEPARATOR);

require_once(DIR_Lib.'swift_required.php');
require_once (DIR_Mail.'SwiftMailer.php');

$account = SdkRestApi::getParam('log_data');

$subject = 'Logging-Info from pentymarkets MIO plugin';
$messageBody = $log_data;


$serverName = 'maas.treaction.de';
$smtpHost = 'smtp.office365.com';
$smtpUser = 'dev-maassmtp@treaction.de';
$smtpPassword = 'fNc8H+3Rw@m&1x';
$smtpEncryption = 'tls';
$smtpPort = 587;
$fromEmail = 'dev-maassmtp@treaction.de';
$fromName = 'Maas Treaction';

$cmSenderPrefix = '@treaction.de';

$mailEngine = new \Packages\Core\Mail\SwiftMailer();

$mailEngine->setSmtpHost($smtpHost);
$mailEngine->setSmtpUsername($smtpUser);
$mailEngine->setSmtpPassword($smtpPassword);
$mailEngine->setSmtpPort($smtpPort);
$mailEngine->setEncryption($smtpEncryption);
$mailEngine->setServerName($serverName);
$mailEngine->setFromEmail($fromEmail);
$mailEngine->setFromName($fromName);
$mailEngine->init();

$Swift_Message = $mailEngine->createMessageObject();
/* @var $swiftMessage \Swift_Message */

$Swift_Message->setBody($messageBody)
    ->setSubject($subject)
;
$Swift_Message->setTo(array('sami.jarmoud@treaction.de','campaign@treaction.de','jochen.schaefer@treation.net'));
#$Swift_Message->setTo('sami.jarmoud@treaction.de');

$mailerSendResults = $mailEngine->sendMessage($Swift_Message);
return $mailerSendResults;
