<?php


$rootPath = __DIR__ . \DIRECTORY_SEPARATOR;
\define('DIR_Includes', $rootPath . 'includes' . \DIRECTORY_SEPARATOR);

require_once(DIR_Includes.'MaileonApiClient.php');

function validationZeichen($name){
        $input = \htmlspecialchars_decode($name);
          $zeichen = array(
              '&' => '-',
              '$' => '-',
              '%' => '-',
              '{' => '-',
              '}' => '-',
              '\\' => '-',
              '/' => '-',
              '`' => '-',
              '€' => '-',
              '*' => '-',
              '+' => '-',
              '~' => '-',
              '<' => '-',
              '>' => '-',
              ';' => '-',
              '|' => '-',
              '#' => '-' 
          );
          foreach ($zeichen as $key => $value){
              $ouput = \str_replace($key, $value, $input);
              $input = $ouput;
          }
          $strlen = strlen($ouput);
          if($strlen < 3){
              $ouputlen = $ouput.'--';
          }else{
              $ouputlen = $ouput;
          }
          return $ouputlen;
}

try{
    $apiKey = SdkRestApi::getParam('apiKey');
    // Set the global configuration for accessing the REST-API
    $config = array(
        "BASE_URI" => "https://api.maileon.com/1.0",
        "API_KEY" => $apiKey,
        "PROXY_HOST" => "",
        "PROXY_PORT" => "",
        "THROW_EXCEPTION" => true,
        "TIMEOUT" => 300, 
        "DEBUG" => "false" // NEVER enable on production
    );

    $contactsService = new com_maileon_api_contacts_ContactsService($config);
    $debug = false;
    $contactsService->setDebug($debug);

    $folder = SdkRestApi::getParam('maileon');
    $folderValidation = validationZeichen($folder['folderName']);
    
    switch ($folderValidation) {
        case 'plentyID':
            $result = $contactsService->createCustomField($folder['folderName'],'string');
            break;
        case 'lastOrderAt':
            $result = $contactsService->createCustomField($folder['folderName'],'date');
            break;
        
        default:
            $result = $contactsService->createCustomField($folder['folderName'],'boolean');
            break;
    }
    
    return $result;

}catch (Exception $e){
    return json_decode($e->getData());
}









