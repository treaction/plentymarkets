<?php
namespace Packages\Core\Mail;


/**
 * Description of InterfaceMail
 *
 * @author Cristian.Reus
 */
interface InterfaceMail {
	
	/**
	 * init
	 * 
	 * @return void
	 */
	public function init();

	/**
	 * sendMessage
	 * 
	 * @param mixed $message
	 * @return mixed
	 */
	public function sendMessage($message);
	
	/**
	 * createMessageObject
	 * 
	 * @return mixed
	 */
	public function createMessageObject();
}
