<?php
/* @internal returns mio custom fields
 * @author JSR|treaction ag
 * @api api.maileon.com/1.0
 * @function getDoiPermissionByEmail
 */
$rootPath = __DIR__ . \DIRECTORY_SEPARATOR;
\define('DIR_Includes', $rootPath . 'includes' . \DIRECTORY_SEPARATOR);

require_once(DIR_Includes.'MaileonApiClient.php');
$mio_contact_email = SdkRestApi::getParam('mio_contact_email');

try{
    $apiKey = SdkRestApi::getParam('apiKey');
    // Set the global configuration for accessing the REST-API
    $config = array(
        "BASE_URI" => "https://api.maileon.com/1.0",
        "API_KEY" => $apiKey,
        "PROXY_HOST" => "",
        "PROXY_PORT" => "",
        "THROW_EXCEPTION" => true,
        "TIMEOUT" => 300,
        "DEBUG" => "false" // NEVER enable on production
    );

    $contactsService = new com_maileon_api_contacts_ContactsService($config);
    $contactsService->setDebug(false);
    $contact = $contactsService->getContactByEmail($mio_contact_email)->getResult();
    return $contact->permission;


}catch (Exception $e){
    return json_decode($e->getData());
}
