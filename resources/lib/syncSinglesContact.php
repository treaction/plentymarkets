<?php
$rootPath = __DIR__.\DIRECTORY_SEPARATOR;
\define('DIR_Includes', $rootPath.'includes'.\DIRECTORY_SEPARATOR);

require_once(DIR_Includes.'MaileonApiClient.php');
/**
 * @var const false = use email address as identifier
 */
const USE_EXTERNAL_ID = false;

/**
 * @var const true  = invalid contacts are ignored and the synchronization succeeds for valid contacts
 */
const IGNORE_INVALID_CONTACTS = true;

/**
 * @var const If set to true unsubscribed contacts will be reimported, else, they will be ignored
 */
const REIMPORT_UNSUBSCRIBED_CONTACTS = false;

/**
 * @var const false = the permission will be used for new contacts only and the permission of existing contacts will not be changed
 */
const OVERRIDE_PERMISSION = false;

/**
 * @var const If set to true only existing contacts will be updated. Not existing contacts will not be created
 */
const UPDATE_ONLY = false;

/**
 * @var const  If set to true, the maileon id is used as identifier for the contacts. If no id is set, the email will be used instead. If ‚prefer_maileon_id' and ‚use_external_id' are set to true, a bad request (400) will be returned
 */
const PREFER_MAILEON_ID = false;




try {
    $apiKey                   = SdkRestApi::getParam('apiKey');
    $syncContactsWithOutId    = SdkRestApi::getParam('syncContactsWithOutId');
    $syncUpdateContactsWithId = SdkRestApi::getParam('syncUpdateContactsWithId');
    $syncNewContactsWithId    = SdkRestApi::getParam('syncNewContactsWithId');


    $countSyncContactsWithOutId    = count($syncContactsWithOutId);
    $countSyncUpdateContactsWithId = count($syncUpdateContactsWithId);
    $countSyncNewContactsWithId    = count($syncNewContactsWithId);

    $resultSync['$countSyncContactsWithOutId']    = $countSyncContactsWithOutId;
    $resultSync['$countSyncUpdateContactsWithId'] = $countSyncUpdateContactsWithId;
    $resultSync['$countSyncNewContactsWithId']    = $countSyncNewContactsWithId;

    // Set the global configuration for accessing the REST-API
    $config = array(
        "BASE_URI" => "https://api.maileon.com/1.0",
        "API_KEY" => $apiKey,
        "PROXY_HOST" => "",
        "PROXY_PORT" => "",
        "THROW_EXCEPTION" => true,
        "TIMEOUT" => 3000000,
        "DEBUG" => "false" // NEVER enable on production
    );

    $contactsService = new com_maileon_api_contacts_ContactsService($config);
    $contactsService->setDebug(false);


    /**
     * syncContactsWithOutId -> funktioniert
     */
    if ($countSyncContactsWithOutId > 0) {

        //checkDuplicate
        $checkEmailDuplicateWithOutId = array();
        $duplicateEmailWithOutId      = array();
        $custom_fields                = [];
        $email                        = '';


        /**
         * @internal collect plenty folders for mio custom fields of contact -> by doing this only new folders should be added.
         * CAUTION: no longer exiting plenty folders (where a plenty contact is assigned to) will not be removed this way in MIO!!!
         */
        foreach ($syncContactsWithOutId as $plenty_contact) {

            if (!empty($plenty_contact['maileon']["folderName"])) {
                $custom_fields[$plenty_contact['maileon']["email"]][] = $plenty_contact['maileon']["folderName"];
            } else {
                ; //do nothing
            }
        }


        foreach ($syncContactsWithOutId as $contactArrayWithOutId) {

            //check Duplicate
            if (array_key_exists($contactArrayWithOutId['maileon']["email"], $checkEmailDuplicateWithOutId)) {

                $duplicateEmailWithOutId[$contactArrayWithOutId['maileon']["email"]] = $contactArrayWithOutId; // is contact in MailInOne ? single update contact : create contact
            } else {

                $checkEmailDuplicateWithOutId[$contactArrayWithOutId['maileon']["email"]] = $contactArrayWithOutId;
            }
        }

        //Kontakt Sync
        if (count($checkEmailDuplicateWithOutId) > 0) {

            $resultSync['emailWithOutId'] = doContactSync(
                $checkEmailDuplicateWithOutId,
                $contactsService,
                $custom_fields
            );
        }

        // Duplictae Update
        if (count($duplicateEmailWithOutId) > 0) {

            $resultSync['duplicateEmailWithOutId'] = doContactSync(
                $duplicateEmailWithOutId,
                $contactsService,
                $custom_fields
            );
        }
    }




    /**
     * syncUpdateContactsWithId -> noch nicht getestet
     */
    if ($countSyncUpdateContactsWithId > 0) {

        //checkDuplicate
        $checkEmailDuplicateWithId = array();
        $duplicateEmailWithId      = array();
        $custom_fields             = array();
        $email                     = '';

        foreach ($syncUpdateContactsWithId as $plenty_contact) {

            if (!empty($plenty_contact['maileon']["folderName"])) {
                $custom_fields[$plenty_contact['maileon']["email"]][] = $plenty_contact['maileon']["folderName"];
            } else {
                ; //do nothing
            }
        }


        foreach ($syncUpdateContactsWithId as $contactArrayUpdateWithId) {
            //check Duplicate
            if (array_key_exists($contactArrayUpdateWithId['maileon']["email"], $checkEmailDuplicateWithId)) {

                $duplicateEmailWithId[$contactArrayUpdateWithId['maileon']["email"]] = $contactArrayUpdateWithId;
            } else {

                $checkEmailDuplicateWithId[$contactArrayUpdateWithId['maileon']["email"]] = $contactArrayUpdateWithId;
            }
        }

        //Kontakt Sync
        if (count($checkEmailDuplicateWithId) > 0) {
            $resultSync['emailWithId'] = doContactSync(
                $checkEmailDuplicateWithId,
                $contactsService,
                $custom_fields
            );
        }
        // Duplictae Update
        if (count($duplicateEmailWithId) > 0) {
            $resultSync['duplicateEmailWithId'] = doContactSync(
                $duplicateEmailWithId,
                $contactsService,
                $custom_fields
            );
        }
    }


    /**
     * syncNewContactsWithId -> keine ordner !!!
     */
    if ($countSyncNewContactsWithId > 0) {


        //checkDuplicate
        $checkNewEmailDuplicate = array();
        $duplicateNewEmail      = array();
        $custom_fields          = array();
        $email                  = '';

        foreach ($syncNewContactsWithId as $plenty_contact) {

            if (!empty($plenty_contact['maileon']["folderName"])) {
                $custom_fields[$plenty_contact['maileon']["email"]][] = $plenty_contact['maileon']["folderName"];
            } else {
                ; //do nothing
            }
        }

        foreach ($syncNewContactsWithId as $contactArrayNewWithId) {

            //check Duplicate
            if (array_key_exists($contactArrayNewWithId['maileon']["email"], $checkNewEmailDuplicate)) {

                $duplicateNewEmail[$contactArrayNewWithId['maileon']["email"]] = $contactArrayNewWithId;
            } else {

                $checkNewEmailDuplicate[$contactArrayNewWithId['maileon']["email"]]
                    = $contactArrayNewWithId;
            }
        }

        //Kontakt Sync
        if (count($checkNewEmailDuplicate) > 0) {

            $resultSync['newEmailWithId'] = doContactSync(
                $checkNewEmailDuplicate,
                $contactsService,
                $custom_fields
            );
        }
        // Duplictae Update
        if (count($duplicateNewEmail) > 0) {

            $resultSync['duplicateNewEmailWithId'] = doContactSync(
                $duplicateNewEmail,
                $contactsService,
                $custom_fields
            );
        }
    }

    return $resultSync;
} catch (Exception $e) {
    return json_decode($e->getData());
}

//Sync Kontakt
/**
 * @internal (jsr) added syncMode = 1 to update exiting contacts
 * @see https://dev.maileon.com/api/rest-api-1-0/contacts/synchronize-contacts/
 * @param type $syncContacts
 * @param type $contactsService
 * @return type
 */
function doContactSync($syncContacts, $contactsService, $custom_fields)
{
    $sync_mode           = com_maileon_api_contacts_SynchronizationMode::$UPDATE; // Specifies the synchronization option in case a contact with the provided identifier (external id or email address) already exists: 1:update, 2(default!!!):ignore
    $mio_contact_to_sync = new com_maileon_api_contacts_Contacts();

    foreach ($syncContacts as $plenty_contact_arr) {
        $doi = $plenty_contact_arr['doi'];

        if ($doi) {
            $permission = com_maileon_api_contacts_Permission::$NONE;
        } else {
            $permission = com_maileon_api_contacts_Permission::$DOI_PLUS;
        }

        if (isset($plenty_contact_arr['maileon']["contactId"]) && !empty($plenty_contact_arr['maileon']["contactId"])) {
            ; // do nothing
        } else {
            $plenty_contact_arr['maileon']["contactId"] = 0;
        }

        if (!isset($plenty_contact_arr['maileon']["birthday"]) || ($plenty_contact_arr['maileon']["birthday"]
            == "0000-00-00")) {

            $plenty_contact_arr['maileon']["birthday"] = '';
        }

        if (!isset($plenty_contact_arr['maileon']["gender"]) || (empty($plenty_contact_arr['maileon']["gender"]))) {

            $plenty_contact_arr['maileon']["gender"] = 'm';
        }

        //gender Divers
        if ($plenty_contact_arr['maileon']["gender"] = 'd') {
            $plenty_contact_arr['maileon']["gender"] = 'm';
        }

        if (!isset($plenty_contact_arr['maileon']["SALUTATION"]) || (empty($plenty_contact_arr['maileon']["SALUTATION"]))) {
            $plenty_contact_arr['maileon']["SALUTATION"] = 'Herr';
        }

        // add folder custom fields
        $cust_fields = [];
        foreach ($custom_fields as $email => $cf_arr) {
            if ($email == $plenty_contact_arr['maileon']["email"]) {
                foreach ($cf_arr as $cf) {
                    $cust_fields[$cf] = true;
                }
            }
        }

        // add default custom fields
        $cust_fields['plentyID']    = isset($plenty_contact_arr['maileon']["plentyID"])
                ? $plenty_contact_arr['maileon']["plentyID"]
                : '';

        $cust_fields['lastOrderAt'] = isset($plenty_contact_arr['maileon']["lastOrderAt"])
                ? $plenty_contact_arr['maileon']["lastOrderAt"]
                : '';

           $mio_contact_to_sync = new com_maileon_api_contacts_Contact(
                null, $plenty_contact_arr['maileon']["email"], $permission,
                -1, // external_id (-1) = default value
                $anonymous = false, //(jsr: boolean required!) null,
                array(
                    'FIRSTNAME' => isset($plenty_contact_arr['maileon']["firstName"])
                            ? $plenty_contact_arr['maileon']["firstName"]
                            : '',

                    'LASTNAME' => isset($plenty_contact_arr['maileon']["lastName"])
                            ? $plenty_contact_arr['maileon']["lastName"]
                            : '',

                    'GENDER' => isset($plenty_contact_arr['maileon']["gender"])
                            ? $plenty_contact_arr['maileon']["gender"]
                            : '',

                    'SALUTATION' => isset($plenty_contact_arr['maileon']["SALUTATION"])
                            ? $plenty_contact_arr['maileon']["SALUTATION"]
                            : '',

                    'BIRTHDAY' => isset($plenty_contact_arr['maileon']["birthday"])
                            ? $plenty_contact_arr['maileon']["birthday"]
                            : ''
                ),
                $cust_fields
        );
        // Single Sync
       $response = $contactsService->createContact($mio_contact_to_sync, $sync_mode, "", "", false, false);
 
       $result[$plenty_contact_arr['maileon']["email"]] = $response->getResultXML();
    }

    $resultSync['contactResponse'] = $result;
    return $resultSync;
}

