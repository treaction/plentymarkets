<?php
/* @internal returns mio transaction types as array with id and name alias Kontaktereignis alias contact event
 * @author JSR|treaction ag
 * @api api.maileon.com/1.0
 * @function getAllMioTransactionTypes
 * @returns array arr['unique_transaction_type_name'] = (int)<transaction_type_id>
 */
$rootPath = __DIR__ . \DIRECTORY_SEPARATOR;
\define('DIR_Includes', $rootPath . 'includes' . \DIRECTORY_SEPARATOR);

require_once(DIR_Includes.'MaileonApiClient.php');


try{
    $apiKey = SdkRestApi::getParam('apiKey');
    // Set the global configuration for accessing the REST-API
    $config = array(
        "BASE_URI" => "https://api.maileon.com/1.0",
        "API_KEY" => $apiKey,
        "PROXY_HOST" => "",
        "PROXY_PORT" => "",
        "THROW_EXCEPTION" => true,
        "TIMEOUT" => 300,
        "DEBUG" => "false" // NEVER enable on production
    );

    $transaction_service = new com_maileon_api_transactions_TransactionsService($config);
    $transaction_service->setDebug(false);

    $number_of_trsctn_types = $transaction_service->getTransactionTypesCount();
    $trsctn_types_arr = $transaction_service->getTransactionTypes(1, $number_of_trsctn_types)->getResult();

    $ret_arr = []; // ret_arr['email_address'] = (int)<permission>

    foreach($trsctn_types_arr as $t) {
        $ret_arr[$t->name] = $t->id;
    }

    return $ret_arr;


}catch (Exception $e){
    return json_decode($e->getData());
}

