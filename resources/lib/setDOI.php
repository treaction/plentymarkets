<?php

$rootPath = __DIR__ . \DIRECTORY_SEPARATOR;
\define('DIR_Includes', $rootPath . 'includes' . \DIRECTORY_SEPARATOR);

require_once(DIR_Includes . 'MaileonApiClient.php');


try {
    $apiKey = SdkRestApi::getParam('apiKey');
    $doiKey = SdkRestApi::getParam('doiKey');
    $contact = SdkRestApi::getParam('maileon');
    // Set the global configuration for accessing the REST-API
    $config = array(
        "BASE_URI" => "https://api.maileon.com/1.0",
        "API_KEY" => $apiKey,
        "PROXY_HOST" => "",
        "PROXY_PORT" => "",
        "THROW_EXCEPTION" => true,
        "TIMEOUT" => 300,
        "DEBUG" => "false" // NEVER enable on production
    );

    $contactsService = new com_maileon_api_contacts_ContactsService($config);
    $contactsService->setDebug(false);

   /* $getContact = $contactsService->getContactByEmail($email);
    if ($getContact->isSuccess() && $getContact->getResult()->permission != com_maileon_api_contacts_Permission::$NONE) {
       //nothing
    } else {*/
        $newContact = new com_maileon_api_contacts_Contact();
        $newContact->email = $contact['email'];
        $newContact->anonymous = false;
        $newContact->permission = com_maileon_api_contacts_Permission::$NONE;

        $newContact->standard_fields["SALUTATION"] = $contact["gender"];
        $newContact->standard_fields["FIRSTNAME"] = $contact["firstName"];
        $newContact->standard_fields["LASTNAME"] = $contact["lastName"];

        $response = $contactsService->createContact($newContact, com_maileon_api_contacts_SynchronizationMode::$UPDATE, '', '', true, true, (string)$doiKey);

      return $response->getResult();
   # }
} catch (Exception $e) {
    return json_decode($e->getData());
}