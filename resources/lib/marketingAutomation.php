<?php

$rootPath = __DIR__ . \DIRECTORY_SEPARATOR;
\define('DIR_Includes', $rootPath . 'includes' . \DIRECTORY_SEPARATOR);

require_once(DIR_Includes.'MaileonApiClient.php');


function isUnsubscribed($email, $config)
{
    $reportsService = new com_maileon_api_reports_ReportsService($config);
    $reportsService->setDebug(false);
    $response = $reportsService->getUnsubscribers(null, null, null, null, [$email]);

    return (count($response->getResult()) > 0)
        ? true  // unsubsribed
        :  false; // not unsubsribed => still subscribed
}


// check Contact
function existsContact($email, $config)
{
    $debug           = FALSE;
    $contactsService = new com_maileon_api_contacts_ContactsService($config);
    $contactsService->setDebug($debug);
    $response        = $contactsService->getContactByEmail($email);

    return $response->isSuccess();
}


// This method creates/updates a contact with the given id in Maileon
function createContact($email, $FIRSTNAME, $LASTNAME, $gender, $config)
{
    $debug           = FALSE;
    $contactsService = new com_maileon_api_contacts_ContactsService($config);
    $contactsService->setDebug($debug);

    $newContact                                                                              = new com_maileon_api_contacts_Contact();
    $newContact->email                                                                       = $email;
    $newContact->standard_fields[com_maileon_api_contacts_StandardContactField::$FIRSTNAME]  = $FIRSTNAME;
    $newContact->standard_fields[com_maileon_api_contacts_StandardContactField::$LASTNAME]   = $LASTNAME;
    $newContact->standard_fields[com_maileon_api_contacts_StandardContactField::$SALUTATION] = $gender;
    $newContact->permission                                                                  = com_maileon_api_contacts_Permission::$DOI_PLUS;

    $response = $contactsService->createContact($newContact, com_maileon_api_contacts_SynchronizationMode::$UPDATE, "", "", false);

    return $response;
}


try{

    $apiKey = SdkRestApi::getParam('apiKey');
    $programId= SdkRestApi::getParam('programId');
    $contact = SdkRestApi::getParam('maileon');
    $email = $contact['email'];

    // Set the global configuration for accessing the REST-API
    $config = array(
        "BASE_URI" => "https://api.maileon.com/1.0",
        "API_KEY" => $apiKey,
        "PROXY_HOST" => "",
        "PROXY_PORT" => "",
        "THROW_EXCEPTION" => true,
        "TIMEOUT" => 300,
        "DEBUG" => "false" // NEVER enable on production
    );

    switch ($contact["gender"]) {
        case 'd':
        case 'divers':
        case 'male':
        case 'm':
            $gender = 'Herr';
            break;
        case 'female':
        case 'f':
            $gender = 'Frau';
            break;
        default:
            $gender = 'Herr';
            break;
    }

    $response = FALSE;

    $is_unsubscribed = isUnsubscribed($contact['email'], $config);

    if(!$is_unsubscribed) {
        //Check Conatct
        $contactExisted = existsContact($contact['email'], $config);

        $create_contact_res = '';

        if (!$contactExisted) {
            $create_contact_res = createContact(
                $contact['email'],
                $contact["firstName"],
                $contact["lastName"],
                $gender,
                $config
            );
            sleep(2);
        }

        if (!empty($programId)) {
            // Important: the ID must be a number (and thus, submitted without "" to the REST-Service, otherwhile it will result in 400 - Bad Request)
            $programId = intval(trim($programId));
            $maService = new com_maileon_api_marketingAutomation_MarketingAutomationService($config);
            $debug = false;
            $maService->setDebug($debug);

            $response = $maService->startMarketingAutomationProgram($programId, strtolower($email));

        } else {

           return $contact['email'] . ' subscribed, but without programÍd';
       }

       return [$programId, strtolower($email),$response, $is_unsubscribed, $contactExisted,  $create_contact_res];

    } else {

        return $contact['email'] . ' is unsubscribed';
    }

}catch (Exception $e){

    return json_decode($e->getData());

}
