<?php

$rootPath = __DIR__ . \DIRECTORY_SEPARATOR;
\define('DIR_Includes', $rootPath . 'includes' . \DIRECTORY_SEPARATOR);

require_once(DIR_Includes.'MaileonApiClient.php');

function getQuote($brutto, $netto) {
        $q = 0;
        if ($brutto > 0)
            $q = round((($netto / $brutto) * 100), 2);
        return $q;
    }
try{
  
    $apiKey = SdkRestApi::getParam('apiKey');
    // Set the global configuration for accessing the REST-API
    $config = array(
        "BASE_URI" => "https://api.maileon.com/1.0",
        "API_KEY" => $apiKey,
        "PROXY_HOST" => "",
        "PROXY_PORT" => "",
        "THROW_EXCEPTION" => true,
        "TIMEOUT" => 30000000, 
        "DEBUG" => "false" // NEVER enable on production
    );
    $responseXML = '';
    $mailingService = new com_maileon_api_mailings_MailingsService($config);
    $debug = false;
    $mailingService->setDebug($debug);

    $reportsService = new com_maileon_api_reports_ReportsService($config); 
    $debugReport = false;
    $reportsService->setDebug($debugReport);

    
    $mailingData = array(
            'versendet' => 'getRecipientsCount',
            'openings' => 'getUniqueOpensCount',
            'openings_all' => 'getOpensCount',
            'klicks' => 'getUniqueClicksCount',
            'klicks_all' => 'getClicksCount',
            'abmelder' => 'getUnsubscribersCount'
        );
    
    $now = date('Y-m-d+');

    $fields = array(com_maileon_api_mailings_MailingFields::$NAME,
                    com_maileon_api_mailings_MailingFields::$STATE,
		    com_maileon_api_mailings_MailingFields::$TYPE);

    $response = $mailingService->getMailingsBySchedulingTime($now."00:00:00", false, $fields, 1, 50);
    
    if($response->isSuccess()){
         $responseXML = $response->getResultXML();
          foreach ($responseXML->mailing as $re) {
            foreach ($mailingData as $key => $value){
                
              $response = $reportsService->{$value}(
                                     null,
                                     null, 
                                    array(
                                        $re->id
                      )
                             );
               ${$key} = $response->getResult();
               $re->addChild($key,$response->getResult());
               if($key == 'klicks_all'){
                   $klickRate = getQuote($versendet, $klicks_all);
                    $re->addChild('klicks_rate',$klickRate);
               }
                if($key == 'openings_all'){
                   $openingsRate = getQuote($versendet, $openings_all);
                    $re->addChild('openings_rate',$openingsRate);
               }
          }
           if($re->fields->field[2]->value == 'regular'){            
                $startDate = $mailingService->getSchedule($re->id);
                 if($startDate->isSuccess()){
                     $date = (string)$startDate->getResult()->date;
                     $re->addChild('startDate',$date);
                  }else{
                      $re->addChild('startDate','-----');
                  }                
             }else{
                      $re->addChild('startDate','-----');
                  }
             //Hard Bounce
         $hardBounce = $reportsService->getBouncesCount(
                 null, 
                 null, 
                 array($re->id), 
                 null, 
                 null, 
                 null, 
                 null,
                'permanent',
                 null,
                 null
                 );
                    
          $re->addChild('hbounces',$hardBounce->getResult());
         //Soft Bounce
         $softBounce = $reportsService->getBouncesCount(
                 null, 
                 null, 
                 array($re->id), 
                 null, 
                 null, 
                 null, 
                 null,
                'transient',
                 null,
                 null
                 ); 
         $re->addChild('sbounces',$softBounce->getResult());  
         //Subject
        // $subject = $mailingService->getSubject($re->id);
        // $re->addChild('subject',$subject->getResult());  
         
        }
    }
    return $responseXML;

}catch (Exception $e){
    return json_decode($e->getData());

}