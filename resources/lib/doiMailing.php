<?php

$rootPath = __DIR__ . \DIRECTORY_SEPARATOR;
\define('DIR_Includes', $rootPath . 'includes' . \DIRECTORY_SEPARATOR);

require_once(DIR_Includes . 'MaileonApiClient.php');


try {
    $apiKey = SdkRestApi::getParam('apiKey');
    // Set the global configuration for accessing the REST-API
    $config = array(
        "BASE_URI" => "https://api.maileon.com/1.0",
        "API_KEY" => $apiKey,
        "PROXY_HOST" => "",
        "PROXY_PORT" => "",
        "THROW_EXCEPTION" => true,
        "TIMEOUT" => 300,
        "DEBUG" => "false" // NEVER enable on production
    );

    // Create the service
    $mailingService = new com_maileon_api_mailings_MailingsService($config);
    $debug = false;
    $mailingService->setDebug($debug);

    $response = $mailingService->getMailingsByTypes(array('doi'));
    $res = $response->getResult();
    $doiMailings = array();
    foreach ($res as $result) {
        $doiKeys = $mailingService->getDoiMailingKey((string) $result->id);
        $mailingNames = $mailingService->getName((string) $result->id);
        $doiKey = \simplexml_load_string($doiKeys->getResult());
        $doiMailing = array(
            "doiKey" => (string) $doiKey[0],
            "name" => (string) $mailingNames->getResult()
        );
        array_push($doiMailings, $doiMailing);
    }
    return $doiMailings;
} catch (Exception $e) {
    return json_decode($e->getData());
}